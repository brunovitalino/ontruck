# OnTruck

Gerenciador de informações dos veículos usadas no OnTruck.

## Serviços

- Estados;
- Cidades;
- Cores;
- Marcas;
- Modelos;
- Motoristas;
- Veiculos;
- Viagens;
- Movimentacoes;
- Usuarios;

## Sites

[Documentação](https://ontruck.herokuapp.com/swagger-ui.html)  
[Aplicação](https://ontruck.herokuapp.com)

## Instruções

Quando a aplicação for executada pela primeira vez, é necessário configurar o banco de dados chamando a url "[/balao-magico](https://ontruck.herokuapp.com/balao-magico)" via `GET`. Desse modo, a API estará configurada e pronta para uso.

#### Credenciais   

Para obter o token, é necessário chamar a url "[/auth](https://ontruck.herokuapp.com/auth)" via método `POST` com as seguintes informações abaixo:  

HEAD  
>Content-Type: application/json  

BODY
> {"email": "admin@ontruck.com","senha": "123"}  

Para acessar outros serviços, é necessário informar o token obtido do tipo bearer.  

HEAD (exemplo)  
>Authorization: Bearer ea1fa56rffa4ga  

#### Executando localmente  

* Empacotador: `Maven 3.6.3`  
* JVM: `Java 11`  
* Database: `Postgres 9.6`  

Deve ser criado um database com nome: ``ontruck``  
Configure as credenciais de acesso ao banco. Exemplo:  
> user: postgres  
> password: 123  

Clone o repositório do projeto para uma pasta local do computador, abra o terminal ou prompt de comando, e realize os seguintes passos:

1. Entre na pasta raiz do projeto clonado e execute o comando:  
``mvn clean package``  

2. Em seguida, suba o projeto com o comando:  
``java -jar -Dspring.profiles.active=dev target/ontruck.jar``  

3. Configure a aplicação através da chamada à url via `GET`:  
`GET` [/balao-magico](http://localhost:8080/balao-magico)

4. Faça chamada aos recursos que deseja (em desenvolvimento o token de autenticação não é necessário):  
`GET` [/estados](http://localhost:8080/estados) (exemplo)  

#### Executando em produção  

Repita os mesmos passos acima, para execução local, mas substitua o item 2 pelo seguinte comando de exemplo:  
``java -jar -DONTRUCK_DATASOURCE_URL=jdbc:postgresql://localhost:5432/ontruck -DONTRUCK_DATASOURCE_USERNAME=postgres -DONTRUCK_DATASOURCE_PASSWORD=123 -Dspring.profiles.active=prod target/ontruck.jar``  
obs: substitua a credencial de acesso ao banco, pela sua.  

#### Executando em produção via Docker  

* Empacotador: `Maven 3.6.3`  
* JVM: `Java 11`  
* Database: `Postgres 9.6`  

Deve-se criar um banco próprio ou subir um container docker com Postgres e configurar da seguinte maneira:
1. Nome do database (schema?): `ontruck`;  
2. Configurar as credenciais de acesso ao banco. Exemplo:  
> user: postgres  
> password: 123  
3. Conferir se as porta `5432` está liberada.

Clone o repositório do projeto para uma pasta local do computador, e com o docker instalado abra o terminal ou prompt de comando, e realize os seguintes passos:

1. Entre na pasta raiz do projeto clonado e execute o comando:  
``mvn clean package``  

2. Ainda na pasta raiz, gere a imagem docker da sua aplicação pelo comando:  
``docker build -t tacografoonline/ontruck``  

3. Execute a imagem docker da sua aplicação pelo comando:  
``docker run -p 8080:8080 -e ONTRUCK_DATASOURCE_URL='jdbc:postgresql://localhost:5432/ontruck' -e ONTRUCK_DATASOURCE_USERNAME='postgres' -e ONTRUCK_DATASOURCE_PASSWORD='123' -e spring.profiles.active='prod' tacografoonline/ontruck``  
obs: substitua a credencial de acesso ao banco, pela sua.  

4. Configure a aplicação através da chamada à url via `GET`:  
`GET` [/balao-magico](http://localhost:8080/balao-magico)

5. Faça chamada aos recursos que deseja (em desenvolvimento o token de autenticação não é necessário):  
`GET` [/estados](http://localhost:8080/estados) (exemplo)  
