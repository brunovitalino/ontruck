package com.tacografoonline.ontruck.config.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import com.tacografoonline.ontruck.repository.UsuarioRepository;

@EnableWebSecurity
@Configuration
@Profile(value = {"test", "prod"})
public class SecurityConfigurations extends WebSecurityConfigurerAdapter {
	
	@Autowired
	AuthenticationService usuarioService;
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager(); // instanceara o construtor pai, assim injetamos essa dependencia na classe UsuarioController
	}

	// Configuracao de Autenticacao
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(usuarioService).passwordEncoder(new BCryptPasswordEncoder());
	}

	// Configuracao de Autorizacao
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
		.antMatchers(HttpMethod.GET, "/h2-console/**").permitAll()
		.antMatchers(HttpMethod.GET, "/balao-magico").permitAll()
		.antMatchers(HttpMethod.GET, "/home").permitAll()
		.antMatchers(HttpMethod.GET, "/usuarios").permitAll()
		.antMatchers(HttpMethod.GET, "/usuarios/*").permitAll()
		.antMatchers(HttpMethod.DELETE, "/usuarios/*").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET, "/estados").permitAll()
		.antMatchers(HttpMethod.GET, "/estados/*").permitAll()
		.antMatchers(HttpMethod.GET, "/cidades").permitAll()
		.antMatchers(HttpMethod.GET, "/cidades/*").permitAll()
		.antMatchers(HttpMethod.GET, "/motoristas").permitAll()
		.antMatchers(HttpMethod.GET, "/motoristas/*").permitAll()
		.antMatchers(HttpMethod.GET, "/cores").permitAll()
		.antMatchers(HttpMethod.GET, "/cores/*").permitAll()
		.antMatchers(HttpMethod.GET, "/marcas").permitAll()
		.antMatchers(HttpMethod.GET, "/marcas/*").permitAll()
		.antMatchers(HttpMethod.GET, "/modelos").permitAll()
		.antMatchers(HttpMethod.GET, "/modelos/*").permitAll()
		.antMatchers(HttpMethod.GET, "/veiculos").permitAll()
		.antMatchers(HttpMethod.GET, "/veiculos/*").permitAll()
		.antMatchers(HttpMethod.GET, "/viagens").permitAll()
		.antMatchers(HttpMethod.GET, "/viagens/*").permitAll()
		.antMatchers(HttpMethod.GET, "/movimentacoes").permitAll()
		.antMatchers(HttpMethod.GET, "/movimentacoes/*").permitAll()
		.antMatchers(HttpMethod.POST, "/auth").permitAll()
		.anyRequest().authenticated()
//		.and().formLogin(); // autenticacao atraves de sessao
		.and().csrf().disable() // tokens sao imunes a ataques do tipo csrf, entao podemos desabilitar
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and().addFilterBefore(new AuthenticationFilter(tokenService, usuarioRepository), UsernamePasswordAuthenticationFilter.class) // Remove o formLogin. Requisicoes por tokens nao e necessario guardar estado.
		.cors().configurationSource(request -> {
			CorsConfiguration corsConfiguration = new CorsConfiguration();
			corsConfiguration.setAllowedOrigins(Arrays.asList("http://ontruck.surge.sh"));
			corsConfiguration.setAllowedMethods(Arrays.asList("POST", "OPTIONS", "GET", "PUT", "DELETE"));
			corsConfiguration.setAllowedHeaders(Arrays.asList("*"));
			corsConfiguration.setAllowCredentials(true);
			corsConfiguration.setMaxAge(Long.valueOf(3600));
			return corsConfiguration;
		});
	}

	// Configuracao de arquivos estaticos(js, css, imagens, etc.)
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/h2-console/**", "/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**");
	}
	
}
