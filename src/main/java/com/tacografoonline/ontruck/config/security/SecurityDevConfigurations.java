package com.tacografoonline.ontruck.config.security;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;

@EnableWebSecurity
@Configuration
@Profile(value = {"dev", "heroku"})
public class SecurityDevConfigurations extends WebSecurityConfigurerAdapter {
	
	// Configuracao de Autorizacao
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
		.and().csrf().disable()
//		.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());
		.cors().configurationSource(request -> {
			CorsConfiguration corsConfiguration = new CorsConfiguration();
			corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "http://127.0.0.1:4200", "http://ontruck.surge.sh"));
			corsConfiguration.setAllowedMethods(Arrays.asList("POST", "OPTIONS", "GET", "PUT", "DELETE"));
			corsConfiguration.setAllowedHeaders(Arrays.asList("*"));
			corsConfiguration.setAllowCredentials(true);
			corsConfiguration.setMaxAge(Long.valueOf(3600));
			return corsConfiguration;
		});
	}

	// Configuracao de arquivos estaticos(js, css, imagens, etc.)
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**");
	}
	
}
