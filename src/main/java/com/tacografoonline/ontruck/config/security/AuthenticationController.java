package com.tacografoonline.ontruck.config.security;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tacografoonline.ontruck.controller.dto.TokenDto;
import com.tacografoonline.ontruck.controller.dto.UsuarioFormDto;

@RestController
@RequestMapping("/auth")
@Profile(value = {"test", "prod"})
public class AuthenticationController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	private TokenService tokenService;

	@PostMapping
	public ResponseEntity<?> authenticateUserAndGenerateToken(@RequestBody @Valid UsuarioFormDto usuarioFormDto) {
		try {
			Authentication authentication = authenticationManager.authenticate(usuarioFormDto.toAuthentication());
			String jwt = tokenService.gerarJWT(authentication);
			return ResponseEntity.ok(new TokenDto(jwt, "Bearer"));
		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().build();
		}
	}
}
