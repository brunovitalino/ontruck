package com.tacografoonline.ontruck.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.tacografoonline.ontruck.model.Usuario;
import com.tacografoonline.ontruck.repository.UsuarioRepository;

public class AuthenticationFilter extends OncePerRequestFilter {

	private TokenService tokenService;
	private UsuarioRepository usuarioRepository;

	public AuthenticationFilter(TokenService tokenService, UsuarioRepository usuarioRepository) {
		this.tokenService = tokenService;
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String jwt = getJWT(request);
		if (tokenService.isValido(jwt)) {
			liberarAcesso(jwt);
		}
		filterChain.doFilter(request, response);
	}

	private String getJWT(HttpServletRequest request) {
		String jwt = request.getHeader("Authorization");
		if (jwt == null || jwt.isEmpty() || !jwt.startsWith("Bearer ")) {
			return null;
		}
		return jwt.substring(7);
	}

	private void liberarAcesso(String jwt) {
		Long usuarioId = tokenService.getUsuarioId(jwt);
		Usuario usuario = usuarioRepository.findById(usuarioId).get();
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usuario, null, usuario.getRoles());
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

}
