package com.tacografoonline.ontruck.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Usuario;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
@Profile(value = {"test", "prod", "heroku"})
public class TokenService {
	
	@Value("${ontruck.jwt.expirationDate}")
	private String expirationDate;
	
	@Value("${ontruck.jwt.secretKey}")
	private String secretKey;

	public String gerarJWT(Authentication authentication) {
		Usuario usuario = (Usuario) authentication.getPrincipal();
		Date dataAtual = new Date();
		Date dataExpiracao = new Date(dataAtual.getTime() + Long.parseLong(expirationDate));
		return Jwts.builder()
				.setIssuer("API do OnTruck")
				.setSubject(usuario.getId().toString())
				.setIssuedAt(dataAtual)
				.setExpiration(dataExpiracao)
				.signWith(SignatureAlgorithm.HS256, secretKey)
				.compact();
	}

	public boolean isValido(String jwt) {
		try {
			Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(jwt);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Long getUsuarioId(String jwt) {
		Claims claims = Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(jwt).getBody();
		return Long.parseLong(claims.getSubject());
	}

}
