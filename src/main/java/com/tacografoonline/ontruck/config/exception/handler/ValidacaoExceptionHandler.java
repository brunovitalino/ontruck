package com.tacografoonline.ontruck.config.exception.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.tacografoonline.ontruck.controller.dto.ValidacaoExceptionDto;

@RestControllerAdvice
public class ValidacaoExceptionHandler {
	
	@Autowired
	MessageSource messageSource;
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ValidacaoExceptionDto> handler(MethodArgumentNotValidException exception) {
		System.err.println("ERRO: " + exception.getLocalizedMessage());
		List<ValidacaoExceptionDto> validacaoExceptionDtoList = new ArrayList<>();
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
		
		fieldErrors.forEach(f -> {
			String defaultMessage = messageSource.getMessage(f, LocaleContextHolder.getLocale());
			ValidacaoExceptionDto validacaoExceptionDto = new ValidacaoExceptionDto(f.getField(), defaultMessage);
			validacaoExceptionDtoList.add(validacaoExceptionDto);
		});
		
		return validacaoExceptionDtoList;
	}

}
