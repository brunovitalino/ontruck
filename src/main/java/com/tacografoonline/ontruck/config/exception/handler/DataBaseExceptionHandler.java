package com.tacografoonline.ontruck.config.exception.handler;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.tacografoonline.ontruck.controller.dto.ExceptionDto;

@RestControllerAdvice
public class DataBaseExceptionHandler {
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR) // banco = 500
	public ExceptionDto handlerDataIntegrityViolationException(DataIntegrityViolationException e) {
		String cause = e.getMostSpecificCause().getMessage();
		String causa = "";
		String exceptionClass = e.getClass().getName().substring(e.getClass().getName().lastIndexOf(".") + 1);

		ExceptionDto exceptionDto = new ExceptionDto(exceptionClass, cause, causa);
		return exceptionDto;
	}

}
