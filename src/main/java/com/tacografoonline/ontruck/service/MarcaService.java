package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.service.exception.MarcaException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2021-01-16
 *
 */
public interface MarcaService {
	
	/**
	 * Método usado para buscar todos as marcas.
	 */
	List<Marca> findAll() throws MarcaException;
	
	/**
	 * Método usado para buscar todos as marcas organizados pela paginação.
	 */
	Page<Marca> findAll(Pageable pageable) throws MarcaException;
	
	/**
	 * Método usado para buscar uma marca pelo nome e também por paginação.
	 */
	Page<Marca> findAllByNome(String nome, Pageable pageable) throws MarcaException;
	
	/**
	 * Método usado para buscar uma marca pela chave primária.
	 */
	Optional<Marca> findOneById(Long id) throws MarcaException;

	/**
	 * Método usado para buscar uma marca pelo nome.
	 */
	Optional<Marca> findOneByNome(String nome) throws MarcaException;
	
	/**
	 * Método usado para salvar uma marca.
	 */
	Marca save(Marca marca) throws MarcaException;
	
	/**
	 * Método usado para atualizar uma marca.
	 */
	Marca update(Long id, Marca marcaNew) throws MarcaException;
	
	/**
	 * Método usado para remover uma marca.
	 */
	void delete(Long id) throws MarcaException;

}
