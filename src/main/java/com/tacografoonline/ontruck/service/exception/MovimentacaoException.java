package com.tacografoonline.ontruck.service.exception;

public class MovimentacaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public MovimentacaoException() {
	}

	public MovimentacaoException(String msg) {
		super("Erro no serviço de Movimentacao. " + msg);
	}

	public MovimentacaoException(MovimentacaoException e) {
		super(e);
	}

}
