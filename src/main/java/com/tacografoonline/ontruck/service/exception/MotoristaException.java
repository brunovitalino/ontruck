package com.tacografoonline.ontruck.service.exception;

public class MotoristaException extends Exception {

	private static final long serialVersionUID = 1L;

	public MotoristaException() {
	}

	public MotoristaException(String msg) {
		super("Erro em Motorista. " + msg);
	}

	public MotoristaException(MotoristaException e) {
		super(e);
	}

}
