package com.tacografoonline.ontruck.service.exception;

public class EstadoException extends Exception {

	private static final long serialVersionUID = 1L;

	public EstadoException() {
	}

	public EstadoException(String msg) {
		super("Erro em Estado. " + msg);
	}

}
