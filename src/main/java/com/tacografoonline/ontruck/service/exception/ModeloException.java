package com.tacografoonline.ontruck.service.exception;

public class ModeloException extends Exception {

	private static final long serialVersionUID = 1L;

	public ModeloException() {
	}

	public ModeloException(String msg) {
		super("Erro em Modelo. " + msg);
	}

	public ModeloException(ModeloException e) {
		super(e);
	}

}
