package com.tacografoonline.ontruck.service.exception;

public class CidadeException extends Exception {

	private static final long serialVersionUID = 1L;

	public CidadeException() {
	}

	public CidadeException(String msg) {
		super("Erro em Cidade. " + msg);
	}

	public CidadeException(CidadeException e) {
		super(e);
	}

}
