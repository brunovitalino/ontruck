package com.tacografoonline.ontruck.service.exception;

public class VeiculoException extends Exception {

	private static final long serialVersionUID = 1L;

	public VeiculoException() {
	}

	public VeiculoException(String msg) {
		super("Erro em Veiculo. " + msg);
	}

	public VeiculoException(VeiculoException e) {
		super(e);
	}

}
