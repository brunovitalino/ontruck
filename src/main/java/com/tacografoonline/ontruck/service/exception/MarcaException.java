package com.tacografoonline.ontruck.service.exception;

public class MarcaException extends Exception {

	private static final long serialVersionUID = 1L;

	public MarcaException() {
	}

	public MarcaException(String msg) {
		super("Erro em Marca. " + msg);
	}

}
