package com.tacografoonline.ontruck.service.exception;

public class ConfigException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConfigException() {
	}

	public ConfigException(String msg) {
		super("Erro em Configuração. " + msg);
	}

	public ConfigException(ConfigException e) {
		super(e);
	}

}
