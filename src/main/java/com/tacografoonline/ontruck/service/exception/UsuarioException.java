package com.tacografoonline.ontruck.service.exception;

public class UsuarioException extends Exception {

	private static final long serialVersionUID = 1L;

	public UsuarioException() {
	}

	public UsuarioException(String msg) {
		super("Erro em Usuario. " + msg);
	}

	public UsuarioException(UsuarioException e) {
		super(e);
	}

}
