package com.tacografoonline.ontruck.service.exception;

public class BlockchainException extends Exception {

	private static final long serialVersionUID = 1L;

	public BlockchainException() {
	}

	public BlockchainException(String msg) {
		super("Erro no serviço de Blockchain. " + msg);
	}

	public BlockchainException(BlockchainException e) {
		super(e);
	}

}
