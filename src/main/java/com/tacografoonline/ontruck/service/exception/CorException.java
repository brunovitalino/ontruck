package com.tacografoonline.ontruck.service.exception;

public class CorException extends Exception {

	private static final long serialVersionUID = 1L;

	public CorException() {
	}

	public CorException(String msg) {
		super("Erro em Cor. " + msg);
	}

}
