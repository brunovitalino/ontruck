package com.tacografoonline.ontruck.service.exception;

public class RoleException extends Exception {

	private static final long serialVersionUID = 1L;

	public RoleException() {
	}

	public RoleException(String msg) {
		super("Erro em Role. " + msg);
	}

	public RoleException(RoleException e) {
		super(e);
	}

}
