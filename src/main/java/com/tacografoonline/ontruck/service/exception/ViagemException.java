package com.tacografoonline.ontruck.service.exception;

public class ViagemException extends Exception {

	private static final long serialVersionUID = 1L;

	public ViagemException() {
	}

	public ViagemException(String msg) {
		super("Erro em Viagem. " + msg);
	}

	public ViagemException(ViagemException e) {
		super(e);
	}

}
