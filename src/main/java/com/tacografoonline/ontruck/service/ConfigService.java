package com.tacografoonline.ontruck.service;

import java.util.Optional;

import com.tacografoonline.ontruck.model.Config;
import com.tacografoonline.ontruck.service.exception.ConfigException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-18
 *
 */
public interface ConfigService {
	
	/**
	 * Método usado para preencher informações no banco de dados.
	 */
	void fillDB() throws ConfigException;

	Optional<Config> findById(Long id) throws ConfigException;

	Config save(Config configuracao) throws ConfigException;

	Config update(Long id, Config configuracaoNew) throws ConfigException;
	
}
