package com.tacografoonline.ontruck.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.service.exception.ViagemException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface ViagemService {
	
	/**
	 * Método usado para buscar todos os viagens.
	 * @return List contendo as Viagens.
	 */
	List<Viagem> findAll() throws ViagemException;
	
	/**
	 * Método usado para buscar todos os viagens organizados por paginação.
	 * @return Page contendo as Viagens.
	 */
	Page<Viagem> findAll(Pageable pageable) throws ViagemException;
	
	/**
	 * Método usado para buscar uma viagem por data e também por paginação.
	 * @return Page contendo as Viagens.
	 */
	Page<Viagem> findAllByData(LocalDate data, Pageable pageable) throws ViagemException;
	
	/**
	 * Método usado para buscar uma viagem por placa de veículo e também por paginação.
	 * @return Page contendo as Viagens.
	 */
	Page<Viagem> findAllByVeiculoPlaca(String veiculoPlaca, Pageable pageable) throws ViagemException;
	
	/**
	 * Método usado para buscar uma viagem por chave primária.
	 * @return Optional de um Viagem.
	 */
	Optional<Viagem> findOneById(Long id) throws ViagemException;
	
	/**
	 * Método usado para salvar uma viagem.
	 * @return Viagem com as novas informações cadastradas, como id.
	 */
	Viagem save(Viagem viagem) throws ViagemException;

	/**
	 * Método usado para salvar uma lista de viagens.
	 * @return Viagens com as novas informações cadastradas, como id.
	 */
	List<Viagem> save(List<Viagem> viagens) throws ViagemException;
	
	/**
	 * Método usado para atualizar uma viagem.
	 * @return Optional da viagem atualizada.
	 */
	Viagem update(Long id, Viagem viagemNew) throws ViagemException;
	
	/**
	 * Método usado para atualizar apenas o HorarioPartida de uma viagem.
	 * @return Optional da viagem atualizada.
	 */
	Viagem updateHorarioPartida(Long id, LocalDateTime HorarioPartida) throws ViagemException;
	
	/**
	 * Método usado para atualizar apenas o HorarioChegada de uma viagem.
	 * @return Optional da viagem atualizada.
	 */
	Viagem updateHorarioChegada(Long id, LocalDateTime HorarioChegada) throws ViagemException;
	
	/**
	 * Método usado para atualizar apenas o KmTotalPartida de uma viagem.
	 * @return Optional da viagem atualizada.
	 */
	Viagem updateKmTotalPartida(Long id, Integer KmTotalPartida) throws ViagemException;
	
	/**
	 * Método usado para atualizar apenas o KmTotalChegada de uma viagem.
	 * @return Optional da viagem atualizada.
	 */
	Viagem updateKmTotalChegada(Long id, Integer KmTotalChegada) throws ViagemException;
	
	/**
	 * Método usado para remover uma viagem.
	 */
	void delete(Long id) throws ViagemException;

}
