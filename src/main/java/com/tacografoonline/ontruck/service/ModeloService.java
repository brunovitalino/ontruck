package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.service.exception.ModeloException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface ModeloService {
	
	/**
	 * Método usado para buscar todos os modelos.
	 */
	List<Modelo> findAll() throws ModeloException;
	
	/**
	 * Método usado para buscar todos os modelos organizados pela paginação.
	 */
	Page<Modelo> findAll(Pageable pageable) throws ModeloException;
	
	/**
	 * Método usado para buscar todas as modelos pelos atributos e organizados pela paginação.
	 */
	Page<Modelo> findAll(Optional<String> nome, Optional<String> marcaNome, Pageable pageable) throws ModeloException;
	
	/**
	 * Método usado para buscar um modelo pelo nome e também por paginação.
	 */
	Page<Modelo> findAllByNome(String nome, Pageable pageable) throws ModeloException;

	/**
	 * Método usado para buscar um modelo pelo nome do marca e também por paginação.
	 */
	Page<Modelo> findAllByMarcaNome(String marcaNome, Pageable pageable) throws ModeloException;

	/**
	 * Método usado para buscar um modelo pelo nome do marca, nome da modelo e também por paginação.
	 */
	Page<Modelo> findAllByNomeAndMarcaNome(String nome, String marcaNome, Pageable pageable) throws ModeloException;
	
	/**
	 * Método usado para buscar um modelo pela chave primária.
	 */
	Optional<Modelo> findOneById(Long id) throws ModeloException;
	
	/**
	 * Método usado para salvar um modelo.
	 */
	Modelo save(Modelo modelo) throws ModeloException;
	
	/**
	 * Método usado para salvar uma lista de modelos.
	 */
	List<Modelo> save(List<Modelo> modelos) throws ModeloException;
	
	/**
	 * Método usado para atualizar um modelo.
	 */
	Modelo update(Long id, Modelo modeloNew) throws ModeloException;
	
	/**
	 * Método usado para remover um modelo.
	 */
	void delete(Long id) throws ModeloException;

}
