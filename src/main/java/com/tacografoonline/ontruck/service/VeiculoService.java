package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.service.exception.VeiculoException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface VeiculoService {
	
	/**
	 * Método usado para buscar todos os veiculos.
	 */
	List<Veiculo> findAll() throws VeiculoException;
	
	/**
	 * Método usado para buscar todos os veiculos organizados pela paginação.
	 */
	Page<Veiculo> findAll(Pageable pageable) throws VeiculoException;
	
	/**
	 * Método usado para buscar uma veiculo pela placa e também por paginação.
	 */
//	Page<Veiculo> findAllByPlaca(String placa, Pageable ) throws VeiculoException;
	
	
	/**
	 * Método usado para buscar uma veiculo pela chave primária.
	 */
	Optional<Veiculo> findOneById(Long id) throws VeiculoException;
	
	/**
	 * Método usado para buscar um veiculo pela placa.
	 */
	Page<Veiculo> findByPlaca(String placa, Pageable pageable) throws VeiculoException;
	
	/**
	 * Método usado para salvar uma veiculo.
	 * @return Veiculo com as novas informações cadastradas, como id.
	 */
	Veiculo save(Veiculo veiculo) throws VeiculoException;

	/**
	 * Método usado para salvar uma lista de veiculos.
	 * @return Veiculos com as novas informações cadastradas, como id.
	 */
	List<Veiculo> save(List<Veiculo> veiculos) throws VeiculoException;
	
	/**
	 * Método usado para atualizar uma veiculo.
	 */
	Veiculo update(Long id, Veiculo veiculoNew) throws VeiculoException;
	
	/**
	 * Método usado para atualizar apenas a KmTital de um veiculo.
	 */
	Veiculo updateKmTotal(Long id, Integer kmTotal) throws VeiculoException;
	
	/**
	 * Método usado para remover uma veiculo.
	 */
	void delete(Long id) throws VeiculoException;

}
