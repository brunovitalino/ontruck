package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Usuario;
import com.tacografoonline.ontruck.service.exception.UsuarioException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface UsuarioService {
	
	/**
	 * Método usado para buscar todos os usuarios.
	 * @return List contendo os Usuarios.
	 */
	List<Usuario> findAll() throws UsuarioException;
	
	/**
	 * Método usado para buscar todos os usuarios organizados por paginação.
	 * @return Page contendo os Usuarios.
	 */
	Page<Usuario> findAll(Pageable pageable) throws UsuarioException;
	
	/**
	 * Método usado para buscar um usuario por nome e também por paginação.
	 * @return Page contendo os Usuarios.
	 */
	Page<Usuario> findAllByNome(String nome, Pageable pageable) throws UsuarioException;
	
	/**
	 * Método usado para buscar um usuario por chave primária.
	 * @return Optional de um Usuario.
	 */
	Optional<Usuario> findOneById(Long id) throws UsuarioException;
	
	/**
	 * Método usado para buscar um usuario por nome.
	 * @return Optional de um Usuario.
	 */
	Optional<Usuario> findByNome(String nome) throws UsuarioException;
	
	/**
	 * Método usado para buscar um usuario por email.
	 * @return Optional de um Usuario.
	 */
	Optional<Usuario> findByEmail(String emai) throws UsuarioException;
	
	/**
	 * Método usado para salvar um usuario.
	 * @return Usuario com as novas informações cadastradas, como id.
	 */
	Usuario save(Usuario usuario) throws UsuarioException;

	/**
	 * Método usado para salvar uma lista de usuarios.
	 * @return Usuarios com as novas informações cadastradas, como id.
	 */
	List<Usuario> saveAll(List<Usuario> usuarios) throws UsuarioException;
	
	/**
	 * Método usado para atualizar um usuario.
	 */
	Usuario update(Long id, Usuario usuarioNew) throws UsuarioException;
	
	/**
	 * Método usado para atualizar apenas o Email de um usuario.
	 */
	Usuario updateEmail(Long id, String email) throws UsuarioException;
	
	/**
	 * Método usado para atualizar apenas a Senha de um usuario.
	 */
	Usuario updateSenha(Long id, String senha) throws UsuarioException;
	
	/**
	 * Método usado para remover um usuario.
	 */
	void delete(Long id) throws UsuarioException;

}
