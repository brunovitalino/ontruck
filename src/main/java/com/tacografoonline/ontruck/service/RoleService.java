package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import com.tacografoonline.ontruck.model.Role;
import com.tacografoonline.ontruck.service.exception.RoleException;

/**
 * 
 * @author Bruno Vitalino
 * @version 2.0
 * @since 2021-03-02
 *
 */
public interface RoleService {
	
	/**
	 * Método usado para buscar todos os roles.
	 * @return List contendo os Roles.
	 */
	List<Role> findAll() throws RoleException;
	
	/**
	 * Método usado para buscar um role por chave primária.
	 * @return Optional de um Role.
	 */
	Optional<Role> findOneById(Long id) throws RoleException;
	
	/**
	 * Método usado para buscar um role por nome.
	 * @return Optional de um Role.
	 */
	Optional<Role> findByNome(String nome) throws RoleException;
	
	/**
	 * Método usado para salvar um role.
	 * @return Role com as novas informações cadastradas, como id.
	 */
	Role save(Role role) throws RoleException;

	/**
	 * Método usado para salvar uma lista de roles.
	 * @return Roles com as novas informações cadastradas, como id.
	 */
	List<Role> saveAll(List<Role> roles) throws RoleException;
	
	/**
	 * Método usado para atualizar um role.
	 */
	Role update(Long id, Role roleNew) throws RoleException;
	
	/**
	 * Método usado para remover um role.
	 */
	void delete(Long id) throws RoleException;

}
