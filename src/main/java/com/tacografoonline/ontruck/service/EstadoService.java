package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.service.exception.EstadoException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface EstadoService {
	
	/**
	 * Método usado para buscar todos os estados.
	 */
	List<Estado> findAll() throws EstadoException;
	
	/**
	 * Método usado para buscar todos os estados organizados pela paginação.
	 */
	Page<Estado> findAll(Pageable pageable) throws EstadoException;
	
	/**
	 * Método usado para buscar um estado pelo nome e também por paginação.
	 */
	Page<Estado> findAllByNome(String nome, Pageable pageable) throws EstadoException;
	
	/**
	 * Método usado para buscar um estado pela chave primária.
	 */
	Optional<Estado> findOne(Long id) throws EstadoException;

	/**
	 * Método usado para buscar um estado pelo nome.
	 */
	Optional<Estado> findOneByNome(String nome) throws EstadoException;
	
	/**
	 * Método usado para salvar um estado.
	 */
	Estado save(Estado estado) throws EstadoException;

	/**
	 * Método usado para salvar vários estados.
	 */
	List<Estado> saveAll(List<Estado> estadosToSave) throws EstadoException;
	
	/**
	 * Método usado para atualizar um estado.
	 */
	Estado update(Long id, Estado estadoNew) throws EstadoException;
	
	/**
	 * Método usado para remover um estado.
	 */
	void delete(Long id) throws EstadoException;

}
