package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.service.exception.CorException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2021-01-16
 *
 */
public interface CorService {
	
	/**
	 * Método usado para buscar todos as cores.
	 */
	List<Cor> findAll() throws CorException;
	
	/**
	 * Método usado para buscar todos as cores organizados pela paginação.
	 */
	Page<Cor> findAll(Pageable pageable) throws CorException;
	
	/**
	 * Método usado para buscar uma cor pelo nome e também por paginação.
	 */
	Page<Cor> findAllByNome(String nome, Pageable pageable) throws CorException;
	
	/**
	 * Método usado para buscar uma cor pela chave primária.
	 */
	Optional<Cor> findOneById(Long id) throws CorException;

	/**
	 * Método usado para buscar uma cor pelo nome.
	 */
	Optional<Cor> findOneByNome(String nome) throws CorException;
	
	/**
	 * Método usado para salvar uma cor.
	 */
	Cor save(Cor cor) throws CorException;
	
	/**
	 * Método usado para atualizar uma cor.
	 */
	Cor update(Long id, Cor corNew) throws CorException;
	
	/**
	 * Método usado para remover uma cor.
	 */
	void delete(Long id) throws CorException;

}
