package com.tacografoonline.ontruck.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
@Deprecated
public interface MovimentacaoServiceOLD {
	
	/**
	 * Método usado para buscar todos os movimentacaos.
	 */
	List<Movimentacao> findAll() throws MovimentacaoException;
	
	/**
	 * Método usado para buscar todos os movimentacaos organizados pela paginação.
	 */
	Page<Movimentacao> findAll(Pageable pageable) throws MovimentacaoException;
	
	/**
	 * Método usado para buscar uma movimentacao pelo nome e também por paginação.
	 */
	Page<Movimentacao> findAllByData(LocalDate dataInicio, LocalDate dataFim, Pageable pageable) throws MovimentacaoException;
	
	
	/**
	 * Método usado para buscar uma movimentacao pela chave primária.
	 */
	Optional<Movimentacao> findOneById(Long id) throws MovimentacaoException;
	
	/**
	 * Método usado para salvar uma movimentacao.
	 * @return Movimentacao com as novas informações cadastradas, como id.
	 */
	Movimentacao save(Movimentacao movimentacao) throws MovimentacaoException;

	/**
	 * Método usado para salvar uma lista de movimentacaos.
	 * @return Movimentacaos com as novas informações cadastradas, como id.
	 */
	List<Movimentacao> save(List<Movimentacao> movimentacaos) throws MovimentacaoException;
	
	/**
	 * Método usado para atualizar uma movimentacao.
	 */
	Movimentacao update(Long id, Movimentacao movimentacaoNew) throws MovimentacaoException;
	
	/**
	 * Método usado para remover uma movimentacao.
	 */
	void delete(Long id) throws MovimentacaoException;

}
