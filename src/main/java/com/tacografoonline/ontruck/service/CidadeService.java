package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.service.exception.CidadeException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface CidadeService {
	
	/**
	 * Método usado para buscar todos os cidades.
	 */
	List<Cidade> findAll() throws CidadeException;
	
	/**
	 * Método usado para buscar todos os cidades organizados pela paginação.
	 */
	Page<Cidade> findAll(Pageable pageable) throws CidadeException;
	
	/**
	 * Método usado para buscar todas as cidades pelos atributos e organizados pela paginação.
	 */
	Page<Cidade> findAll(Optional<String> nome, Optional<String> estadoNome, Pageable pageable) throws CidadeException;
	
	/**
	 * Método usado para buscar um cidade pelo nome e também por paginação.
	 */
	Page<Cidade> findAllByNome(Optional<String> nome, Pageable pageable) throws CidadeException;

	/**
	 * Método usado para buscar um cidade pelo nome do estado e também por paginação.
	 */
	Page<Cidade> findAllByEstadoNome(Optional<String> estadoNome, Pageable pageable) throws CidadeException;

	/**
	 * Método usado para buscar um cidade pelo nome do estado, nome da cidade e também por paginação.
	 */
	Page<Cidade> findAllByNomeAndEstadoNome(Optional<String> nome, Optional<String> estadoNome, Pageable pageable) throws CidadeException;
	
	/**
	 * Método usado para buscar um cidade pela chave primária.
	 */
	Optional<Cidade> findOne(Long id) throws CidadeException;
	
	/**
	 * Método usado para salvar um cidade.
	 */
	Cidade saveOne(Cidade cidade) throws CidadeException;
	
	/**
	 * Método usado para salvar uma lista de cidades.
	 */
	List<Cidade> saveAll(List<Cidade> cidades) throws CidadeException;
	
	/**
	 * Método usado para atualizar um cidade.
	 */
	Cidade update(Long id, Cidade cidadeNew) throws CidadeException;
	
	/**
	 * Método usado para remover um cidade.
	 */
	void delete(Long id) throws CidadeException;

}
