package com.tacografoonline.ontruck.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.MotoristaIdentificacao;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;

public interface MovimentacaoService {

	public Movimentacao save(Movimentacao movimentacao, MotoristaIdentificacao identificacao) throws MovimentacaoException;

	public List<Movimentacao> findAll() throws MovimentacaoException;

	public Page<Movimentacao> findAll(Pageable pageable) throws MovimentacaoException;

	public List<Movimentacao> findAllByViagemId(Long viagemId) throws MovimentacaoException;

	public Page<Movimentacao> findAllByViagemId(Long viagemId, Pageable pageable) throws MovimentacaoException;

}