package com.tacografoonline.ontruck.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Config;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Role;
import com.tacografoonline.ontruck.model.Usuario;
import com.tacografoonline.ontruck.repository.ConfigRepository;
import com.tacografoonline.ontruck.service.CidadeService;
import com.tacografoonline.ontruck.service.ConfigService;
import com.tacografoonline.ontruck.service.EstadoService;
import com.tacografoonline.ontruck.service.MotoristaService;
import com.tacografoonline.ontruck.service.RoleService;
import com.tacografoonline.ontruck.service.UsuarioService;
import com.tacografoonline.ontruck.service.exception.ConfigException;

@Service
public class ConfigServiceImpl implements ConfigService {
	
	@Autowired
	ConfigRepository configRepository;

	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	EstadoService estadoService;

	@Autowired
	CidadeService cidadeService;

	@Autowired
	MotoristaService motoristaService;

	public ConfigServiceImpl(ConfigRepository configRepository) {
		this.configRepository = configRepository;
	}

	@Override
	public void fillDB() throws ConfigException {
		try {
			List<Role> roles = fillRoles();
			fillUsers(roles);
			List<Estado> estados = fillEstados();
			fillCidades(estados);
			fillMotoristas();

			fillConfig();

		} catch (Exception e) {
			throw new ConfigException("Não foi possível preencher o banco de dados. " + e.getMessage());
		}
	}

	private void fillConfig() throws ConfigException {
		long idConfig1 = Long.valueOf(1);
		Optional<Config> config1Old = this.findById(idConfig1);
		if (config1Old.isEmpty()) {
			configRepository.save(new Config("Quando as informações básicas do banco de dados é preenchida, esse registro fica verdadeiro", true));
		} else {
			config1Old.get().setTrueOrFalse(true);
			this.update(idConfig1, config1Old.get());
		}
	}

	private List<Role> fillRoles() throws ConfigException {
		List<Role> roles = new ArrayList<>();
		roles.add(new Role("ROLE_ADMIN"));
		roles.add(new Role("ROLE_USER"));
		List<Role> rolesToSave = new ArrayList<>();
		try {
			for (Role r: roles) {
				Optional<Role> role = roleService.findByNome(r.getNome());
				if (role.isEmpty()) {
					rolesToSave.add(r);
				}
			};
			return roleService.saveAll(rolesToSave);
		} catch (Exception e) {
			throw new ConfigException("Não foi possível preencher perfis. " + e.getMessage());
		}
	}

	private void fillUsers(List<Role> roles) throws ConfigException {
		try {
			Optional<Usuario> usuario = usuarioService.findByEmail("admin@ontruck.com");
			if (usuario.isEmpty()) {
				usuario = Optional.of(new Usuario("Admin", "admin@ontruck.com", "admin"));
			}
			List<Role> rolesUserSaved = usuario.get().getRoles();
			List<Role> rolesUserUnsaved = roles.stream().filter(r -> !rolesUserSaved.contains(r)).collect(Collectors.toList());
			rolesUserSaved.addAll(rolesUserUnsaved);
			usuario.get().setRoles(rolesUserSaved);
			usuarioService.save(usuario.get());
		} catch (Exception e) {
			throw new ConfigException("Não foi possível preencher usuários. " + e.getMessage());
		}
	}

	private List<Estado> fillEstados() throws ConfigException {

		List<Estado> estadosUnsaved = new ArrayList<>();
		estadosUnsaved.add(new Estado("Ceará"));
		estadosUnsaved.add(new Estado("Rio Grande do Norte"));

		try {
			List<Estado> estadosSaved = estadoService.findAll();

			estadosUnsaved = estadosUnsaved.stream()
					.filter(eu -> estadosSaved.stream()
							.filter(es -> eu.getNome().equals(es.getNome())).findAny().isEmpty()
							).collect(Collectors.toList());
			estadoService.saveAll(estadosUnsaved);
			estadosSaved.addAll(estadosUnsaved);
			return estadosSaved;
		} catch (Exception e) {
			throw new ConfigException("Não foi possível preencher estados. " + e.getMessage());
		}
	}

	private List<Cidade> fillCidades(List<Estado> estados) throws ConfigException {

		Optional<Estado> ceara = estados.stream().filter(e -> e.getNome().equals("Ceará")).findAny();
		Optional<Estado> rgn = estados.stream().filter(e -> e.getNome().equals("Rio Grande do Norte")).findAny();
		
		List<Cidade> cidadesUnsaved = new ArrayList<>();
		cidadesUnsaved.add(new Cidade("Fortaleza", ceara.get()));
		cidadesUnsaved.add(new Cidade("Sobral", ceara.get()));
		cidadesUnsaved.add(new Cidade("Natal", rgn.get()));
			
		try {
			List<Cidade> cidadesSaved = cidadeService.findAll();

			cidadesUnsaved = cidadesUnsaved.stream()
					.filter(cu -> cidadesSaved.stream()
							.filter(cs -> cu.getNome().equals(cs.getNome())).findAny().isEmpty()
					).collect(Collectors.toList());

			cidadeService.saveAll(cidadesUnsaved);
			cidadesSaved.addAll(cidadesUnsaved);
			return cidadesSaved;

		} catch (Exception e) {
			throw new ConfigException("Não foi possível preencher estados. " + e.getMessage());
		}
	}

	private List<Motorista> fillMotoristas() throws ConfigException {

		List<Motorista> motoristasUnsaved = new ArrayList<>();
		motoristasUnsaved.add(new Motorista("024", "João"));
		motoristasUnsaved.add(new Motorista("812", "Pedro"));
		
		List<Motorista> motoristasSaved = new ArrayList<>();

		try {
			for (int i = 0; i < motoristasUnsaved.size(); i++) {
				Motorista mu = motoristasUnsaved.get(i);
				Optional<Motorista> motorista = motoristaService.findOneByCpf(mu.getCpf());
				if (motorista.isPresent()) {
					motoristasSaved.add(motorista.get());
				}
			};
			
			motoristasUnsaved = motoristasUnsaved.stream()
					.filter(mu -> motoristasSaved.stream()
							.filter(ms -> mu.getNome().equals(ms.getNome())).findAny().isEmpty()
					).collect(Collectors.toList());
			
			motoristaService.saveAll(motoristasUnsaved);
			motoristasSaved.addAll(motoristasUnsaved);
			return motoristasSaved;

		} catch (Exception e) {
			throw new ConfigException("Não foi possível preencher motoristas. " + e.getMessage());
		}
	}

	@Override
	public Optional<Config> findById(Long id) throws ConfigException {
		try {
			return configRepository.findById(id);
		} catch (Exception e) {
			throw new ConfigException("Não foi possível buscar a configuração. " + e.getMessage());
		}
	}

	@Override
	public Config save(Config configuracao) throws ConfigException {
		try {
			return configRepository.save(configuracao);
		} catch (Exception e) {
			throw new ConfigException("Não foi possível salvar. " + e.getMessage());
		}
	}

	private void updateOldFields(Config configuracaoNew, Config configuracaoOld) {
		configuracaoOld.setTrueOrFalse(configuracaoNew.getTrueOrFalse());
	}

	@Override
	public Config update(Long id, Config configuracaoNew) throws ConfigException {
		try {
			Optional<Config> configuracaoOld = configRepository.findById(id);
			if (configuracaoOld.isEmpty()) {
				throw new ConfigException("Essa Config não existe.");
			}
			updateOldFields(configuracaoNew, configuracaoOld.get());
			return configuracaoOld.get();
		} catch (Exception e) {
			throw new ConfigException("Não foi possível atualizar. " + e.getMessage());
		}
	}

}
