package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.repository.CorRepository;
import com.tacografoonline.ontruck.service.CorService;
import com.tacografoonline.ontruck.service.exception.CorException;

@Service
public class CorServiceImpl implements CorService {
	
	@Autowired
	CorRepository corRepository;

	public CorServiceImpl() {
	}

	public CorServiceImpl(CorRepository corRepository) {
		this.corRepository = corRepository;
	}

	private void validateCor(Cor cor) throws CorException {
		if (isNullOrEmpty(cor)) {
			throw new CorException("Cor inválido.");
		}
		validateNome(cor.getNome());
	}

	private void validateId(Long id) throws CorException {
		if (isNullOrEmpty(id)) {
			throw new CorException("Id inválido.");
		}
	}

	private void validateNome(String nome) throws CorException {
		if (isNullOrEmpty(nome)) {
			throw new CorException("Nome inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws CorException {
		if (isNullOrEmpty(pageable)) {
			throw new CorException("Paginação inválida.");
		}
	}

	@Override
	public List<Cor> findAll() throws CorException {
		try {
			return corRepository.findAll();
		} catch (Exception e) {
			throw new CorException("Não foi possível buscar as cores. " + e.getMessage());
		}
	}

	@Override
	public Page<Cor> findAll(Pageable pageable) throws CorException {
		try {
			validatePageable(pageable);
			return corRepository.findAll(pageable);
		} catch (Exception e) {
			throw new CorException("Não foi possível buscar as cores. " + e.getMessage());
		}
	}

	@Override
	public Page<Cor> findAllByNome(String nome, Pageable pageable) throws CorException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return corRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new CorException("Não foi possível buscar as cores pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Cor> findOneById(Long id) throws CorException {
		try {
			validateId(id);
			return corRepository.findById(id);
		} catch (Exception e) {
			throw new CorException("Não foi possível buscar o cor pelo id. " + e.getMessage());
		}
	}

	@Override
	public Optional<Cor> findOneByNome(String nome) throws CorException {
		try {
			validateNome(nome);
			return corRepository.findByNome(nome);
		} catch (Exception e) {
			throw new CorException("Não foi possível buscar o cor pelo id. " + e.getMessage());
		}
	}

	@Override
	public Cor save(Cor cor) throws CorException {
		try {
			validateCor(cor);
			return corRepository.save(cor);
		} catch (Exception e) {
			throw new CorException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Cor update(Long id, Cor corNew) throws CorException {
		try {
			validateId(id);
			validateCor(corNew);
			Optional<Cor> cor = corRepository.findById(id);
			if (cor.isEmpty())
				throw new CorException("Não foi possível atualizar. Cor não existe");
			cor.get().setNome(corNew.getNome());
			return cor.get();
		} catch (Exception e) {
			throw new CorException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws CorException {
		try {
			validateId(id);
			corRepository.deleteById(id);
		} catch (Exception e) {
			throw new CorException("Não foi possível remover. " + e.getMessage());
		}
	}

}
