package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.repository.EstadoRepository;
import com.tacografoonline.ontruck.service.EstadoService;
import com.tacografoonline.ontruck.service.exception.EstadoException;

@Service
public class EstadoServiceImpl implements EstadoService {
	
	@Autowired
	EstadoRepository estadoRepository;

	public EstadoServiceImpl() {
	}

	public EstadoServiceImpl(EstadoRepository estadoRepository) {
		this.estadoRepository = estadoRepository;
	}

	private void validateEstado(Estado estado) throws EstadoException {
		if (isNullOrEmpty(estado)) {
			throw new EstadoException("Estado inválido.");
		}
		validateNome(estado.getNome());
	}

	private void validateId(Long id) throws EstadoException {
		if (isNullOrEmpty(id)) {
			throw new EstadoException("Id inválido.");
		}
	}

	private void validateNome(String nome) throws EstadoException {
		if (isNullOrEmpty(nome)) {
			throw new EstadoException("Nome inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws EstadoException {
		if (isNullOrEmpty(pageable)) {
			throw new EstadoException("Paginação inválida.");
		}
	}

	@Override
	public List<Estado> findAll() throws EstadoException {
		try {
			return estadoRepository.findAll();
		} catch (Exception e) {
			throw new EstadoException("Não foi possível buscar os estados. " + e.getMessage());
		}
	}

	@Override
	public Page<Estado> findAll(Pageable pageable) throws EstadoException {
		try {
			validatePageable(pageable);
			return estadoRepository.findAll(pageable);
		} catch (Exception e) {
			throw new EstadoException("Não foi possível buscar os estados. " + e.getMessage());
		}
	}

	@Override
	public Page<Estado> findAllByNome(String nome, Pageable pageable) throws EstadoException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return estadoRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new EstadoException("Não foi possível buscar os estados pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Estado> findOne(Long id) throws EstadoException {
		try {
			validateId(id);
			return estadoRepository.findById(id);
		} catch (Exception e) {
			throw new EstadoException("Não foi possível buscar o estado pelo id. " + e.getMessage());
		}
	}

	@Override
	public Optional<Estado> findOneByNome(String nome) throws EstadoException {
		try {
			validateNome(nome);
			return estadoRepository.findByNome(nome);
		} catch (Exception e) {
			throw new EstadoException("Não foi possível buscar o estado pelo id. " + e.getMessage());
		}
	}

	@Override
	public Estado save(Estado estado) throws EstadoException {
		try {
			validateEstado(estado);
			return estadoRepository.save(estado);
		} catch (Exception e) {
			throw new EstadoException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Estado> saveAll(List<Estado> cidades) throws EstadoException {
		try {
			for (Estado cidade : cidades) {
				validateEstado(cidade);
			}
			return estadoRepository.saveAll(cidades);
		}catch (Exception e) {
			throw new EstadoException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Estado update(Long id, Estado estadoNew) throws EstadoException {
		try {
			validateId(id);
			validateEstado(estadoNew);
			Optional<Estado> estado = estadoRepository.findById(id);
			if (estado.isEmpty())
				throw new EstadoException("Não foi possível atualizar. Estado não existe");
			estado.get().setNome(estadoNew.getNome());
			return estado.get();
		} catch (Exception e) {
			throw new EstadoException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws EstadoException {
		try {
			validateId(id);
			estadoRepository.deleteById(id);
		} catch (Exception e) {
			throw new EstadoException("Não foi possível remover. " + e.getMessage());
		}
	}

}
