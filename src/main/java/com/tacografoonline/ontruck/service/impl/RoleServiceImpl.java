package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Role;
import com.tacografoonline.ontruck.repository.RoleRepository;
import com.tacografoonline.ontruck.service.RoleService;
import com.tacografoonline.ontruck.service.exception.RoleException;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepository;

	public RoleServiceImpl(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	private void validateRole(Role role) throws RoleException {
		if (isNullOrEmpty(role)) {
			throw new RoleException("Usuário inválido.");
		}
		validateNome(role.getNome());
	}

	private void validateId(Long id) throws RoleException {
		if (isNullOrEmpty(id)) {
			throw new RoleException("Id inválida.");
		}
	}

	private void validateNome(String nome) throws RoleException {
		if (isNullOrEmpty(nome)) {
			throw new RoleException("Nome inválido.");
		}
	}

	@Override
	public List<Role> findAll() throws RoleException {
		try {
			return roleRepository.findAll();
		} catch (Exception e) {
			throw new RoleException("Não foi possível buscar os usuários. " + e.getMessage());
		}
	}

	@Override
	public Optional<Role> findOneById(Long id) throws RoleException {
		try {
			validateId(id);
			return roleRepository.findById(id);
		} catch (Exception e) {
			throw new RoleException("Não foi possível buscar o role pelo id. " + e.getMessage());
		}
	}

	@Override
	public Optional<Role> findByNome(String nome) throws RoleException {
		try {
			validateNome(nome);
			return roleRepository.findByNome(nome);
		} catch (Exception e) {
			throw new RoleException("Não foi possível buscar os usuários pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Role save(Role role) throws RoleException {
		try {
			validateRole(role);
			return roleRepository.save(role);
		} catch (Exception e) {
			throw new RoleException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Role> saveAll(List<Role> roles) throws RoleException {
		try {
			for (Role role : roles) {
				validateRole(role);
			}
			return roleRepository.saveAll(roles);
		} catch (Exception e) {
			throw new RoleException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Role update(Long id, Role roleNew) throws RoleException {
		try {
			validateId(id);
			validateRole(roleNew);
			Optional<Role> role = roleRepository.findById(id);
			if (role.isEmpty())
				throw new RoleException("Não foi possível atualizar. Role não existe");
			role.get().setNome(roleNew.getNome());
			return role.get();
		} catch (Exception e) {
			throw new RoleException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws RoleException {
		try {
			validateId(id);
			roleRepository.deleteById(id);
		} catch (Exception e) {
			throw new RoleException("Não foi possível remover. " + e.getMessage());
		}
	}

}
