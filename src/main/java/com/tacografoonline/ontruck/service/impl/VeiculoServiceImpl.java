package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.repository.VeiculoRepository;
import com.tacografoonline.ontruck.service.VeiculoService;
import com.tacografoonline.ontruck.service.exception.MarcaException;
import com.tacografoonline.ontruck.service.exception.VeiculoException;

@Service
public class VeiculoServiceImpl implements VeiculoService {

	@Autowired
	VeiculoRepository veiculoRepository;

	public VeiculoServiceImpl(VeiculoRepository veiculoRepository) {
		this.veiculoRepository = veiculoRepository;
	}

	private void validateVeiculo(Veiculo veiculo) throws VeiculoException {
		if (isNullOrEmpty(veiculo)) {
			throw new VeiculoException("Movimentação inválida.");
		}
		validatePlaca(veiculo.getPlaca());
		validateCor(veiculo.getCor());
		validateModelo(veiculo.getModelo());
		validateAno(veiculo.getAno());
		validateKmTotal(veiculo.getKmTotal());
	}

	private void validateId(Long id) throws VeiculoException {
		if (isNullOrEmpty(id)) {
			throw new VeiculoException("Id inválida.");
		}
	}

	private void validatePlaca(String placa) throws VeiculoException {
		if (isNullOrEmpty(placa)) {
			throw new VeiculoException("Placa inválida.");
		}
	}

	private void validateCor(Cor cor) throws VeiculoException {
		if (isNullOrEmpty(cor) || isNullOrEmpty(cor.getId())) {
			throw new VeiculoException("Cor inválida.");
		}
	}

	private void validateModelo(Modelo modelo) throws VeiculoException {
		if (isNullOrEmpty(modelo) || isNullOrEmpty(modelo.getId())) {
			throw new VeiculoException("Modelo inválido.");
		}
	}

	private void validateAno(Integer ano) throws VeiculoException {
		if (isNullOrEmpty(ano)) {
			throw new VeiculoException("Ano inválida.");
		}
	}

	private void validateKmTotal(Integer kmTotal) throws VeiculoException {
		if (isNullOrEmpty(kmTotal)) {
			throw new VeiculoException("KmTotal inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws VeiculoException {
		if (isNullOrEmpty(pageable)) {
			throw new VeiculoException("Paginação inválida.");
		}
	}

	@Override
	public List<Veiculo> findAll() throws VeiculoException {
		try {
			return veiculoRepository.findAll();
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível buscar os veiculos. " + e.getMessage());
		}
	}

	@Override
	public Page<Veiculo> findAll(Pageable pageable) throws VeiculoException {
		try {
			validatePageable(pageable);
			return veiculoRepository.findAll(pageable);
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível buscar os veiculos. " + e.getMessage());
		}
	}

//	@Override
//	public Page<Veiculo> findAllByPlaca(String placa, Pageable pageable)
//			throws VeiculoException {
//		try {
//			validatePlaca(placa);
//			validatePageable(pageable);
//			return veiculoRepository.findByPlaca(placa, pageable);
//		} catch (Exception e) {
//			throw new VeiculoException("Não foi possível buscar os veiculos pelo nome. " + e.getMessage());
//		}
//	}

//	public Page<Veiculo> findByCorNome(String nome, Pageable pageable);
//
//	public Page<Veiculo> findByModeloNome(String nome, Pageable pageable);
//
//	public Page<Veiculo> findByAno(Integer ano, Pageable pageable);
//
//	@Query("SELECT v FROM Veiculo v WHERE v.kmTotal <= :kmTotal")
//	public Page<Veiculo> findWhereKmTotalEqualsOrGreaterThanX(Integer kmTotal, Pageable pageable);
//
//	public Optional<Veiculo> findByPlaca(String placa);

	@Override
	public Optional<Veiculo> findOneById(Long id) throws VeiculoException {
		try {
			validateId(id);
			return veiculoRepository.findById(id);
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível buscar o veiculo pelo id. " + e.getMessage());
		}
	}

	@Override
	public Page<Veiculo> findByPlaca(String placa, Pageable pageable)
			throws VeiculoException {
		try {
			validatePlaca(placa);
			return veiculoRepository.findByPlaca(placa, pageable);
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível buscar os veiculos pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Veiculo save(Veiculo veiculo) throws VeiculoException {
		try {
			validateVeiculo(veiculo);
			return veiculoRepository.save(veiculo);
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Veiculo> save(List<Veiculo> veiculos) throws VeiculoException {
		try {
			for (Veiculo veiculo : veiculos) {
				validateVeiculo(veiculo);
			}
			return veiculoRepository.saveAll(veiculos);
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Veiculo update(Long id, Veiculo veiculoNew) throws VeiculoException {
		try {
			validateId(id);
			validateVeiculo(veiculoNew);
			Optional<Veiculo> veiculo = veiculoRepository.findById(id);
			if (veiculo.isEmpty())
				throw new MarcaException("Não foi possível atualizar. Veiculo não existe");
			veiculo.get().setPlaca(veiculoNew.getPlaca());
			veiculo.get().setCor(veiculoNew.getCor());
			veiculo.get().setModelo(veiculoNew.getModelo());
			veiculo.get().setAno(veiculoNew.getAno());
			veiculo.get().setKmTotal(veiculoNew.getKmTotal());
			return veiculo.get();
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Veiculo updateKmTotal(Long id, Integer kmTotal) throws VeiculoException {
		try {
			validateId(id);
			validateKmTotal(kmTotal);
			Optional<Veiculo> veiculo = veiculoRepository.findById(id);
			if (veiculo.isEmpty())
				throw new MarcaException("Não foi possível atualizar. Veiculo não existe");
			veiculo.get().setKmTotal(kmTotal);
			return veiculo.get();
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws VeiculoException {
		try {
			validateId(id);
			veiculoRepository.deleteById(id);
		} catch (Exception e) {
			throw new VeiculoException("Não foi possível remover. " + e.getMessage());
		}
	}

}
