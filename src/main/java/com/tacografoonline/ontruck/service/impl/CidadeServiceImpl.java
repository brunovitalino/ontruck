package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.repository.CidadeRepository;
import com.tacografoonline.ontruck.service.CidadeService;
import com.tacografoonline.ontruck.service.EstadoService;
import com.tacografoonline.ontruck.service.exception.CidadeException;
import com.tacografoonline.ontruck.service.exception.EstadoException;

@Service
public class CidadeServiceImpl implements CidadeService {
	
	@Autowired
	CidadeRepository cidadeRepository;
	
	@Autowired
	EstadoService estadoService;
	
	public CidadeServiceImpl(CidadeRepository cidadeRepository) {
		this.cidadeRepository = cidadeRepository;
	}

	private void validateCidade(Cidade cidade) throws CidadeException {
		if (isNullOrEmpty(cidade)) {
			throw new CidadeException("Cidade inválida.");
		}
		validateNome(Optional.ofNullable(cidade.getNome()));
		validateEstado(cidade);
	}

	private void validateId(Long id) throws CidadeException {
		if (isNullOrEmpty(id)) {
			throw new CidadeException("Id inválido.");
		}
	}

	private void validateNome(Optional<String> nome) throws CidadeException {
		if (nome.isEmpty()) {
			throw new CidadeException("Nome inválido.");
		}
	}

	private void validateEstado(Cidade cidade) throws CidadeException {
		if (isNullOrEmpty(cidade.getEstado()) || isNullOrEmpty(cidade.getEstado().getId())) {
			throw new CidadeException("Estado inválido.");
		}
	}

	private void validateEstadoNome(Optional<String> estadoNome) throws CidadeException {
		if (estadoNome.isEmpty()) {
			throw new CidadeException("Nome de Estado inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws CidadeException {
		if (isNullOrEmpty(pageable)) {
			throw new CidadeException("Paginação inválida.");
		}
	}

	@Override
	public List<Cidade> findAll() throws CidadeException {
		try {
			return cidadeRepository.findAll();
		} catch (Exception e) {
			throw new CidadeException("Não foi possível buscar todas as cidades. " + e.getMessage());
		}
	}

	@Override
	public Page<Cidade> findAll(Optional<String> nome, Optional<String> estadoNome, Pageable pageable) throws CidadeException {
		if (nome.isPresent())
			if (estadoNome.isPresent())
//				Sort sort = Sort.by(Direction.ASC, "estado.nome").and(Sort.by(Direction.ASC, "nome"));
//				pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
				return this.findAllByNomeAndEstadoNome(estadoNome, nome, pageable);
			else
				return this.findAllByNome(nome, pageable);
		else
			if (estadoNome.isPresent())
				return this.findAllByEstadoNome(estadoNome, pageable);
			else
				return this.findAll(pageable);
	}

	@Override
	public Page<Cidade> findAll(Pageable pageable) throws CidadeException {
		try {
			validatePageable(pageable);
			return cidadeRepository.findAll(pageable);
		} catch (Exception e) {
			throw new CidadeException("Não foi possível buscar todas as cidades. " + e.getMessage());
		}
	}

	@Override
	public Page<Cidade> findAllByNome(Optional<String> nome, Pageable pageable) throws CidadeException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return cidadeRepository.findByEstadoNome(nome.get(), pageable);
		} catch (Exception e) {
			throw new CidadeException("Não foi possível buscar todas pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Page<Cidade> findAllByEstadoNome(Optional<String> estadoNome, Pageable pageable) throws CidadeException {
		try {
			validateEstadoNome(estadoNome);
			validatePageable(pageable);
			return cidadeRepository.findByEstadoNome(estadoNome.get(), pageable);
		} catch (Exception e) {
			throw new CidadeException("Não foi possível buscar todas pelo nome do estado. " + e.getMessage());
		}
	}

	@Override
	public Page<Cidade> findAllByNomeAndEstadoNome(Optional<String> nome, Optional<String> estadoNome, Pageable pageable) throws CidadeException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return cidadeRepository.findAllByNomeAndEstadoNome(nome.get(), estadoNome.get(), pageable);
		} catch (Exception e) {
			throw new CidadeException("Não foi possível buscar todas pelo nome e nome do estado. " + e.getMessage());
		}
	}

	@Override
	public Optional<Cidade> findOne(Long id) throws CidadeException {
		try {
			validateId(id);
			return cidadeRepository.findById(id);
		} catch (Exception e) {
			throw new CidadeException("Não foi possível buscar pelo id. " + e.getMessage());
		}
	}

	@Override
	public Cidade saveOne(Cidade cidade) throws CidadeException {
		try {
			validateCidade(cidade);
			return cidadeRepository.save(cidade);
		} catch (Exception e) {
			throw new CidadeException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Cidade> saveAll(List<Cidade> cidades) throws CidadeException {
		try {
			for (Cidade cidade : cidades) {
				validateCidade(cidade);
			}
			return cidadeRepository.saveAll(cidades);
		}catch (CidadeException e) {
			throw new CidadeException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Cidade update(Long id, Cidade cidadeNew) throws CidadeException {
		try {
			validateId(id);
			validateCidade(cidadeNew);
			Optional<Cidade> cidade = cidadeRepository.findById(id);
			if (cidade.isEmpty())
				throw new EstadoException("Não foi possível atualizar. Cidade não existe");
			cidade.get().setNome(cidadeNew.getNome());
			cidade.get().setEstado(cidadeNew.getEstado());
			return cidade.get();
		} catch (Exception e) {
			throw new CidadeException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws CidadeException {
		try {
			validateId(id);
			if (cidadeRepository.existsById(id)) {
				cidadeRepository.deleteById(id);
			}
		} catch (Exception e) {
			throw new CidadeException("Não foi possível remover. " + e.getMessage());
		}
	}

}
