package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.enums.MovendoParadoEnum;
import com.tacografoonline.ontruck.repository.MovimentacaoRepository;
import com.tacografoonline.ontruck.service.MovimentacaoServiceOLD;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;

@Deprecated
@Service
public class MovimentacaoServiceImplOLD implements MovimentacaoServiceOLD {

	@Autowired
	MovimentacaoRepository movimentacaoRepository;

	public MovimentacaoServiceImplOLD(MovimentacaoRepository movimentacaoRepository) {
		this.movimentacaoRepository = movimentacaoRepository;
	}

	private void validateMovimentacao(Movimentacao movimentacao) throws MovimentacaoException {
		if (isNullOrEmpty(movimentacao)) {
			throw new MovimentacaoException("Movimentação inválida.");
		}
		validateStatus(movimentacao.getStatus());
	}

	private void validateId(Long id) throws MovimentacaoException {
		if (isNullOrEmpty(id)) {
			throw new MovimentacaoException("Id inválida.");
		}
	}

	private void validateDate(LocalDate data) throws MovimentacaoException {
		if (isNullOrEmpty(data)) {
			throw new MovimentacaoException("Data inválida.");
		}
	}

	private void validateStatus(MovendoParadoEnum status) throws MovimentacaoException {
		if (isNullOrEmpty(status)) {
			throw new MovimentacaoException("Status inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws MovimentacaoException {
		if (isNullOrEmpty(pageable)) {
			throw new MovimentacaoException("Parâmetro de entrada PAGEABLE está nulo.");
		}
	}

	@Override
	public List<Movimentacao> findAll() throws MovimentacaoException {
		try {
			return movimentacaoRepository.findAll();
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar os movimentacaos. " + e.getMessage());
		}
	}

	@Override
	public Page<Movimentacao> findAll(Pageable pageable) throws MovimentacaoException {
		try {
			validatePageable(pageable);
			return movimentacaoRepository.findAll(pageable);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar os movimentacaos. " + e.getMessage());
		}
	}

	@Override
	public Page<Movimentacao> findAllByData(LocalDate dataInicio, LocalDate dataFim, Pageable pageable)
			throws MovimentacaoException {
		try {
			validateDate(dataInicio);
			validateDate(dataFim);
			validatePageable(pageable);
			return movimentacaoRepository.findAllByDataBetween(dataInicio.atStartOfDay(),
					dataFim.plusDays(1).atStartOfDay(), pageable);
//			return movimentacaoRepository.findAll(pageable);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar os movimentacaos pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Movimentacao> findOneById(Long id) throws MovimentacaoException {
		try {
			validateId(id);
			return movimentacaoRepository.findById(id);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar o movimentacao pelo id. " + e.getMessage());
		}
	}

	@Override
	public Movimentacao save(Movimentacao movimentacao) throws MovimentacaoException {
		try {
			validateMovimentacao(movimentacao);
			return movimentacaoRepository.save(movimentacao);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Movimentacao> save(List<Movimentacao> movimentacaos) throws MovimentacaoException {
		try {
			for (Movimentacao movimentacao : movimentacaos) {
				validateMovimentacao(movimentacao);
			}
			return movimentacaoRepository.saveAll(movimentacaos);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Movimentacao update(Long id, Movimentacao movimentacaoNew) throws MovimentacaoException {
		try {
			validateId(id);
			validateMovimentacao(movimentacaoNew);
			Optional<Movimentacao> movimentacao = movimentacaoRepository.findById(id);
			if (movimentacao.isPresent())
				movimentacao.get().setHorario(movimentacaoNew.getHorario());
			return movimentacao.get();
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws MovimentacaoException {
		try {
			validateId(id);
			movimentacaoRepository.deleteById(id);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível remover. " + e.getMessage());
		}
	}

}
