package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.repository.ViagemRepository;
import com.tacografoonline.ontruck.service.ViagemService;
import com.tacografoonline.ontruck.service.exception.ViagemException;

@Service
public class ViagemServiceImpl implements ViagemService {

	@Autowired
	ViagemRepository viagemRepository;

	public ViagemServiceImpl(ViagemRepository viagemRepository) {
		this.viagemRepository = viagemRepository;
	}

	private void validateViagem(Viagem viagem) throws ViagemException {
		if (isNullOrEmpty(viagem)) {
			throw new ViagemException("Viagem inválido.");
		}
		validateData(viagem.getData());
	}

	private void validateId(Long id) throws ViagemException {
		if (isNullOrEmpty(id)) {
			throw new ViagemException("Id inválido.");
		}
	}

	private void validateData(LocalDate data) throws ViagemException {
		if (isNullOrEmpty(data)) {
			throw new ViagemException("Data inválida.");
		}
	}

	private void validateVeiculoPlaca(String veiculoPlaca) throws ViagemException {
		if (isNullOrEmpty(veiculoPlaca)) {
			throw new ViagemException("Placa de veículo inválida.");
		}
	}

	private void validateHorarioPartida(LocalDateTime horarioPartida) throws ViagemException {
		if (isNullOrEmpty(horarioPartida)) {
			throw new ViagemException("Horario de Partida inválido.");
		}
	}

	private void validateHorarioChegada(LocalDateTime horarioChegada) throws ViagemException {
		if (isNullOrEmpty(horarioChegada)) {
			throw new ViagemException("Horario de Chegada inválido.");
		}
	}

	private void validateKmTotalPartida(Integer KmTotalPartida) throws ViagemException {
		if (isNullOrEmpty(KmTotalPartida)) {
			throw new ViagemException("Km Total de Partida inválido.");
		}
	}

	private void validateKmTotalChegada(Integer KmTotalChegada) throws ViagemException {
		if (isNullOrEmpty(KmTotalChegada)) {
			throw new ViagemException("Km Total de Chegada inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws ViagemException {
		if (isNullOrEmpty(pageable)) {
			throw new ViagemException("Paginação inválida.");
		}
	}

	@Override
	public List<Viagem> findAll() throws ViagemException {
		try {
			return viagemRepository.findAll();
		} catch (Exception e) {
			throw new ViagemException("Não foi possível buscar os viagens. " + e.getMessage());
		}
	}

	@Override
	public Page<Viagem> findAll(Pageable pageable) throws ViagemException {
		try {
			validatePageable(pageable);
			return viagemRepository.findAll(pageable);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível buscar os viagens. " + e.getMessage());
		}
	}

	@Override
	public Page<Viagem> findAllByData(LocalDate data, Pageable pageable) throws ViagemException {
		try {
			validateData(data);
			validatePageable(pageable);
			return viagemRepository.findByData(data, pageable);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível buscar os viagens pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Page<Viagem> findAllByVeiculoPlaca(String veiculoPlaca, Pageable pageable) throws ViagemException {
		try {
			validateVeiculoPlaca(veiculoPlaca);
			validatePageable(pageable);
			return viagemRepository.findByVeiculoPlaca(veiculoPlaca, pageable);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível buscar os viagens pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Viagem> findOneById(Long id) throws ViagemException {
		try {
			validateId(id);
			return viagemRepository.findById(id);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível buscar o viagem pelo id. " + e.getMessage());
		}
	}

	@Override
	public Viagem save(Viagem viagem) throws ViagemException {
		try {
			validateViagem(viagem);
			return viagemRepository.save(viagem);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Viagem> save(List<Viagem> viagens) throws ViagemException {
		try {
			for (Viagem viagem : viagens) {
				validateViagem(viagem);
			}
			return viagemRepository.saveAll(viagens);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Viagem update(Long id, Viagem viagemNew) throws ViagemException {
		try {
			validateId(id);
			validateViagem(viagemNew);
			Optional<Viagem> viagem = viagemRepository.findById(id);
			if (viagem.isEmpty())
				throw new ViagemException("Não foi possível atualizar. Viagem não existe");
			viagem.get().setData(viagemNew.getData());
			viagem.get().setMotorista1(viagemNew.getMotorista1());
			viagem.get().setMotorista2(viagemNew.getMotorista2());
			viagem.get().setVeiculo(viagemNew.getVeiculo());
			viagem.get().setOrigem(viagemNew.getOrigem());
			viagem.get().setDestino(viagemNew.getDestino());
			viagem.get().setHorarioPartida(viagemNew.getHorarioPartida());
			viagem.get().setHorarioChegada(viagemNew.getHorarioChegada());
			viagem.get().setKmTotalPartida(viagemNew.getKmTotalPartida());
			viagem.get().setKmTotalChegada(viagemNew.getKmTotalChegada());
			return viagem.get();
		} catch (Exception e) {
			throw new ViagemException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Viagem updateHorarioPartida(Long id, LocalDateTime horarioPartida) throws ViagemException {
		try {
			validateId(id);
			validateHorarioPartida(horarioPartida);
			Optional<Viagem> viagem = viagemRepository.findById(id);
			if (viagem.isEmpty())
				throw new ViagemException("Não foi possível atualizar. Viagem não existe");
			viagem.get().setHorarioPartida(horarioPartida);
			return viagem.get();
		} catch (Exception e) {
			throw new ViagemException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Viagem updateHorarioChegada(Long id, LocalDateTime horarioChegada) throws ViagemException {
		try {
			validateId(id);
			validateHorarioChegada(horarioChegada);
			Optional<Viagem> viagem = viagemRepository.findById(id);
			if (viagem.isEmpty())
				throw new ViagemException("Não foi possível atualizar. Viagem não existe");
			viagem.get().setHorarioChegada(horarioChegada);
			return viagem.get();
		} catch (Exception e) {
			throw new ViagemException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Viagem updateKmTotalPartida(Long id, Integer kmTotalPartida) throws ViagemException {
		try {
			validateId(id);
			validateKmTotalPartida(kmTotalPartida);
			Optional<Viagem> viagem = viagemRepository.findById(id);
			if (viagem.isEmpty())
				throw new ViagemException("Não foi possível atualizar. Viagem não existe");
			viagem.get().setKmTotalPartida(kmTotalPartida);
			return viagem.get();
		} catch (Exception e) {
			throw new ViagemException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Viagem updateKmTotalChegada(Long id, Integer kmTotalChegada) throws ViagemException {
		try {
			validateId(id);
			validateKmTotalChegada(kmTotalChegada);
			Optional<Viagem> viagem = viagemRepository.findById(id);
			if (viagem.isEmpty())
				throw new ViagemException("Não foi possível atualizar. Viagem não existe");
			viagem.get().setKmTotalChegada(kmTotalChegada);
			return viagem.get();
		} catch (Exception e) {
			throw new ViagemException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws ViagemException {
		try {
			validateId(id);
			viagemRepository.deleteById(id);
		} catch (Exception e) {
			throw new ViagemException("Não foi possível remover. " + e.getMessage());
		}
	}

}
