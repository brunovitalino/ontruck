package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.blockchain.api.controller.dto.BlockchainDTO;
import com.tacografoonline.ontruck.blockchain.api.service.BlockchainService;
import com.tacografoonline.ontruck.controller.dto.MovimentacaoDto;
import com.tacografoonline.ontruck.model.MotoristaIdentificacao;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.service.MovimentacaoService;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;

@Service
public class MovimentacaoServiceImpl implements MovimentacaoService {

	@Autowired
	BlockchainService blockchainService;
	
	private void validateViagem(Viagem viagem) throws MovimentacaoException {
		if (isNullOrEmpty(viagem) || isNullOrEmpty(viagem.getId())) {
			throw new MovimentacaoException("Parâmetro de entrada id de viagem está nulo.");
		}
	}
	
	private void validateHorario(LocalDateTime horario) throws MovimentacaoException {
		if (isNullOrEmpty(horario)) {
			throw new MovimentacaoException("Parâmetro de entrada horário está nulo.");
		}
	}
	
	private void validateVelocidade(Integer velocidade) throws MovimentacaoException {
		if (isNullOrEmpty(velocidade)) {
			throw new MovimentacaoException("Parâmetro de entrada velocidade está nulo.");
		}
	}

	private void validateMovimentacao(Movimentacao movimentacao) throws MovimentacaoException {
		if (isNullOrEmpty(movimentacao)) {
			throw new MovimentacaoException("Parâmetro de entrada MOVIMENTACAO está nulo.");
		}
		validateViagem(movimentacao.getViagem());
		validateHorario(movimentacao.getHorario());
		validateVelocidade(movimentacao.getVelocidade());
	}

	private void validateMotoristaIdentificacao(MotoristaIdentificacao motoristaIdentificacao) throws MovimentacaoException {
		if (isNullOrEmpty(motoristaIdentificacao) || isNullOrEmpty(motoristaIdentificacao.getChavePrivada())) {
			throw new MovimentacaoException("Parâmetro de entrada identificação do motorista está nulo.");
		}
	}

	@Override
	public Movimentacao save(Movimentacao movimentacao, MotoristaIdentificacao identificacao) throws MovimentacaoException {
		try {
			validateMovimentacao(movimentacao);
			validateMotoristaIdentificacao(identificacao);
			BlockchainDTO blockchainDTO = blockchainService.save(movimentacao, identificacao);
			return new MovimentacaoDto(blockchainDTO).toEntity();
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível salvar a movimentacao. " + e.getMessage());
		}
	}
	
	@Override
	public List<Movimentacao> findAll() throws MovimentacaoException {
		try {
			List<BlockchainDTO> blockchainListDTO = blockchainService.findAll();
			return MovimentacaoDto.toEntityListFromBlockchainDtoList(blockchainListDTO);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar todas as movimentacoes. " + e.getMessage());
		}
	}

	@Override
	public Page<Movimentacao> findAll(Pageable pageable) throws MovimentacaoException {
		try {
			List<Movimentacao> movimentacoes = this.findAll();
			return new PageImpl<>(movimentacoes, pageable, movimentacoes.size());
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar todas as movimentacoes paginadas. " + e.getMessage());
		}
	}

	@Override
	public List<Movimentacao> findAllByViagemId(Long viagemId) throws MovimentacaoException {
		try {
			List<BlockchainDTO> blockchainListDTO = blockchainService.findAllByViagemId(viagemId);
			return MovimentacaoDto.toEntityListFromBlockchainDtoList(blockchainListDTO);
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar todas as movimentacoes pelo id de viagem. " + e.getMessage());
		}
	}

	@Override
	public Page<Movimentacao> findAllByViagemId(Long viagemId, Pageable pageable) throws MovimentacaoException {
		try {
			List<Movimentacao> movimentacoes = this.findAllByViagemId(viagemId);
			return new PageImpl<>(movimentacoes, pageable, movimentacoes.size());
		} catch (Exception e) {
			throw new MovimentacaoException("Não foi possível buscar todas as movimentacoes paginadas pelo id de viagem. " + e.getMessage());
		}
	}

}