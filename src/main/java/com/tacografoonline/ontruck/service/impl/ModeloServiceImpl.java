package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.repository.ModeloRepository;
import com.tacografoonline.ontruck.service.ModeloService;
import com.tacografoonline.ontruck.service.MarcaService;
import com.tacografoonline.ontruck.service.exception.ModeloException;
import com.tacografoonline.ontruck.service.exception.MarcaException;

@Service
public class ModeloServiceImpl implements ModeloService {
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	MarcaService marcaService;
	
	public ModeloServiceImpl(ModeloRepository modeloRepository) {
		this.modeloRepository = modeloRepository;
	}

	private void validateModelo(Modelo modelo) throws ModeloException {
		if (isNullOrEmpty(modelo)) {
			throw new ModeloException("Modelo inválida.");
		}
		validateNome(modelo.getNome());
		validateMarca(modelo);
	}

	private void validateId(Long id) throws ModeloException {
		if (isNullOrEmpty(id)) {
			throw new ModeloException("Id inválido.");
		}
	}

	private void validateNome(String nome) throws ModeloException {
		if (isNullOrEmpty(nome)) {
			throw new ModeloException("Nome inválido.");
		}
	}

	private void validateMarca(Modelo modelo) throws ModeloException {
		if (isNullOrEmpty(modelo.getMarca()) || isNullOrEmpty(modelo.getMarca().getId())) {
			throw new ModeloException("Marca inválido.");
		}
	}

	private void validateMarcaNome(String marcaNome) throws ModeloException {
		if (isNullOrEmpty(marcaNome)) {
			throw new ModeloException("Nome de Marca inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws ModeloException {
		if (isNullOrEmpty(pageable)) {
			throw new ModeloException("Paginação inválida.");
		}
	}

	@Override
	public List<Modelo> findAll() throws ModeloException {
		try {
			return modeloRepository.findAll();
		} catch (Exception e) {
			throw new ModeloException("Não foi possível buscar todas as modelos. " + e.getMessage());
		}
	}

	@Override
	public Page<Modelo> findAll(Pageable pageable) throws ModeloException {
		try {
			validatePageable(pageable);
			return modeloRepository.findAll(pageable);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível buscar todas as modelos. " + e.getMessage());
		}
	}

	@Override
	public Page<Modelo> findAllByNome(String nome, Pageable pageable) throws ModeloException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return modeloRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível buscar todas pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Page<Modelo> findAllByMarcaNome(String marcaNome, Pageable pageable) throws ModeloException {
		try {
			validateMarcaNome(marcaNome);
			validatePageable(pageable);
			return modeloRepository.findByMarcaNome(marcaNome, pageable);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível buscar todas pelo nome do marca. " + e.getMessage());
		}
	}

	// TESTAR NO CONTROLLER, ENTRADA DE DADOS NULO, SE FILTRA
	@Override
	public Page<Modelo> findAllByNomeAndMarcaNome(String nome, String marcaNome, Pageable pageable) throws ModeloException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return modeloRepository.findAllByNomeAndMarcaNome(nome, marcaNome, pageable);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível buscar todas pelo nome e nome do marca. " + e.getMessage());
		}
	}

	// TESTAR NO CONTROLLER, ENTRADA DE DADOS NULO, SE FILTRA
	@Override
	public Page<Modelo> findAll(Optional<String> nome, Optional<String> marcaNome, Pageable pageable) throws ModeloException {
		if (nome.isPresent())
			if (marcaNome.isPresent())
//				Sort sort = Sort.by(Direction.ASC, "marca.nome").and(Sort.by(Direction.ASC, "nome"));
//				pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
				return this.findAllByNomeAndMarcaNome(marcaNome.get(), nome.get(), pageable);
			else
				return this.findAllByNome(nome.get(), pageable);
		else
			if (marcaNome.isPresent())
				return this.findAllByMarcaNome(marcaNome.get(), pageable);
			else
				return this.findAll(pageable);
	}

	@Override
	public Optional<Modelo> findOneById(Long id) throws ModeloException {
		try {
			validateId(id);
			return modeloRepository.findById(id);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível buscar pelo id. " + e.getMessage());
		}
	}

	@Override
	public Modelo save(Modelo modelo) throws ModeloException {
		try {
			validateModelo(modelo);
			return modeloRepository.save(modelo);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Modelo> save(List<Modelo> modelos) throws ModeloException {
		try {
			for (Modelo modelo : modelos) {
				validateModelo(modelo);
			}
			return modeloRepository.saveAll(modelos);
		}catch (ModeloException e) {
			throw new ModeloException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Modelo update(Long id, Modelo modeloNew) throws ModeloException {
		try {
			validateId(id);
			validateModelo(modeloNew);
			Optional<Modelo> modelo = modeloRepository.findById(id);
			if (modelo.isEmpty())
				throw new MarcaException("Não foi possível atualizar. Modelo não existe");
			modelo.get().setNome(modeloNew.getNome());
			return modelo.get();
		} catch (Exception e) {
			throw new ModeloException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws ModeloException {
		try {
			validateId(id);
			modeloRepository.deleteById(id);
		} catch (Exception e) {
			throw new ModeloException("Não foi possível remover. " + e.getMessage());
		}
	}

}
