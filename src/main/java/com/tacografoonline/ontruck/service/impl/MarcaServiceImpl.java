package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.repository.MarcaRepository;
import com.tacografoonline.ontruck.service.MarcaService;
import com.tacografoonline.ontruck.service.exception.MarcaException;

@Service
public class MarcaServiceImpl implements MarcaService {
	
	@Autowired
	MarcaRepository marcaRepository;

	public MarcaServiceImpl() {
	}

	public MarcaServiceImpl(MarcaRepository marcaRepository) {
		this.marcaRepository = marcaRepository;
	}

	private void validateMarca(Marca marca) throws MarcaException {
		if (isNullOrEmpty(marca)) {
			throw new MarcaException("Marca inválido.");
		}
		validateNome(marca.getNome());
	}

	private void validateId(Long id) throws MarcaException {
		if (isNullOrEmpty(id)) {
			throw new MarcaException("Id inválido.");
		}
	}

	private void validateNome(String nome) throws MarcaException {
		if (isNullOrEmpty(nome)) {
			throw new MarcaException("Nome inválido.");
		}
	}

	private void validatePageable(Pageable pageable) throws MarcaException {
		if (isNullOrEmpty(pageable)) {
			throw new MarcaException("Paginação inválida.");
		}
	}

	@Override
	public List<Marca> findAll() throws MarcaException {
		try {
			return marcaRepository.findAll();
		} catch (Exception e) {
			throw new MarcaException("Não foi possível buscar as marcaes. " + e.getMessage());
		}
	}

	@Override
	public Page<Marca> findAll(Pageable pageable) throws MarcaException {
		try {
			validatePageable(pageable);
			return marcaRepository.findAll(pageable);
		} catch (Exception e) {
			throw new MarcaException("Não foi possível buscar as marcaes. " + e.getMessage());
		}
	}

	@Override
	public Page<Marca> findAllByNome(String nome, Pageable pageable) throws MarcaException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return marcaRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new MarcaException("Não foi possível buscar as marcaes pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Marca> findOneById(Long id) throws MarcaException {
		try {
			validateId(id);
			return marcaRepository.findById(id);
		} catch (Exception e) {
			throw new MarcaException("Não foi possível buscar o marca pelo id. " + e.getMessage());
		}
	}

	@Override
	public Optional<Marca> findOneByNome(String nome) throws MarcaException {
		try {
			validateNome(nome);
			return marcaRepository.findByNome(nome);
		} catch (Exception e) {
			throw new MarcaException("Não foi possível buscar o marca pelo id. " + e.getMessage());
		}
	}

	@Override
	public Marca save(Marca marca) throws MarcaException {
		try {
			validateMarca(marca);
			return marcaRepository.save(marca);
		} catch (Exception e) {
			throw new MarcaException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Marca update(Long id, Marca marcaNew) throws MarcaException {
		try {
			validateId(id);
			validateMarca(marcaNew);
			Optional<Marca> marca = marcaRepository.findById(id);
			if (marca.isEmpty())
				throw new MarcaException("Não foi possível atualizar. Marca não existe");
			marca.get().setNome(marcaNew.getNome());
			return marca.get();
		} catch (Exception e) {
			throw new MarcaException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws MarcaException {
		try {
			validateId(id);
			marcaRepository.deleteById(id);
		} catch (Exception e) {
			throw new MarcaException("Não foi possível remover. " + e.getMessage());
		}
	}

}
