package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Usuario;
import com.tacografoonline.ontruck.repository.UsuarioRepository;
import com.tacografoonline.ontruck.service.UsuarioService;
import com.tacografoonline.ontruck.service.exception.UsuarioException;
import com.tacografoonline.ontruck.util.OntruckUtil;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	public UsuarioServiceImpl(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}

	private void validateUsuario(Usuario usuario) throws UsuarioException {
		if (isNullOrEmpty(usuario)) {
			throw new UsuarioException("Usuário inválido.");
		}
		validateNome(usuario.getNome());
		validateEmail(usuario.getEmail());
		validateSenha(usuario.getSenha());
	}

	private void validateId(Long id) throws UsuarioException {
		if (isNullOrEmpty(id)) {
			throw new UsuarioException("Id inválida.");
		}
	}

	private void validateNome(String nome) throws UsuarioException {
		if (isNullOrEmpty(nome)) {
			throw new UsuarioException("Nome inválido.");
		}
	}

	private void validateEmail(String email) throws UsuarioException {
		if (isNullOrEmpty(email)) {
			throw new UsuarioException("Email inválido.");
		}
	}

	private void validateSenha(String senha) throws UsuarioException {
		if (isNullOrEmpty(senha)) {
			throw new UsuarioException("Senha inválida.");
		}
	}

	private void validatePageable(Pageable pageable) throws UsuarioException {
		if (isNullOrEmpty(pageable)) {
			throw new UsuarioException("Paginação inválida.");
		}
	}

	@Override
	public List<Usuario> findAll() throws UsuarioException {
		try {
			return usuarioRepository.findAll();
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível buscar os usuários. " + e.getMessage());
		}
	}

	@Override
	public Page<Usuario> findAll(Pageable pageable) throws UsuarioException {
		try {
			validatePageable(pageable);
			return usuarioRepository.findAll(pageable);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível buscar os usuários. " + e.getMessage());
		}
	}

	@Override
	public Page<Usuario> findAllByNome(String nome, Pageable pageable)
			throws UsuarioException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return usuarioRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível buscar os usuários pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Usuario> findOneById(Long id) throws UsuarioException {
		try {
			validateId(id);
			return usuarioRepository.findById(id);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível buscar o usuario pelo id. " + e.getMessage());
		}
	}

	@Override
	public Optional<Usuario> findByNome(String nome)
			throws UsuarioException {
		try {
			validateNome(nome);
			return usuarioRepository.findByNome(nome);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível buscar os usuários pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Usuario> findByEmail(String email)
			throws UsuarioException {
		try {
			validateEmail(email);
			return usuarioRepository.findByEmail(email);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível buscar os usuários pelo email. " + e.getMessage());
		}
	}

	@Override
	public Usuario save(Usuario usuario) throws UsuarioException {
		try {
			validateUsuario(usuario);
			usuario.setSenha(OntruckUtil.codificarSenha(usuario.getSenha()));
			return usuarioRepository.save(usuario);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Usuario> saveAll(List<Usuario> usuarios) throws UsuarioException {
		try {
			for (Usuario usuario : usuarios) {
				validateUsuario(usuario);
				usuario.setSenha(OntruckUtil.codificarSenha(usuario.getSenha()));
			}
			return usuarioRepository.saveAll(usuarios);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Usuario update(Long id, Usuario usuarioNew) throws UsuarioException {
		try {
			validateId(id);
			validateUsuario(usuarioNew);
			Optional<Usuario> usuario = usuarioRepository.findById(id);
			if (usuario.isEmpty())
				throw new UsuarioException("Não foi possível atualizar. Usuario não existe");
			usuario.get().setNome(usuarioNew.getNome());
			usuario.get().setEmail(usuarioNew.getEmail());
			usuario.get().setSenha(OntruckUtil.codificarSenha(usuarioNew.getSenha()));
			return usuario.get();
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Usuario updateEmail(Long id, String email) throws UsuarioException {
		try {
			validateId(id);
			validateEmail(email);
			Optional<Usuario> usuario = usuarioRepository.findById(id);
			if (usuario.isPresent())
				usuario.get().setEmail(email);
			return usuario.get();
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public Usuario updateSenha(Long id, String senha) throws UsuarioException {
		try {
			validateId(id);
			validateSenha(senha);
			Optional<Usuario> usuario = usuarioRepository.findById(id);
			if (usuario.isPresent())
				usuario.get().setSenha(OntruckUtil.codificarSenha(senha));
			return usuario.get();
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws UsuarioException {
		try {
			validateId(id);
			usuarioRepository.deleteById(id);
		} catch (Exception e) {
			throw new UsuarioException("Não foi possível remover. " + e.getMessage());
		}
	}

}
