package com.tacografoonline.ontruck.service.impl;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.repository.MotoristaRepository;
import com.tacografoonline.ontruck.service.MotoristaService;
import com.tacografoonline.ontruck.service.exception.MotoristaException;
import com.tacografoonline.ontruck.util.BlockchainUtil;

@Service
public class MotoristaServiceImpl implements MotoristaService {
	
	@Autowired
	MotoristaRepository motoristaRepository;
	
	public MotoristaServiceImpl(MotoristaRepository motoristaRepository) {
		this.motoristaRepository = motoristaRepository;
	}

	private void validateMotorista(Motorista motorista) throws MotoristaException {
		if (isNullOrEmpty(motorista)) {
			throw new MotoristaException("Parâmetro de entrada ESTADO está nulo.");
		}
		validateCpf(motorista.getCpf());
		validateNome(motorista.getNome());
	}

	private void validateId(Long id) throws MotoristaException {
		if (isNullOrEmpty(id)) {
			throw new MotoristaException("Parâmetro de entrada ID está nulo.");
		}
	}

	private void validateNome(String nome) throws MotoristaException {
		if (isNullOrEmpty(nome)) {
			throw new MotoristaException("Parâmetro de entrada NOME está nulo ou vazio.");
		}
	}

	private void validateChavePrivada(String codigo) throws MotoristaException {
		if (isNullOrEmpty(codigo)) {
			throw new MotoristaException("Parâmetro de entrada CODIGO está nulo ou vazio.");
		}
	}

	private void validateCpf(String cpf) throws MotoristaException {
		if (isNullOrEmpty(cpf)) {
			throw new MotoristaException("Parâmetro de entrada CPF está nulo ou vazio.");
		}
	}

	private void validatePageable(Pageable pageable) throws MotoristaException {
		if (isNullOrEmpty(pageable)) {
			throw new MotoristaException("Parâmetro de entrada PAGEABLE está nulo.");
		}
	}

	@Override
	public List<Motorista> findAll() throws MotoristaException {
		try {
			return motoristaRepository.findAll();
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível buscar os motoristas. " + e.getMessage());
		}
	}

	@Override
	public Page<Motorista> findAll(Pageable pageable) throws MotoristaException {
		try {
			validatePageable(pageable);
			return motoristaRepository.findAll(pageable);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível buscar os motoristas. " + e.getMessage());
		}
	}

	@Override
	public Page<Motorista> findAllByNome(String nome, Pageable pageable) throws MotoristaException {
		try {
			validateNome(nome);
			validatePageable(pageable);
			return motoristaRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível buscar os motoristas pelo nome. " + e.getMessage());
		}
	}

	@Override
	public Optional<Motorista> findOne(Long id) throws MotoristaException {
		try {
			validateId(id);
			return motoristaRepository.findById(id);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível buscar o motorista pelo id. " + e.getMessage());
		}
	}

	@Override
	public Optional<Motorista> findOneByCpf(String cpf) throws MotoristaException {
		try {
			validateCpf(cpf);
			return motoristaRepository.findByCpf(cpf);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível buscar o motorista pelo cpf. " + e.getMessage());
		}
	}

	@Override
	public Optional<Motorista> findOneByChavePrivada(String chavePrivada) throws MotoristaException {
		try {
			validateChavePrivada(chavePrivada);
			return motoristaRepository.findByChavePrivada(chavePrivada);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível buscar o motorista pelo código. " + e.getMessage());
		}
	}

	@Override
	public Motorista save(Motorista motorista) throws MotoristaException {
		try {
			validateMotorista(motorista);
			String chavePrivada;
			Optional<Motorista> motoristaComChavePrivadaExistente;
			do {
				chavePrivada = BlockchainUtil.generatePrivateKey();
				motoristaComChavePrivadaExistente = this.findOneByChavePrivada(chavePrivada);
			} while (motoristaComChavePrivadaExistente.isPresent());
			motorista.setChavePrivada(chavePrivada);
			return motoristaRepository.save(motorista);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public List<Motorista> saveAll(List<Motorista> motoristas) throws MotoristaException {
		try {
			for (Motorista motorista : motoristas) {
				validateMotorista(motorista);
				String chavePrivada;
				Optional<Motorista> motoristaComChavePrivadaExistente;
				do {
					chavePrivada = BlockchainUtil.generatePrivateKey();
					motoristaComChavePrivadaExistente = this.findOneByChavePrivada(chavePrivada);
				} while (motoristaComChavePrivadaExistente.isPresent());
				motorista.setChavePrivada(chavePrivada);
			}
			return motoristaRepository.saveAll(motoristas);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível salvar. " + e.getMessage());
		}
	}

	@Override
	public Optional<Motorista> update(Long id, Motorista motoristaNew) throws MotoristaException {
		try {
			validateId(id);
			validateMotorista(motoristaNew);
			Optional<Motorista> motorista = motoristaRepository.findById(id);
			if (motorista.isPresent()) {
				motorista.get().setCpf(motoristaNew.getCpf());
				motorista.get().setNome(motoristaNew.getNome());
			}
			return motorista;
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível atualizar. " + e.getMessage());
		}
	}

	@Override
	public void delete(Long id) throws MotoristaException {
		try {
			validateId(id);
			motoristaRepository.deleteById(id);
		} catch (Exception e) {
			throw new MotoristaException("Não foi possível remover. " + e.getMessage());
		}
	}

}
