package com.tacografoonline.ontruck.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.service.exception.MotoristaException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public interface MotoristaService {
	
	/**
	 * Método usado para buscar todos os motoristas.
	 */
	List<Motorista> findAll() throws MotoristaException;
	
	/**
	 * Método usado para buscar todos os motoristas organizados pela paginação.
	 */
	Page<Motorista> findAll(Pageable pageable) throws MotoristaException;
	
	/**
	 * Método usado para buscar um motorista pelo nome e também por paginação.
	 */
	Page<Motorista> findAllByNome(String nome, Pageable pageable) throws MotoristaException;
	
	/**
	 * Método usado para buscar um motorista pela chave primária.
	 */
	Optional<Motorista> findOne(Long id) throws MotoristaException;
	
	/**
	 * Método usado para buscar um motorista pela chave primária.
	 */
	Optional<Motorista> findOneByChavePrivada(String codigoMotorista) throws MotoristaException;
	
	/**
	 * Método usado para buscar um motorista pelo cpf.
	 */
	Optional<Motorista> findOneByCpf(String cpf) throws MotoristaException;

	
	/**
	 * Método usado para salvar um motorista.
	 * @return Motorista com as novas informações cadastradas, como id.
	 */
	Motorista save(Motorista motorista) throws MotoristaException;

	/**
	 * Método usado para salvar uma lista de motoristas.
	 * @return Motoristas com as novas informações cadastradas, como id.
	 */
	List<Motorista> saveAll(List<Motorista> motoristas) throws MotoristaException;
	
	/**
	 * Método usado para atualizar um motorista.
	 */
	Optional<Motorista> update(Long id, Motorista motoristaNew) throws MotoristaException;
	
	/**
	 * Método usado para remover um motorista.
	 */
	void delete(Long id) throws MotoristaException;

}
