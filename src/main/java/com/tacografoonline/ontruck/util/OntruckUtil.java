package com.tacografoonline.ontruck.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class OntruckUtil {

	public static boolean isNull(Object o) {
		if (o == null) return true;
		return false;
	}

	public static boolean isNullOrEmpty(Object o) {
		if (!isNull(o)) {
			if (o instanceof String) return ((String) o).isEmpty();
		}
		return isNull(o);
	}

	/**
	 * Método usado para codificar uma cadeia de caracteres sensivel (senha / password).
	 */
	public static String codificarSenha(String senha) {
		return new BCryptPasswordEncoder().encode(senha);
	}
}
