package com.tacografoonline.ontruck.util;

import java.util.UUID;

import org.bitcoinj.core.ECKey;

import com.tacografoonline.ontruck.service.exception.BlockchainException;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-24
 *
 */
public class BlockchainUtil {

	/**
	 * Método para gerar chave privada.
	 */
	public static String generatePrivateKey() throws BlockchainException {
		return new ECKey().getPrivateKeyAsHex();
	}

	/**
	 * Método para gerar o address.
	 */
	public static String generateAddress() throws BlockchainException {
		return UUID.randomUUID().toString();
	}

}
