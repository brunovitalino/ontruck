package com.tacografoonline.ontruck.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Viagem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	private LocalDate data;

	@ManyToOne
	@NotNull
	private Motorista motorista1;

	@ManyToOne
	private Motorista motorista2;

	@ManyToOne
	@NotNull
	private Veiculo veiculo;

	@ManyToOne
	@NotNull
	private Cidade origem;

	@ManyToOne
	@NotNull
	private Cidade destino;

	private LocalDateTime horarioPartida;

	private LocalDateTime horarioChegada;

	private Integer kmTotalPartida;

	private Integer kmTotalChegada;

	@OneToMany(mappedBy = "viagem")
	private List<Movimentacao> movimentacoes;

	@CreationTimestamp
	private LocalDateTime created;

	@UpdateTimestamp
	private LocalDateTime updated;

	public Viagem(@NotNull Long id) {
		this.id = id;
	}

	public Viagem(@NotNull LocalDate data, @NotNull Motorista motorista1, Motorista motorista2,
			@NotNull Veiculo veiculo, @NotNull Cidade origem, @NotNull Cidade destino,
			@NotNull LocalDateTime horarioPartida, LocalDateTime horarioChegada, @NotNull Integer kmTotalPartida,
			Integer kmTotalChegada) {
		this.data = data;
		this.motorista1 = motorista1;
		this.motorista2 = motorista2;
		this.veiculo = veiculo;
		this.origem = origem;
		this.destino = destino;
		this.horarioPartida = horarioPartida;
		this.horarioChegada = horarioChegada;
		this.kmTotalPartida = kmTotalPartida;
		this.kmTotalChegada = kmTotalChegada;
	}

	public Viagem(@NotNull Long id, @NotNull LocalDate data, @NotNull Motorista motorista1, Motorista motorista2,
			@NotNull Veiculo veiculo, @NotNull Cidade origem, @NotNull Cidade destino,
			@NotNull LocalDateTime horarioPartida, LocalDateTime horarioChegada, @NotNull Integer kmTotalPartida,
			Integer kmTotalChegada) {
		this(data, motorista1, motorista2, veiculo, origem, destino, horarioPartida, horarioChegada, kmTotalPartida, kmTotalChegada);
		this.id = id;
	}

}
