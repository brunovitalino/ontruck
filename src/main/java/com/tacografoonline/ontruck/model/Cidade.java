package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Cidade {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String nome;
	
	@ManyToOne
	@NotNull
	private Estado estado;
	
	@OneToMany(mappedBy = "origem")
	private List<Viagem> viagensOrigem;
	
	@OneToMany(mappedBy = "destino")
	private List<Viagem> viagensDestino;
	
	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime updated;
	
	public Cidade(@NotEmpty String nome, @NotNull Estado estado) {
		this.nome = nome;
		this.estado = estado;
	}

	public Cidade(Long id, @NotEmpty String nome, @NotNull Estado estado) {
		this(nome, estado);
		this.id = id;
	}

}
