package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Config {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true)
	@NotNull
	private String descricao;
	
	@NotNull
	private Boolean trueOrFalse;
	
	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime updated;
	
	public Config(@NotNull Boolean trueOrFalse) {
		this.trueOrFalse = trueOrFalse;
	}
	
	public Config(@NotEmpty String descricao, @NotNull Boolean trueOrFalse) {
		this(trueOrFalse);
		this.descricao = descricao;
	}
	
	public Config(Long id, @NotNull Boolean trueOrFalse) {
		this(trueOrFalse);
		this.id = id;
	}
	
	public Config(Long id, @NotEmpty String descricao, @NotNull Boolean trueOrFalse) {
		this(descricao, trueOrFalse);
		this.id = id;
	}

	public Boolean isTrue() {
		return trueOrFalse;
	}

}
