package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.tacografoonline.ontruck.model.enums.MovendoParadoEnum;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Movimentacao {

	@Id	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@NotNull
	private Viagem viagem;

//	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
//	@JsonFormat(pattern="hh:mm:ss")
	@UpdateTimestamp
	private LocalDateTime horario;

	@Enumerated(EnumType.STRING)
	@NotNull
	private MovendoParadoEnum status;

	@NotNull
	private Integer velocidade;

	@CreationTimestamp
	private LocalDateTime created;

	@UpdateTimestamp
	private LocalDateTime updated;

	public Movimentacao(@NotNull Viagem viagem, @NotNull Integer velocidade) {
		this.viagem = viagem;
		this.velocidade = velocidade;
	}

	public Movimentacao(@NotNull Viagem viagem, @NotNull LocalDateTime horario, @NotNull Integer velocidade) {
		this(viagem, velocidade);
		this.horario = horario;
	}

	public Movimentacao(@NotNull Viagem viagem, @NotNull MovendoParadoEnum status, @NotNull Integer velocidade) {
		this(viagem, velocidade);
		this.status = status;
	}

	public Movimentacao(@NotNull Viagem viagem, @NotNull MovendoParadoEnum status, @NotNull LocalDateTime horario,
			@NotNull Integer velocidade) {
		this(viagem, status, velocidade);
		this.horario = horario;
	}

	public Movimentacao(@NotNull Long id, @NotNull Viagem viagem, @NotNull MovendoParadoEnum status,
			@NotNull LocalDateTime horario, @NotNull Integer velocidade) {
		this(viagem, status, horario, velocidade);
		this.id = id;
	}

}
