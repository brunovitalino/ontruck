package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Veiculo {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true)
	@NotEmpty
	private String placa;
	
	@ManyToOne
	@NotNull
	private Cor cor;
	
	@ManyToOne
	@NotNull
	private Modelo modelo;
	
	@NotNull
	private Integer ano;
	
	@NotNull
	private Integer kmTotal;
	
	@OneToMany(mappedBy = "veiculo")
	private List<Viagem> viagens;
	
	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime updated;

	public Veiculo(@NotEmpty String placa, @NotNull Cor cor, @NotNull Modelo modelo, @NotNull Integer ano, @NotNull Integer kmTotal) {
		this.placa = placa;
		this.cor = cor;
		this.modelo = modelo;
		this.ano = ano;
		this.kmTotal = kmTotal;
	}

	public Veiculo(@NotNull Long id, @NotEmpty String placa, @NotNull Cor cor, @NotNull Modelo modelo, @NotNull Integer ano, @NotNull Integer kmTotal) {
		this(placa, cor, modelo, ano, kmTotal);
		this.id = id;
	}

}
