package com.tacografoonline.ontruck.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MovendoParadoEnum {
	MOVENDO("Movendo"), PARADO("Parado");

	private String value;
}
