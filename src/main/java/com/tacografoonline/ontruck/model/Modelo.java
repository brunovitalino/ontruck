package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Modelo {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true)
	@NotEmpty
	private String nome;
	
	@NotNull
	@ManyToOne
	private Marca marca;
	
	@OneToMany(mappedBy = "modelo")
	private List<Veiculo> veiculos;
	
	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime updated;

	public Modelo(@NotEmpty String nome, @NotNull Marca marca) {
		this.nome = nome;
		this.marca = marca;
	}

	public Modelo(@NotNull Long id, @NotEmpty String nome, @NotNull Marca marca) {
		this(nome, marca);
		this.id = id;
	}

}
