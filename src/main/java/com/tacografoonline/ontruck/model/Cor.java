package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Cor {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String nome;
	
	@OneToMany(mappedBy = "cor")
	private List<Veiculo> veiculos;

	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime updated;

	public Cor(@NotEmpty String nome) {
		this.nome = nome;
	}

	public Cor(Long id, @NotEmpty String nome) {
		this(nome);
		this.id = id;
	}

}
