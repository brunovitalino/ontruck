package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Motorista implements MotoristaIdentificacao {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private String chavePrivada;

	@Column(unique = true)
	private String cpf;

	private String nome;
	
	@OneToMany(mappedBy = "motorista1")
	private List<Viagem> viagensMotorista1;
	
	@OneToMany(mappedBy = "motorista2")
	private List<Viagem> viagensMotorista2;
	
	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime updated;

	public Motorista(String cpf, String nome) {
		this.cpf = cpf;
		this.nome = nome;
	}

	public Motorista(Long id, String cpf, String nome) {
		this(cpf, nome);
		this.id = id;
	}

	@Override
	public String getChavePrivada() {
		return chavePrivada;
	}

}
