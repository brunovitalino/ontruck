package com.tacografoonline.ontruck.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Usuario implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String nome;

	@NotEmpty
	@Column(unique = true)
	private String email;

	@NotEmpty
	private String senha;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) // torna carregamento nao lazy
	private List<Role> roles = new ArrayList<>();

	@CreationTimestamp
	private LocalDateTime created;

	@UpdateTimestamp
	private LocalDateTime updated;
	
	public Usuario(@NotEmpty String email, String senha) {
		this.email = email;
		this.senha = senha;
	}

	public Usuario(@NotEmpty String nome, @NotEmpty String email, String senha) {
		this(email, senha);
		this.nome = nome;
	}

	public Usuario(@NotNull Long id, @NotEmpty String nome, @NotEmpty String email, @NotEmpty String senha) {
		this(nome, email, senha);
		this.id = id;
	}

	public Usuario(@NotEmpty String nome, @NotEmpty String email, String senha, @NotEmpty List<Role> roles) {
		this(nome, email, senha);
		this.roles = roles;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.getRoles();
	}

	@Override
	public String getPassword() {
		return this.getSenha();
	}

	@Override
	public String getUsername() {
		return this.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
