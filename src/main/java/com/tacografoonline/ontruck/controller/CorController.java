package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.CorDto;
import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.service.CorService;
import com.tacografoonline.ontruck.service.exception.CorException;

@RestController
@RequestMapping("/cores")
public class CorController {

	@Autowired
	CorService corService;

	@GetMapping
	@Cacheable(value = "findAllCores")
	// Ex. http://localhost:8080/cores?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<CorDto>> findAll(@RequestParam(required = false) Optional<String> nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Cor> cores;
			if (nome.isPresent()) {
				cores = corService.findAllByNome(nome.get(), pageable);
				return ResponseEntity.ok(CorDto.convert(cores));
			}
			cores = corService.findAll(pageable);
			return ResponseEntity.ok(CorDto.convert(cores));
		} catch (CorException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<CorDto> findOne(@PathVariable Long id) {
		try {
			Optional<Cor> cor = corService.findOneById(id);
			if (cor.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new CorDto(cor.get()));
		} catch (CorException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllCores", allEntries = true)
	public ResponseEntity<CorDto> save(@RequestBody @Valid CorDto corDto, UriComponentsBuilder uriBuilder) {
		try {
			Cor cor = corDto.toEntity();
			corService.save(cor);
			URI uri = uriBuilder.path("/cores/{id}").buildAndExpand(cor.getId()).toUri();
			return ResponseEntity.created(uri).body(new CorDto(cor));
		} catch (CorException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllCores", allEntries = true)
	public ResponseEntity<CorDto> update(@PathVariable Long id, @RequestBody @Valid CorDto corDto) {
		try {
			Cor cor = corService.update(id, corDto.toEntity());
			return ResponseEntity.ok(new CorDto(cor));
		} catch (CorException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllCores", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			corService.delete(id);
			return ResponseEntity.ok().build();
		} catch (CorException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
