package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.RoleDto;
import com.tacografoonline.ontruck.controller.form.RoleForm;
import com.tacografoonline.ontruck.model.Role;
import com.tacografoonline.ontruck.service.RoleService;
import com.tacografoonline.ontruck.service.exception.RoleException;

@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	RoleService roleService;

	@GetMapping
	@Cacheable(value = "findAllRoles")
	public ResponseEntity<List<RoleDto>> findAll(@RequestParam Optional<String> nome) {
		try {
			List<Role> roles = roleService.findAll();
			if (nome.isPresent()) {
				roles = roles.stream().filter(r -> nome.get().equals(r.getNome())).collect(Collectors.toList());
				return ResponseEntity.ok(RoleDto.convert(roles));
			}
			return ResponseEntity.ok(RoleDto.convert(roles));
		} catch (RoleException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<RoleDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Role> role = roleService.findOneById(id);
			if (role.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new RoleDto(role.get()));
		} catch (RoleException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllRoles", allEntries = true)
	public ResponseEntity<RoleDto> save(@RequestBody @Valid RoleForm roleForm, UriComponentsBuilder uriBuilder) {
		try {
			Role role = roleForm.toEntity();
			roleService.save(role);
			URI uri = uriBuilder.path("/roles/{id}").buildAndExpand(role.getId()).toUri();
			return ResponseEntity.created(uri).body(new RoleDto(role));
		} catch (RoleException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllRoles", allEntries = true)
	public ResponseEntity<RoleDto> update(@PathVariable Long id, @RequestBody @Valid RoleForm roleForm) {
		try {
			Role role = roleService.update(id, roleForm.toEntity());
			return ResponseEntity.ok(new RoleDto(role));
		} catch (RoleException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllRoles", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			roleService.delete(id);
			return ResponseEntity.ok().build();
		} catch (RoleException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
