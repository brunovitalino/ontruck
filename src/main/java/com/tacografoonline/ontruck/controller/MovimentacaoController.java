package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.blockchain.api.controller.form.BlockchainForm;
import com.tacografoonline.ontruck.controller.dto.MovimentacaoDto;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.service.MotoristaService;
import com.tacografoonline.ontruck.service.MovimentacaoService;
import com.tacografoonline.ontruck.service.exception.MotoristaException;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;

@RestController
@RequestMapping("/movimentacoes")
public class MovimentacaoController {
	
	@Autowired
	MotoristaService motoristaService;
	
	@Autowired
	MovimentacaoService movimentacaoService;
	
	@PostMapping()
	public ResponseEntity<?> save(@RequestBody BlockchainForm movimentacaoForm, UriComponentsBuilder uriBuilder) {
		Optional<Motorista> motorista;
		try {
			motorista = motoristaService.findOneByChavePrivada(movimentacaoForm.getCodigo());
			if (motorista.isEmpty()) {
				return ResponseEntity.notFound().build();
			}
		} catch (MotoristaException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		try {
			Movimentacao movimentacao = movimentacaoService.save(movimentacaoForm.toEntity(), motorista.get());
			URI uri = uriBuilder.path("/movimentacoes/viagem/{id}/dataHora/dataHoraQualquer").buildAndExpand(movimentacao.getViagem().getId()).toUri();
			return ResponseEntity.created(uri).body(new MovimentacaoDto(movimentacao));
		} catch (MovimentacaoException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@GetMapping()
	public ResponseEntity<?> findAll(@PageableDefault(sort = "horario", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Movimentacao> movimentacoes = movimentacaoService.findAll(pageable);
			return ResponseEntity.ok(MovimentacaoDto.convert(movimentacoes));
		} catch (MovimentacaoException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@GetMapping("/viagem/{viagemId}")
	public ResponseEntity<?> findAllByViagem(@PathVariable("viagemId") Long viagemId,
			@PageableDefault(sort = "horario", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Movimentacao> movimentacoes = movimentacaoService.findAllByViagemId(viagemId, pageable);
			return ResponseEntity.ok(MovimentacaoDto.convert(movimentacoes));
		} catch (MovimentacaoException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

}