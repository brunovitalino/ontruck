package com.tacografoonline.ontruck.controller;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tacografoonline.ontruck.model.Config;
import com.tacografoonline.ontruck.repository.EstadoRepository;
import com.tacografoonline.ontruck.service.ConfigService;
import com.tacografoonline.ontruck.service.exception.ConfigException;

@RestController
public class ConfigController {

	@Autowired
	ConfigService configuracaoService;

	@Autowired
	EstadoRepository estadoRepository;

	@GetMapping("/status")
	public String home() {
		return "API online!";
	}

	@GetMapping("/balao-magico")
	@Transactional
	public String configDB() {
		try {
			Long idConfigDB = Long.valueOf(1);
			Optional<Config> configuracao = configuracaoService.findById(idConfigDB);
			if (configuracao.isPresent() && configuracao.get().isTrue()) {
				return "Banco de dados já preenchido!";
			}
			configuracaoService.fillDB();
			return "Banco de dados preenchido!";
		} catch (ConfigException e) {
			e.printStackTrace();
			return "Não foi possível preencher o banco de dados.";
		}
	}
}
