package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.CidadeDto;
import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.service.CidadeService;
import com.tacografoonline.ontruck.service.exception.CidadeException;

@RestController
@RequestMapping("/cidades")
public class CidadeController {

	@Autowired
	CidadeService cidadeService;

	@GetMapping
	@Cacheable(value = "findAllCidades")
	public ResponseEntity<Page<CidadeDto>> findAll(@RequestParam Optional<String> nome,
			@RequestParam Optional<String> estadoNome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Cidade> cidades = cidadeService.findAll(nome, estadoNome, pageable);
			return ResponseEntity.ok(CidadeDto.toDtoPage(cidades));
		} catch (CidadeException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<CidadeDto> findOne(@PathVariable Long id) {
		try {
			Optional<Cidade> cidade = cidadeService.findOne(id);
			if (cidade.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new CidadeDto(cidade.get()));
		} catch (CidadeException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllCidades", allEntries = true)
	public ResponseEntity<CidadeDto> save(@RequestBody @Valid CidadeDto cidadeDto, UriComponentsBuilder uriBuilder) {
		try {
			Cidade cidade = cidadeDto.toEntity();
			cidadeService.saveOne(cidade);
			URI uri = uriBuilder.path("/cidades/{id}").buildAndExpand(cidade.getId()).toUri();
			return ResponseEntity.created(uri).body(new CidadeDto(cidade));
		} catch (CidadeException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllCidades", allEntries = true)
	public ResponseEntity<CidadeDto> update(@PathVariable Long id, @RequestBody @Valid CidadeDto cidadeDto) {
		try {
			Cidade cidade = cidadeService.update(id, cidadeDto.toEntity());
			return ResponseEntity.ok(new CidadeDto(cidade));
		} catch (CidadeException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllCidades", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			cidadeService.delete(id);
			return ResponseEntity.ok().build();
		} catch (CidadeException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
