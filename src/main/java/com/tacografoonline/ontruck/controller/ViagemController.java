package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.ViagemDto;
import com.tacografoonline.ontruck.controller.dto.ViagemFormDto;
import com.tacografoonline.ontruck.controller.dto.ViagemFormHorarioChegadaDto;
import com.tacografoonline.ontruck.controller.dto.ViagemFormHorarioPartidaDto;
import com.tacografoonline.ontruck.controller.dto.ViagemFormKmTotalChegadaDto;
import com.tacografoonline.ontruck.controller.dto.ViagemFormKmTotalPartidaDto;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.service.ViagemService;
import com.tacografoonline.ontruck.service.exception.ViagemException;

@RestController
@RequestMapping("/viagens")
public class ViagemController {

	@Autowired
	ViagemService viagemService;
	
	@GetMapping
	@Cacheable(value = "findAllViagens")
	// Ex. http://localhost:8080/viagens?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<ViagemDto>> findAll(@RequestParam(required = false) Optional<String> viagemPlaca,
			@PageableDefault(sort = "id", direction = Direction.DESC) Pageable pageable) {
		try {
			Page<Viagem> viagens;
			if (viagemPlaca.isPresent()) {
				viagens = viagemService.findAllByVeiculoPlaca(viagemPlaca.get(), pageable);
				return ResponseEntity.ok(ViagemDto.convert(viagens));
			}
			viagens = viagemService.findAll(pageable);
			return ResponseEntity.ok(ViagemDto.convert(viagens));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<ViagemDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Viagem> viagem = viagemService.findOneById(id);
			if (viagem.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new ViagemDto(viagem.get()));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<ViagemDto> save(@RequestBody @Valid ViagemFormDto viagemDto, UriComponentsBuilder uriBuilder) {
		try {
			Viagem viagem = viagemDto.toEntity();
			viagemService.save(viagem);
			URI uri = uriBuilder.path("/viagens/{id}").buildAndExpand(viagem.getId()).toUri();
			return ResponseEntity.created(uri).body(new ViagemDto(viagem));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<ViagemDto> update(@PathVariable Long id, @RequestBody @Valid ViagemFormDto viagemDto) {
		try {
			Viagem viagem = viagemService.update(id, viagemDto.toEntity());
			return ResponseEntity.ok(new ViagemDto(viagem));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PatchMapping("/{id}/horarioPartida")
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<ViagemDto> updateHorarioPartida(@PathVariable Long id, @RequestBody @Valid ViagemFormHorarioPartidaDto viagemFormHorarioPartidaDto) {
		try {
			Viagem viagem = viagemService.updateHorarioPartida(id, viagemFormHorarioPartidaDto.getHorarioPartida());
			return ResponseEntity.ok(new ViagemDto(viagem));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PatchMapping("/{id}/horarioChegada")
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<ViagemDto> updateHorarioChegada(@PathVariable Long id, @RequestBody @Valid ViagemFormHorarioChegadaDto viagemHorarioChegadaFormDto) {
		try {
			Viagem viagem = viagemService.updateHorarioChegada(id, viagemHorarioChegadaFormDto.getHorarioChegada());
			return ResponseEntity.ok(new ViagemDto(viagem));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PatchMapping("/{id}/kmTotalPartida")
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<ViagemDto> updateKmTotalPartida(@PathVariable Long id, @RequestBody @Valid ViagemFormKmTotalPartidaDto viagemFormKmTotalPartidaDto) {
		try {
			Viagem viagem = viagemService.updateKmTotalPartida(id, viagemFormKmTotalPartidaDto.getKmTotalPartida());
			return ResponseEntity.ok(new ViagemDto(viagem));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PatchMapping("/{id}/kmTotalChegada")
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<ViagemDto> updateKmTotalChegada(@PathVariable Long id, @RequestBody @Valid ViagemFormKmTotalChegadaDto viagemKmTotalChegadaFormDto) {
		try {
			Viagem viagem = viagemService.updateKmTotalChegada(id, viagemKmTotalChegadaFormDto.getKmTotalChegada());
			return ResponseEntity.ok(new ViagemDto(viagem));
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllViagens", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			viagemService.delete(id);
			return ResponseEntity.ok().build();
		} catch (ViagemException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
