package com.tacografoonline.ontruck.controller.form;

import javax.validation.constraints.NotEmpty;

import com.tacografoonline.ontruck.model.Role;

import lombok.Getter;

@Getter
public class RoleForm {

	@NotEmpty
	private String nome;

	public Role toEntity() {
		return new Role("ROLE_" + this.getNome().toUpperCase());
	}
}
