package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.UsuarioDto;
import com.tacografoonline.ontruck.controller.dto.UsuarioFormDto;
import com.tacografoonline.ontruck.controller.dto.UsuarioFormEmailDto;
import com.tacografoonline.ontruck.controller.dto.UsuarioFormSenhaDto;
import com.tacografoonline.ontruck.model.Usuario;
import com.tacografoonline.ontruck.service.UsuarioService;
import com.tacografoonline.ontruck.service.exception.UsuarioException;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;

	@GetMapping
	@Cacheable(value = "findAllUsuarios")
	// Ex. http://localhost:8080/usuarios?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<UsuarioDto>> findAll(@RequestParam Optional<String> nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Usuario> usuarios;
			if (nome.isPresent()) {
				usuarios = usuarioService.findAllByNome(nome.get(), pageable);
				return ResponseEntity.ok(UsuarioDto.convert(usuarios));
			}
			usuarios = usuarioService.findAll(pageable);
			return ResponseEntity.ok(UsuarioDto.convert(usuarios));
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<UsuarioDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Usuario> usuario = usuarioService.findOneById(id);
			if (usuario.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new UsuarioDto(usuario.get()));
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllUsuarios", allEntries = true)
	public ResponseEntity<UsuarioDto> save(@RequestBody @Valid UsuarioFormDto usuarioDto, UriComponentsBuilder uriBuilder) {
		try {
			Usuario usuario = usuarioDto.toEntity();
			usuarioService.save(usuario);
			URI uri = uriBuilder.path("/usuarios/{id}").buildAndExpand(usuario.getId()).toUri();
			return ResponseEntity.created(uri).body(new UsuarioDto(usuario));
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllUsuarios", allEntries = true)
	public ResponseEntity<UsuarioDto> update(@PathVariable Long id, @RequestBody @Valid UsuarioFormDto usuarioDto) {
		try {
			Usuario usuario = usuarioService.update(id, usuarioDto.toEntity());
			return ResponseEntity.ok(new UsuarioDto(usuario));
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PatchMapping("/{id}/email")
	@Transactional
	@CacheEvict(value = "findAllUsuarios", allEntries = true)
	public ResponseEntity<UsuarioDto> updateEmail(@PathVariable Long id, @RequestBody @Valid UsuarioFormEmailDto usuarioEmailFormDto) {
		try {
			Usuario usuario = usuarioService.updateEmail(id, usuarioEmailFormDto.getEmail());
			return ResponseEntity.ok(new UsuarioDto(usuario));
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PatchMapping("/{id}/senha")
	@Transactional
	@CacheEvict(value = "findAllUsuarios", allEntries = true)
	public ResponseEntity<UsuarioDto> updateSenha(@PathVariable Long id, @RequestBody @Valid UsuarioFormSenhaDto usuarioSenhaFormDto) {
		try {
			Usuario usuario = usuarioService.updateSenha(id, usuarioSenhaFormDto.getSenha());
			return ResponseEntity.ok(new UsuarioDto(usuario));
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllUsuarios", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			usuarioService.delete(id);
			return ResponseEntity.ok().build();
		} catch (UsuarioException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
