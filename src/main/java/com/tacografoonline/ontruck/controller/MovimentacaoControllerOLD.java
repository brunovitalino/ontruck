package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.time.LocalDate;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.MovimentacaoDto;
import com.tacografoonline.ontruck.controller.dto.MovimentacaoFormDto;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.service.MovimentacaoServiceOLD;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;

@Deprecated
@RestController
@RequestMapping("/movimentacoes-old")
public class MovimentacaoControllerOLD {

	@Autowired
	MovimentacaoServiceOLD movimentacaoService;

	@GetMapping
	@Cacheable(value = "findAllMovimentacoes")
	// Ex. http://localhost:8080/movimentacoes?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<MovimentacaoDto>> findAll(@RequestParam Optional<LocalDate> dataInicio,
			@RequestParam Optional<LocalDate> dataFim,
			@PageableDefault(sort = "id", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Movimentacao> movimentacoes;
			if (dataInicio.isPresent()) {
				if (dataFim.isPresent()) {
					movimentacoes = movimentacaoService.findAllByData(dataInicio.get(), dataFim.get(), pageable);
					return ResponseEntity.ok(MovimentacaoDto.convertToPage(movimentacoes));
				}
				movimentacoes = movimentacaoService.findAllByData(dataInicio.get(), LocalDate.now(), pageable);
				return ResponseEntity.ok(MovimentacaoDto.convertToPage(movimentacoes));
			}
			movimentacoes = movimentacaoService.findAll(pageable);
			return ResponseEntity.ok(MovimentacaoDto.convertToPage(movimentacoes));
		} catch (MovimentacaoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<MovimentacaoDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Movimentacao> movimentacao = movimentacaoService.findOneById(id);
			if (movimentacao.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new MovimentacaoDto(movimentacao.get()));
		} catch (MovimentacaoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllMovimentacoes", allEntries = true)
	public ResponseEntity<MovimentacaoDto> save(@RequestBody @Valid MovimentacaoFormDto movimentacaoDto, UriComponentsBuilder uriBuilder) {
		try {
			Movimentacao movimentacao = movimentacaoDto.toEntity();
			movimentacaoService.save(movimentacao);
			URI uri = uriBuilder.path("/movimentacoes/{id}").buildAndExpand(movimentacao.getId()).toUri();
			return ResponseEntity.created(uri).body(new MovimentacaoDto(movimentacao));
		} catch (MovimentacaoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllMovimentacoes", allEntries = true)
	public ResponseEntity<MovimentacaoDto> update(@PathVariable Long id, @RequestBody @Valid MovimentacaoFormDto movimentacaoDto) {
		try {
			Movimentacao movimentacao = movimentacaoService.update(id, movimentacaoDto.toEntity());
			return ResponseEntity.ok(new MovimentacaoDto(movimentacao));
		} catch (MovimentacaoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllMovimentacoes", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			movimentacaoService.delete(id);
			return ResponseEntity.ok().build();
		} catch (MovimentacaoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
