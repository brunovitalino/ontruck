package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.MotoristaDto;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.service.MotoristaService;
import com.tacografoonline.ontruck.service.exception.MotoristaException;

@RestController
@RequestMapping("/motoristas")
public class MotoristaController {

	@Autowired
	MotoristaService motoristaService;

	@GetMapping
	@Cacheable(value = "findAllMotoristas")
	public ResponseEntity<Page<MotoristaDto>> findAll(@RequestParam(required = false) String nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Motorista> motoristas = null;
			if (nome != null) {
				motoristas = motoristaService.findAllByNome(nome, pageable);
				return ResponseEntity.ok(MotoristaDto.toDtoPage(motoristas));
			}
			motoristas = motoristaService.findAll(pageable);
			return ResponseEntity.ok(MotoristaDto.toDtoPage(motoristas));
		} catch (MotoristaException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<MotoristaDto> findOne(@PathVariable Long id) {
		try {
			Optional<Motorista> motorista = motoristaService.findOne(id);
			return motorista.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok(new MotoristaDto(motorista.get()));
		} catch (MotoristaException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllMotoristas", allEntries = true)
	public ResponseEntity<MotoristaDto> save(@RequestBody @Valid MotoristaDto motoristaDto, UriComponentsBuilder uriBuilder) {
		try {
			Motorista motorista = motoristaDto.toEntity();
			motoristaService.save(motorista);
			URI uri = uriBuilder.path("/motoristas/{id}").buildAndExpand(motorista.getId()).toUri();
			return ResponseEntity.created(uri).body(new MotoristaDto(motorista));
		} catch (MotoristaException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllMotoristas", allEntries = true)
	public ResponseEntity<MotoristaDto> update(@PathVariable Long id, @RequestBody @Valid MotoristaDto motoristaDto) {
		try {
			Optional<Motorista> motorista = motoristaService.update(id, motoristaDto.toEntity());
			if (motorista.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new MotoristaDto(motorista.get()));
		} catch (MotoristaException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllMotoristas", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			motoristaService.delete(id);
			return ResponseEntity.ok().build();
		} catch (MotoristaException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}
	
}
