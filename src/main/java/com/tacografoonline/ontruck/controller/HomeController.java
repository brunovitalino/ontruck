package com.tacografoonline.ontruck.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class HomeController {

	@GetMapping("/")
	public RedirectView barra(RedirectAttributes attributes) {
        attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
        return new RedirectView("home");
	}

	@GetMapping("/home")
	public String home() {
		return "Página inicial!";
	}
}
