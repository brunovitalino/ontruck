package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.MarcaDto;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.service.MarcaService;
import com.tacografoonline.ontruck.service.exception.MarcaException;

@RestController
@RequestMapping("/marcas")
public class MarcaController {

	@Autowired
	MarcaService marcaService;

	@GetMapping
	@Cacheable(value = "findAllMarcas")
	// Ex. http://localhost:8080/marcas?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<MarcaDto>> findAll(@RequestParam(required = false) Optional<String> nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Marca> marcas;
			if (nome.isPresent()) {
				marcas = marcaService.findAllByNome(nome.get(), pageable);
				return ResponseEntity.ok(MarcaDto.convert(marcas));
			}
			marcas = marcaService.findAll(pageable);
			return ResponseEntity.ok(MarcaDto.convert(marcas));
		} catch (MarcaException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<MarcaDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Marca> marca = marcaService.findOneById(id);
			if (marca.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new MarcaDto(marca.get()));
		} catch (MarcaException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllMarcas", allEntries = true)
	public ResponseEntity<MarcaDto> save(@RequestBody @Valid MarcaDto marcaDto, UriComponentsBuilder uriBuilder) {
		try {
			Marca marca = marcaDto.toEntity();
			marcaService.save(marca);
			URI uri = uriBuilder.path("/marcas/{id}").buildAndExpand(marca.getId()).toUri();
			return ResponseEntity.created(uri).body(new MarcaDto(marca));
		} catch (MarcaException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllMarcas", allEntries = true)
	public ResponseEntity<MarcaDto> update(@PathVariable Long id, @RequestBody @Valid MarcaDto marcaDto) {
		try {
			Marca marca = marcaService.update(id, marcaDto.toEntity());
			return ResponseEntity.ok(new MarcaDto(marca));
		} catch (MarcaException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllMarcas", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			marcaService.delete(id);
			return ResponseEntity.ok().build();
		} catch (MarcaException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
