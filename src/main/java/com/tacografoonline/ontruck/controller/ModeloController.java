package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.ModeloDto;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.service.ModeloService;
import com.tacografoonline.ontruck.service.exception.ModeloException;

@RestController
@RequestMapping("/modelos")
public class ModeloController {

	@Autowired
	ModeloService modeloService;

	@GetMapping
	@Cacheable(value = "findAllModelos")
	// Ex. http://localhost:8080/modelos?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<ModeloDto>> findAll(@RequestParam(required = false) Optional<String> nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Modelo> modelos;
			if (nome.isPresent()) {
				modelos = modeloService.findAllByNome(nome.get(), pageable);
				return ResponseEntity.ok(ModeloDto.convert(modelos));
			}
			modelos = modeloService.findAll(pageable);
			return ResponseEntity.ok(ModeloDto.convert(modelos));
		} catch (ModeloException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<ModeloDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Modelo> modelo = modeloService.findOneById(id);
			if (modelo.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new ModeloDto(modelo.get()));
		} catch (ModeloException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllModelos", allEntries = true)
	public ResponseEntity<ModeloDto> save(@RequestBody @Valid ModeloDto modeloDto, UriComponentsBuilder uriBuilder) {
		try {
			Modelo modelo = modeloDto.toEntity();
			modeloService.save(modelo);
			URI uri = uriBuilder.path("/modelos/{id}").buildAndExpand(modelo.getId()).toUri();
			return ResponseEntity.created(uri).body(new ModeloDto(modelo));
		} catch (ModeloException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllModelos", allEntries = true)
	public ResponseEntity<ModeloDto> update(@PathVariable Long id, @RequestBody @Valid ModeloDto modeloDto) {
		try {
			Modelo modelo = modeloService.update(id, modeloDto.toEntity());
			return ResponseEntity.ok(new ModeloDto(modelo));
		} catch (ModeloException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllModelos", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			modeloService.delete(id);
			return ResponseEntity.ok().build();
		} catch (ModeloException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
