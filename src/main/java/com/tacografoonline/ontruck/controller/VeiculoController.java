package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.VeiculoDto;
import com.tacografoonline.ontruck.controller.dto.VeiculoFormDto;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.service.VeiculoService;
import com.tacografoonline.ontruck.service.exception.VeiculoException;

@RestController
@RequestMapping("/veiculos")
public class VeiculoController {

	@Autowired
	VeiculoService veiculoService;

	@GetMapping
	@Cacheable(value = "findAllVeiculos")
	// Ex. http://localhost:8080/veiculos?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<VeiculoDto>> findAll(@RequestParam(required = false) Optional<String> placa,
			@PageableDefault(sort = "modelo.nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Veiculo> veiculos;
			if (placa.isPresent()) {
				veiculos = veiculoService.findByPlaca(placa.get(), pageable);
				return ResponseEntity.ok(VeiculoDto.convert(veiculos));
			}
			veiculos = veiculoService.findAll(pageable);
			return ResponseEntity.ok(VeiculoDto.convert(veiculos));
		} catch (VeiculoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<VeiculoDto> findOneById(@PathVariable Long id) {
		try {
			Optional<Veiculo> veiculo = veiculoService.findOneById(id);
			if (veiculo.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new VeiculoDto(veiculo.get()));
		} catch (VeiculoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllVeiculos", allEntries = true)
	public ResponseEntity<VeiculoDto> save(@RequestBody @Valid VeiculoFormDto veiculoDto, UriComponentsBuilder uriBuilder) {
		try {
			Veiculo veiculo = veiculoDto.toEntity();
			veiculoService.save(veiculo);
			URI uri = uriBuilder.path("/veiculos/{id}").buildAndExpand(veiculo.getId()).toUri();
			return ResponseEntity.created(uri).body(new VeiculoDto(veiculo));
		} catch (VeiculoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllVeiculos", allEntries = true)
	public ResponseEntity<VeiculoDto> update(@PathVariable Long id, @RequestBody @Valid VeiculoFormDto veiculoDto) {
		try {
			Veiculo veiculo = veiculoService.update(id, veiculoDto.toEntity());
			return ResponseEntity.ok(new VeiculoDto(veiculo));
		} catch (VeiculoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllVeiculos", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			veiculoService.delete(id);
			return ResponseEntity.ok().build();
		} catch (VeiculoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
