package com.tacografoonline.ontruck.controller.dto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Modelo;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ModeloDto {

	private Long id;
	
	@NotEmpty
	private String nome;
	
	@NotNull
	private MarcaDto marca;

	public ModeloDto(Modelo modelo) {
		if (Optional.ofNullable(modelo).isPresent()) {
			this.id = modelo.getId();
			this.nome = modelo.getNome();
			this.marca = new MarcaDto(modelo.getMarca());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public MarcaDto getMarca() {
		return marca;
	}

	public void setMarca(MarcaDto marca) {
		this.marca = marca;
	}

	public Modelo toEntity() {
		return new Modelo(this.getId(), this.getNome(), this.getMarca().toEntity());
	}

	public static Page<ModeloDto> convert(Page<Modelo> modelos) {
		return modelos.map(ModeloDto::new);
	}

	public static List<ModeloDto> convert(List<Modelo> modelos) {
		return modelos.stream().map(ModeloDto::new).collect(Collectors.toList());
	}

}
