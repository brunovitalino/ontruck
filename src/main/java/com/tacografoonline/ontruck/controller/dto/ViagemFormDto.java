package com.tacografoonline.ontruck.controller.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.model.Viagem;

public class ViagemFormDto {

	@NotNull
	private LocalDate data;

	@NotNull
	private Motorista motorista1;

	private Motorista motorista2;

	@NotNull
	private Veiculo veiculo;

	@NotNull
	private Cidade origem;

	@NotNull
	private Cidade destino;

	private LocalDateTime horarioPartida;

	private LocalDateTime horarioChegada;

	private Integer kmTotalPartida;

	private Integer kmTotalChegada;

	public LocalDate getData() {
		return data;
	}

	public Motorista getMotorista1() {
		return motorista1;
	}

	public Motorista getMotorista2() {
		return motorista2;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public Cidade getOrigem() {
		return origem;
	}

	public Cidade getDestino() {
		return destino;
	}

	public LocalDateTime getHorarioPartida() {
		return horarioPartida;
	}

	public LocalDateTime getHorarioChegada() {
		return horarioChegada;
	}

	public Integer getKmTotalPartida() {
		return kmTotalPartida;
	}

	public Integer getKmTotalChegada() {
		return kmTotalChegada;
	}

	public Viagem toEntity() {
		return new Viagem(this.getData(), this.getMotorista1(), this.getMotorista2(), this.getVeiculo(),
				this.getOrigem(), this.getDestino(), this.getHorarioPartida(), this.getHorarioChegada(),
				this.getKmTotalPartida(), this.getKmTotalChegada());
	}

}
