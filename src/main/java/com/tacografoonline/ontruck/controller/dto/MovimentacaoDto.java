package com.tacografoonline.ontruck.controller.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.blockchain.api.controller.dto.BlockchainDTO;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.model.enums.MovendoParadoEnum;

import lombok.Getter;

@Getter
public class MovimentacaoDto {

	private Long id;
	private ViagemDto viagem;
	private MovendoParadoEnum status;
	private LocalDateTime horario;
	private Integer velocidade;
	
	public MovimentacaoDto(Movimentacao movimentacao) {
		if (movimentacao != null) {
			this.id = movimentacao.getId();
			this.viagem = new ViagemDto(movimentacao.getViagem());
			this.status = movimentacao.getStatus();
			this.horario = movimentacao.getHorario();
			this.velocidade = movimentacao.getVelocidade();
		}
	}
	
	public MovimentacaoDto(BlockchainDTO movimentacaoBcDTO) {
		if (movimentacaoBcDTO != null) {
			if (movimentacaoBcDTO.getViagemId() != null) {
				Long viagemId = Long.parseLong(movimentacaoBcDTO.getViagemId(), 10);
				viagem = new ViagemDto(new Viagem(viagemId));
			}
	        horario = movimentacaoBcDTO.getHorario() != null ? LocalDateTime.parse(movimentacaoBcDTO.getHorario()) : null;
	        velocidade = movimentacaoBcDTO.getVelocidade() != null ? Integer.parseInt(movimentacaoBcDTO.getVelocidade()) : null;
		}
	}

	public Movimentacao toEntity() {
		return new Movimentacao(viagem == null ? null : viagem.toEntity(), horario, velocidade);
	}

	public static List<Movimentacao> toEntityList(List<MovimentacaoDto> movimentacoesDTO) {
		return movimentacoesDTO == null ? null :
			movimentacoesDTO.stream().map(m -> m.toEntity()).collect(Collectors.toList());
	}

	public static List<Movimentacao> toEntityListFromBlockchainDtoList(List<BlockchainDTO> blockchainListDTO) {
		return blockchainListDTO == null ? null :
			blockchainListDTO.stream().map(b -> new MovimentacaoDto(b).toEntity()).collect(Collectors.toList());
	}

	public static List<MovimentacaoDto> convert(List<Movimentacao> movimentacoes) {
		return movimentacoes == null ? null :
			movimentacoes.stream().map(MovimentacaoDto::new).collect(Collectors.toList());
	}

	public static Page<MovimentacaoDto> convert(Page<Movimentacao> movimentacoes) {
		return movimentacoes.map(MovimentacaoDto::new);
	}

	public static List<MovimentacaoDto> convertFromBlockchainDtoList(List<BlockchainDTO> blockchainListDTO) {
		return blockchainListDTO == null ? null :
			blockchainListDTO.stream().map(MovimentacaoDto::new).collect(Collectors.toList());
	}

	public static Page<MovimentacaoDto> convertToPage(Page<Movimentacao> movimentacoes) {
		return movimentacoes == null ? null :
			movimentacoes.map(MovimentacaoDto::new);
	}

}
