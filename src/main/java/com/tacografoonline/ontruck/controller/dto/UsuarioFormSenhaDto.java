package com.tacografoonline.ontruck.controller.dto;

import javax.validation.constraints.NotEmpty;

public class UsuarioFormSenhaDto {

	@NotEmpty
	private String senha;

	public UsuarioFormSenhaDto() {
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
