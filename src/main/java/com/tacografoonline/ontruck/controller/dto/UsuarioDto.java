package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Usuario;

public class UsuarioDto {

	private Long id;
	private String nome;
	private String email;

	public UsuarioDto(Usuario usuario) {
		if (!isNullOrEmpty(usuario)) {
			this.id = usuario.getId();
			this.nome = usuario.getNome();
			this.email = usuario.getEmail();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static Page<UsuarioDto> convert(Page<Usuario> usuarios) {
		return usuarios.map(UsuarioDto::new);
	}

	public static List<UsuarioDto> convertFe(List<Usuario> usuarios) {
		return usuarios.stream().map(UsuarioDto::new).collect(Collectors.toList());
	}
}
