package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Cor;

public class CorDto {

	private Long id;
	@NotEmpty
	private String nome;

	public CorDto() {
	}

	public CorDto(Cor cor) {
		if (!isNullOrEmpty(cor)) {
			this.id = cor.getId();
			this.nome = cor.getNome();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cor toEntity() {
		return new Cor(this.getId(), this.getNome());
	}

	public static Page<CorDto> convert(Page<Cor> cores) {
		return cores.map(CorDto::new);
	}

	public static List<CorDto> convertFe(List<Cor> cores) {
		return cores.stream().map(CorDto::new).collect(Collectors.toList());
	}

}
