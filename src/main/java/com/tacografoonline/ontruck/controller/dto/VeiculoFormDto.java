package com.tacografoonline.ontruck.controller.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Veiculo;

public class VeiculoFormDto {

	@NotEmpty
	private String placa;

	@NotNull
	private Cor cor;
	
	@NotNull
	private Modelo modelo;
	
	@NotNull
	private Integer ano;
	
	@NotNull
	private Integer kmTotal;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Cor getCor() {
		return cor;
	}

	public void setCor(Cor cor) {
		this.cor = cor;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getKmTotal() {
		return kmTotal;
	}

	public void setKmTotal(Integer kmTotal) {
		this.kmTotal = kmTotal;
	}

	public Veiculo toEntity() {
		return new Veiculo(this.getPlaca(), this.getCor(), this.getModelo(), this.getAno(), this.getKmTotal());
	}

}
