package com.tacografoonline.ontruck.controller.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Veiculo;

public class VeiculoDto {

	private Long id;
	private String placa;
	private CorDto cor;
	private ModeloDto modelo;
	private Integer ano;
	private Integer kmTotal;
	
	public VeiculoDto(Veiculo veiculo) {
		if (veiculo != null) {
			this.id = veiculo.getId();
			this.placa = veiculo.getPlaca();
			this.cor = new CorDto(veiculo.getCor());
			this.modelo = new ModeloDto(veiculo.getModelo());
			this.ano = veiculo.getAno();
			this.kmTotal = veiculo.getKmTotal();
		}
	}

	public Long getId() {
		return id;
	}

	public String getPlaca() {
		return placa;
	}

	public CorDto getCor() {
		return cor;
	}

	public ModeloDto getModelo() {
		return modelo;
	}

	public Integer getAno() {
		return ano;
	}

	public Integer getKmTotal() {
		return kmTotal;
	}

	public static Page<VeiculoDto> convert(Page<Veiculo> veiculos) {
		return veiculos.map(VeiculoDto::new);
	}

	public static List<VeiculoDto> converter(List<Veiculo> veiculos) {
		return veiculos.stream().map(VeiculoDto::new).collect(Collectors.toList());
	}

	public Veiculo toEntity() {
		return new Veiculo(id, placa, cor == null ? null : cor.toEntity(), modelo == null ? null : modelo.toEntity(), ano, kmTotal);
	}

}
