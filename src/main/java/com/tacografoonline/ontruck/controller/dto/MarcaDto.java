package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Marca;

public class MarcaDto {

	private Long id;
	@NotEmpty
	private String nome;

	public MarcaDto() {
	}

	public MarcaDto(Marca marca) {
		if (!isNullOrEmpty(marca)) {
			this.id = marca.getId();
			this.nome = marca.getNome();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Marca toEntity() {
		return new Marca(this.getId(), this.getNome());
	}

	public static Page<MarcaDto> convert(Page<Marca> marcas) {
		return marcas.map(MarcaDto::new);
	}

	public static List<MarcaDto> convertFe(List<Marca> marcas) {
		return marcas.stream().map(MarcaDto::new).collect(Collectors.toList());
	}

}
