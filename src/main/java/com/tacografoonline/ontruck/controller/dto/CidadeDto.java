package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Cidade;;

public class CidadeDto {

	private Long id;
	@NotEmpty
	private String nome;
	private EstadoDto estado;

	public CidadeDto() {
	}

	public CidadeDto(Cidade cidade) {
		if (!isNullOrEmpty(cidade)) {
			this.id = cidade.getId();
			this.nome = cidade.getNome();
			this.estado = new EstadoDto(cidade.getEstado());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadoDto getEstado() {
		return estado;
	}

	public void setEstado(EstadoDto estado) {
		this.estado = estado;
	}

	public static Page<CidadeDto> toDtoPage(Page<Cidade> cidades) {
		return cidades.map(CidadeDto::new);
	}

	public Cidade toEntity() {
		return new Cidade(this.id, this.nome, isNullOrEmpty(this.estado)?null:this.estado.toEntity());
	}

}
