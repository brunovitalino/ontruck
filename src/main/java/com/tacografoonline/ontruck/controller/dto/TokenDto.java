package com.tacografoonline.ontruck.controller.dto;

public class TokenDto {

	private String jwt;
	private String tipo;

	public TokenDto(String jwt, String tipo) {
		this.jwt = jwt;
		this.tipo = tipo;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
