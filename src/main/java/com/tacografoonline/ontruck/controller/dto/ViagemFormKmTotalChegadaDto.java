package com.tacografoonline.ontruck.controller.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;

@Getter
public class ViagemFormKmTotalChegadaDto {

	@NotNull
	private Integer kmTotalChegada;

}
