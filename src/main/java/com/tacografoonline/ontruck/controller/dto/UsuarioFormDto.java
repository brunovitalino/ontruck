package com.tacografoonline.ontruck.controller.dto;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import com.tacografoonline.ontruck.model.Usuario;

public class UsuarioFormDto {

	private Long id;
	private String nome;
	private String email;
	private String senha;

	public UsuarioFormDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public UsernamePasswordAuthenticationToken toAuthentication() {
		return new UsernamePasswordAuthenticationToken(this.email, this.senha);
	}

	public Usuario toEntity() {
		return new Usuario(this.getNome(), this.getEmail(), this.getSenha());
	}

}
