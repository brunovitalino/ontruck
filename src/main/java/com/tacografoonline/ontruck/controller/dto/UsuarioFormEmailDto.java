package com.tacografoonline.ontruck.controller.dto;

import javax.validation.constraints.NotEmpty;

public class UsuarioFormEmailDto {

	@NotEmpty
	private String email;

	public UsuarioFormEmailDto() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
