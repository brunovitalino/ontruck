package com.tacografoonline.ontruck.controller.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import lombok.Getter;

@Getter
public class ViagemFormHorarioPartidaDto {

	@NotNull
	private LocalDateTime horarioPartida;

}
