package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import java.util.List;
import java.util.stream.Collectors;

import com.tacografoonline.ontruck.model.Role;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDto {

	private Long id;
	private String nome;

	public RoleDto(Role role) {
		if (!isNullOrEmpty(role)) {
			this.id = role.getId();
			this.nome = role.getNome();
		}
	}

	public static List<RoleDto> convert(List<Role> roles) {
		return roles.stream().map(RoleDto::new).collect(Collectors.toList());
	}
}
