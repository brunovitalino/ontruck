package com.tacografoonline.ontruck.controller.dto;

import javax.validation.constraints.NotNull;

import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.model.enums.MovendoParadoEnum;

import lombok.Getter;

@Getter
public class MovimentacaoFormDto {

	@NotNull
	private Viagem viagem;
	
	@NotNull
	private MovendoParadoEnum status;
	
	@NotNull
	private Integer velocidade;

	public Movimentacao toEntity() {
		return new Movimentacao(this.getViagem(), this.getStatus(), this.getVelocidade());
	}

}
