package com.tacografoonline.ontruck.controller.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Viagem;

public class ViagemDto {

	private Long id;
	private LocalDate data;
	private MotoristaDto motorista1;
	private MotoristaDto motorista2;
	private VeiculoDto veiculo;
	private CidadeDto origem;
	private CidadeDto destino;
	private LocalDateTime horarioPartida;
	private LocalDateTime horarioChegada;
	private Integer kmTotalPartida;
	private Integer kmTotalChegada;
	
	public ViagemDto(Viagem viagem) {
		if (viagem != null) {
			this.id = viagem.getId();
			this.data = viagem.getData();
			this.motorista1 = new MotoristaDto(viagem.getMotorista1());
			this.motorista2 = new MotoristaDto(viagem.getMotorista2());
			this.veiculo = new VeiculoDto(viagem.getVeiculo());
			this.origem = new CidadeDto(viagem.getOrigem());
			this.destino = new CidadeDto(viagem.getDestino());
			this.horarioPartida = viagem.getHorarioPartida();
			this.horarioChegada = viagem.getHorarioChegada();
			this.kmTotalPartida = viagem.getKmTotalPartida();
			this.kmTotalChegada = viagem.getKmTotalChegada();
		}
	}

	public Long getId() {
		return id;
	}

	public LocalDate getData() {
		return data;
	}

	public MotoristaDto getMotorista1() {
		return motorista1;
	}

	public MotoristaDto getMotorista2() {
		return motorista2;
	}

	public VeiculoDto getVeiculo() {
		return veiculo;
	}

	public CidadeDto getOrigem() {
		return origem;
	}

	public CidadeDto getDestino() {
		return destino;
	}

	public LocalDateTime getHorarioPartida() {
		return horarioPartida;
	}

	public LocalDateTime getHorarioChegada() {
		return horarioChegada;
	}

	public Integer getKmTotalPartida() {
		return kmTotalPartida;
	}

	public Integer getKmTotalChegada() {
		return kmTotalChegada;
	}

	public static Page<ViagemDto> convert(Page<Viagem> viagens) {
		return viagens.map(ViagemDto::new);
	}

	public static List<ViagemDto> converter(List<Viagem> viagens) {
		return viagens.stream().map(ViagemDto::new).collect(Collectors.toList());
	}

	public Viagem toEntity() {
		return new Viagem(id, data, motorista1 == null ? null : motorista1.toEntity(),
				motorista2 == null ? null : motorista2.toEntity(), veiculo == null ? null : veiculo.toEntity(),
				origem == null ? null : origem.toEntity(), destino == null ? null : destino.toEntity(), horarioPartida,
				horarioChegada, kmTotalPartida, kmTotalChegada);
	}

}
