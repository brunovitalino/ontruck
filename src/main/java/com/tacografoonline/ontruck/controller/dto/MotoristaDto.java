package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNull;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Motorista;

public class MotoristaDto {

	private Long id;

	@NotEmpty
	private String cpf;

	@NotEmpty
	private String nome;

	public MotoristaDto() {
	}

	public MotoristaDto(Motorista motorista) {
		if (!isNull(motorista)) {
			id = motorista.getId();
			cpf = motorista.getCpf();
			nome = motorista.getNome();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static Page<MotoristaDto> toDtoPage(Page<Motorista> motoristas) {
		return motoristas.map(MotoristaDto::new);
	}

	public Motorista toEntity() {
		return new Motorista(id, cpf, nome);
	}

}
