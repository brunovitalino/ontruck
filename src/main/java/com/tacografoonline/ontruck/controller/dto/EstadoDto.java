package com.tacografoonline.ontruck.controller.dto;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNullOrEmpty;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.domain.Page;

import com.tacografoonline.ontruck.model.Estado;

public class EstadoDto {

	private Long id;
	@NotEmpty
	private String nome;

	public EstadoDto() {
	}

	public EstadoDto(Estado estado) {
		if (!isNullOrEmpty(estado)) {
			this.id = estado.getId();
			this.nome = estado.getNome();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static Page<EstadoDto> toDtoPage(Page<Estado> estados) {
//		return estados.stream().map(EstadoDto::new).collect(Collectors.toList());
		return estados.map(EstadoDto::new);
	}

	public Estado toEntity() {
		return new Estado(this.getId(), this.getNome());
	}

//	public Estado update(Long id, EstadoRepository estadoRepository) {
//		Estado estado = estadoRepository.getOne(id);
//		estado.setNome(this.getNome());
//		return estado;
//	}

}
