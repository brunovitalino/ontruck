package com.tacografoonline.ontruck.controller.dto;

public class ExceptionDto {

	private String exception;
	private String cause;
	private String causa;

	public ExceptionDto(String exception, String cause, String causeTreated) {
		this.exception = exception;
		this.cause = cause;
		this.causa = causeTreated;
	}

	public String getException() {
		return exception;
	}

	public void setException(String causa) {
		this.exception = causa;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getCausa() {
		return causa;
	}

	public void setCausa(String causeTreated) {
		this.causa = causeTreated;
	}

}
