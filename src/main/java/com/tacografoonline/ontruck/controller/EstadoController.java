package com.tacografoonline.ontruck.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tacografoonline.ontruck.controller.dto.EstadoDto;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.service.EstadoService;
import com.tacografoonline.ontruck.service.exception.EstadoException;

@RestController
@RequestMapping("/estados")
public class EstadoController {

	@Autowired
	EstadoService estadoService;

	@GetMapping
	@Cacheable(value = "findAllEstados")
	// Ex. http://localhost:8080/estados?page=0&size=3&sort=id,desc
	public ResponseEntity<Page<EstadoDto>> findAll(@RequestParam(required = false) Optional<String> nome,
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable pageable) {
		try {
			Page<Estado> estados;
			if (nome.isPresent()) {
				estados = estadoService.findAllByNome(nome.get(), pageable);
				return ResponseEntity.ok(EstadoDto.toDtoPage(estados));
			}
			estados = estadoService.findAll(pageable);
			return ResponseEntity.ok(EstadoDto.toDtoPage(estados));
		} catch (EstadoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<EstadoDto> findOne(@PathVariable Long id) {
		try {
			Optional<Estado> estado = estadoService.findOne(id);
			if (estado.isEmpty())
				return ResponseEntity.notFound().build();
			return ResponseEntity.ok(new EstadoDto(estado.get()));
		} catch (EstadoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "findAllEstados", allEntries = true)
	public ResponseEntity<EstadoDto> save(@RequestBody @Valid EstadoDto estadoDto, UriComponentsBuilder uriBuilder) {
		try {
			Estado estado = estadoDto.toEntity();
			estadoService.save(estado);
			URI uri = uriBuilder.path("/estados/{id}").buildAndExpand(estado.getId()).toUri();
			return ResponseEntity.created(uri).body(new EstadoDto(estado));
		} catch (EstadoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllEstados", allEntries = true)
	public ResponseEntity<EstadoDto> update(@PathVariable Long id, @RequestBody @Valid EstadoDto estadoDto) {
		try {
			Estado estado = estadoService.update(id, estadoDto.toEntity());
			return ResponseEntity.ok(new EstadoDto(estado));
		} catch (EstadoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "findAllEstados", allEntries = true)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			estadoService.delete(id);
			return ResponseEntity.ok().build();
		} catch (EstadoException e) {
			e.printStackTrace();
			return ResponseEntity.unprocessableEntity().build();
		}
	}
	
}
