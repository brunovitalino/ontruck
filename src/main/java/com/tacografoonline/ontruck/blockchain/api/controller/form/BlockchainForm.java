package com.tacografoonline.ontruck.blockchain.api.controller.form;

import java.time.LocalDateTime;

import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.Viagem;

public class BlockchainForm {

	private Viagem viagem;
	private LocalDateTime horario;
	private Integer velocidade;
	private String codigo;
	
	public BlockchainForm() {
	}
	
	public Viagem getViagem() {
		return viagem;
	}
	
	public LocalDateTime getHorario() {
		return horario;
	}
	
	public Integer getVelocidade() {
		return velocidade;
	}

	public String getCodigo() {
		return codigo;
	}

	public Movimentacao toEntity() {
		return new Movimentacao(this.getViagem(), this.getHorario(), this.getVelocidade());
	}

}


