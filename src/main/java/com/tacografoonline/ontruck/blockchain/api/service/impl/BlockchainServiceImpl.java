package com.tacografoonline.ontruck.blockchain.api.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tacografoonline.ontruck.blockchain.api.controller.dto.BlockchainDTO;
import com.tacografoonline.ontruck.blockchain.api.service.BlockchainService;
import com.tacografoonline.ontruck.model.MotoristaIdentificacao;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.service.exception.BlockchainException;

@Service
public class BlockchainServiceImpl implements BlockchainService {

	private final String API_PATH = "http://localhost:8084/blockchain";
	private final String SERVICE_PATH = "/tacografo/api";

	@Override
	public BlockchainDTO save(Movimentacao movimentacao, MotoristaIdentificacao identificacao)	throws BlockchainException {
		String url = API_PATH + SERVICE_PATH + "/movimentacoes";
		RestTemplate restTemplate = new RestTemplate();
//		restTemplate.setErrorHandler(new RestTemplateExceptionHandler());

		BlockchainDTO movimentacaoDTO = new BlockchainDTO(movimentacao, identificacao);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		HttpEntity<BlockchainDTO> headersBodyMovimentacao = new HttpEntity<>(movimentacaoDTO, headers);
		try {
			return restTemplate.exchange(url, HttpMethod.POST, headersBodyMovimentacao,
					new ParameterizedTypeReference<BlockchainDTO>() {}).getBody();
		} catch (Exception e) {
			throw new BlockchainException(e.getMessage());
		}
	}

	@Override
	public List<BlockchainDTO> findAll() throws BlockchainException {
		String url = API_PATH + SERVICE_PATH + "/movimentacoes";
		RestTemplate restTemplate = new RestTemplate();
//		restTemplate.setErrorHandler(new RestTemplateExceptionHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		HttpEntity<Movimentacao> headersMovimentacao = new HttpEntity<>(headers);
		try {
			return restTemplate.exchange(url, HttpMethod.GET, headersMovimentacao,
					new ParameterizedTypeReference<List<BlockchainDTO>>() {}).getBody();
		} catch (Exception e) {
			throw new BlockchainException(e.getMessage());
		}
	}

	@Override
	public List<BlockchainDTO> findAllByViagemId(Long viagemId) throws BlockchainException {
		String url = API_PATH + SERVICE_PATH + "/movimentacoes" + "?viagemId=" + viagemId;
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		HttpEntity<Movimentacao> headersMovimentacao = new HttpEntity<>(headers);
		try {
			return restTemplate.exchange(url, HttpMethod.GET, headersMovimentacao,
					new ParameterizedTypeReference<List<BlockchainDTO>>() {}).getBody();
		} catch (Exception e) {
			throw new BlockchainException(e.getMessage());
		}
	}

}