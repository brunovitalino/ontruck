package com.tacografoonline.ontruck.blockchain.api.service;

import java.util.List;

import com.tacografoonline.ontruck.blockchain.api.controller.dto.BlockchainDTO;
import com.tacografoonline.ontruck.model.MotoristaIdentificacao;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.service.exception.BlockchainException;

public interface BlockchainService {

	/**
	 * 
	 * @param movimentacaoForm  movimentacaoForm que contem os dados
	 * @param motoristaIdentity
	 * @return
	 */
	public BlockchainDTO save(Movimentacao movimentacao, MotoristaIdentificacao identificacao)
			throws BlockchainException;

	/**
	 * 
	 * @return
	 */
	public List<BlockchainDTO> findAll() throws BlockchainException;

	/**
	 * 
	 * @param viagemId
	 * @return
	 */
	public List<BlockchainDTO> findAllByViagemId(Long viagemId) throws BlockchainException;

}