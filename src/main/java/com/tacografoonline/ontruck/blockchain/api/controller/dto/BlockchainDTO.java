package com.tacografoonline.ontruck.blockchain.api.controller.dto;

import java.util.UUID;

import com.tacografoonline.ontruck.model.MotoristaIdentificacao;
import com.tacografoonline.ontruck.model.Movimentacao;

public class BlockchainDTO {

	private String viagemId;
	private String horario;
	private String velocidade;
	private String registerCode;
	private String address;
	
	public BlockchainDTO() {
	}

	public BlockchainDTO(Movimentacao movimentacao, MotoristaIdentificacao identificacao) {
		this.viagemId = movimentacao != null ? movimentacao.getViagem() != null ? movimentacao.getViagem().getId() != null ?
				movimentacao.getViagem().getId().toString() : null : null : null;
		this.horario = movimentacao != null ? movimentacao.getHorario() != null ? movimentacao.getHorario().toString() : null : null;
		this.velocidade = movimentacao != null ? movimentacao.getVelocidade() != null ? movimentacao.getVelocidade().toString() : null : null;
		this.registerCode = identificacao != null ? identificacao.getChavePrivada() : null;
		this.address = UUID.randomUUID().toString();
	}
	
	public String getViagemId() {
		return viagemId;
	}
	
	public String getHorario() {
		return horario;
	}
	
	public String getVelocidade() {
		return velocidade;
	}
	
	public String getRegisterCode() {
		return registerCode;
	}
	
	public String getAddress() {
		return address;
	}

}


