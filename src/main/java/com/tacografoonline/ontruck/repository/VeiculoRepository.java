package com.tacografoonline.ontruck.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Veiculo;

@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {

	public Page<Veiculo> findByCorNome(String nome, Pageable pageable);

	public Page<Veiculo> findByModeloNome(String nome, Pageable pageable);

	public Page<Veiculo> findByAno(Integer ano, Pageable pageable);

	@Query("SELECT v FROM Veiculo v WHERE v.kmTotal <= :kmTotal")
	public Page<Veiculo> findWhereKmTotalEqualsOrGreaterThanX(Integer kmTotal, Pageable pageable);

	public Page<Veiculo> findByPlaca(String placa, Pageable pageable);

}
