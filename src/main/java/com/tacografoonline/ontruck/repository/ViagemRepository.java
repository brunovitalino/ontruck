package com.tacografoonline.ontruck.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Viagem;

@Repository
public interface ViagemRepository extends JpaRepository<Viagem, Long> {

	public Page<Viagem> findByData(LocalDate data, Pageable pageable);
	
	public Page<Viagem> findByVeiculoPlaca(String placa, Pageable pageable);
	
}
