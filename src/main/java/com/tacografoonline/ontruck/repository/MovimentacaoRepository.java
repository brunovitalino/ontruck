package com.tacografoonline.ontruck.repository;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Movimentacao;

@Deprecated
@Repository
public interface MovimentacaoRepository extends JpaRepository<Movimentacao, Long> {

	@Query("SELECT m FROM Movimentacao m WHERE m.horario >= :dataInicio and m.horario < :dataFim")
	public Page<Movimentacao> findAllByDataBetween(LocalDateTime dataInicio, LocalDateTime dataFim, Pageable pageable);

	public Page<Movimentacao> findByViagemId(Long id, Pageable pageable);

}
