package com.tacografoonline.ontruck.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tacografoonline.ontruck.model.Config;

public interface ConfigRepository extends JpaRepository<Config, Long> {

}
