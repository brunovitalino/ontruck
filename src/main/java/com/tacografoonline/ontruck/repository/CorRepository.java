package com.tacografoonline.ontruck.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Cor;

@Repository
public interface CorRepository extends JpaRepository<Cor, Long> {

	public Page<Cor> findByNome(String nome, Pageable pageable);

	public Optional<Cor> findByNome(String string);

}
