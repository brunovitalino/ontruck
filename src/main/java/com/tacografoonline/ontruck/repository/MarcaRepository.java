package com.tacografoonline.ontruck.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Marca;

@Repository
public interface MarcaRepository extends JpaRepository<Marca, Long> {

	public Page<Marca> findByNome(String nome, Pageable pageable);

	public Optional<Marca> findByNome(String nome);

}
