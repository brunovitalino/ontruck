package com.tacografoonline.ontruck.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Modelo;

@Repository
public interface ModeloRepository extends JpaRepository<Modelo, Long> {

	public Page<Modelo> findByNome(String nome, Pageable pageable);

	public Page<Modelo> findByMarcaNome(String marcaNome, Pageable pageable);
	public List<Modelo> findByMarcaNome(String marcaNome);

	public Page<Modelo> findAllByNomeAndMarcaNome(String nome, String marcaNome, Pageable pageable);

	public Optional<Modelo> findByNome(String nome);

}
