package com.tacografoonline.ontruck.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Motorista;

@Repository
public interface MotoristaRepository extends JpaRepository<Motorista, Long> {

	public Optional<Motorista> findByCpf(String cpf);

	public Page<Motorista> findByNome(String nome, Pageable pageable);
	
	public Optional<Motorista> findByChavePrivada(String codigoMotorista);

}
