package com.tacografoonline.ontruck.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	public Page<Cidade> findByNome(String nome, Pageable pageable);

	public Page<Cidade> findByEstadoNome(String estadoNome, Pageable pageable);
	public List<Cidade> findByEstadoNome(String estadoNome);

	public Page<Cidade> findAllByNomeAndEstadoNome(String nome, String estadoNome, Pageable pageable);

	public Optional<Cidade> findByNome(String nome);


}
