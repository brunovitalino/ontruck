package com.tacografoonline.ontruck.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tacografoonline.ontruck.model.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long> {

	public Page<Estado> findByNome(String nome, Pageable pageable);

	public Optional<Estado> findByNome(String nome);


}
