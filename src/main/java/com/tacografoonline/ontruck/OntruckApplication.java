package com.tacografoonline.ontruck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author Bruno Vitalino
 * @version 1.0
 * @since 2020-05-01
 *
 */
//@RunWith(SpringRunner.class)
//@EnableJpaAuditing
@SpringBootApplication
@EnableSpringDataWebSupport
@EnableCaching
@EnableSwagger2
public class OntruckApplication {

	public static void main(String[] args) {
		SpringApplication.run(OntruckApplication.class, args);
	}
	
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry
//				.addMapping("/**")
//				.allowedOrigins("http://localhost:4200", "http://127.0.0.1:4200", "http://ontruck.surge.sh")
//                .allowedMethods("POST", "OPTIONS", "GET", "PUT", "DELETE") // "HEAD");
//				// Método abaixo só causa erro no localhost (Access-Control-Allow-Origin).
//				// Em produção, ele deve ser descomentado.
//                .allowedHeaders("*")
////				.allowedHeaders("Origin, X-Requested-With, Content-Type, Accept"
////						+ ", x-client-key, x-client-token, x-client-secret, Authorization")
//                .allowCredentials(true).maxAge(3600);
//			}
//		};
//	}

}
