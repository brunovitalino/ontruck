INSERT INTO public.usuario(id, email, nome, senha) VALUES(1, 'admin@bv.com', 'admin', '$2a$10$KK6zfkUf6LLVNT0E4dc7VutG0Btp9FkwLZD6G3PojyFPJuI/k7iv6');
--"email": "admin@bv.com",
--"senha": "123"
INSERT INTO public.usuario(id, email, nome, senha) VALUES(2, 'bv@bv.com', 'Bruno', '$2a$10$KK6zfkUf6LLVNT0E4dc7VutG0Btp9FkwLZD6G3PojyFPJuI/k7iv6');

INSERT INTO Estado(nome, CREATED, UPDATED) VALUES('Ceará', NOW(), NOW());
INSERT INTO Cidade(nome, estado_id, CREATED, UPDATED) VALUES('Fortaleza', 1, NOW(), NOW());
INSERT INTO Cidade(nome, estado_id, CREATED, UPDATED) VALUES('Sobral', 1, NOW(), NOW());

INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Amarelo', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Azul', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Branco', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Laranja', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Preto', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Rosa', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Roxo', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Vermelho', NOW(), NOW());
INSERT INTO Cor(nome, CREATED, UPDATED) VALUES('Verde', NOW(), NOW());

INSERT INTO Marca(nome, CREATED, UPDATED) VALUES('Chevrolet', NOW(), NOW());
INSERT INTO Modelo(nome, marca_id, CREATED, UPDATED) VALUES('S10', 1, NOW(), NOW());

INSERT INTO veiculo(placa, cor_id, modelo_id, ano, km_total, CREATED, UPDATED)
VALUES('024', 3, 1, 2020, 15000, NOW(), NOW());

INSERT INTO Motorista(nome, cpf, CREATED, UPDATED) VALUES('Jack Tequila', '024', NOW(), NOW());

INSERT INTO viagem(data, horario_partida, horario_chegada, km_total_partida, km_total_chegada,
origem_id, destino_id, motorista1_id, motorista2_id, veiculo_id, CREATED, UPDATED)
VALUES(CURRENT_DATE, NOW(), NOW() + interval '3' day, 45, 1024,
1, 2, 1, null, 1, NOW(), NOW());

INSERT INTO Movimentacao(viagem_id, horario, status, velocidade, CREATED, UPDATED)
VALUES(1, '2021-02-09T15:29:40', 'PARADO', 0, NOW(), NOW());


INSERT INTO config(descricao, true_or_false, CREATED, UPDATED) VALUES('Quando as informações básicas do banco de dados é preenchida, esse registro fica verdadeiro', false, NOW(), NOW());
