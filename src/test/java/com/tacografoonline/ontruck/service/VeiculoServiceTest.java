package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.repository.VeiculoRepository;
import com.tacografoonline.ontruck.service.exception.VeiculoException;
import com.tacografoonline.ontruck.service.impl.VeiculoServiceImpl;

@SpringBootTest
class VeiculoServiceTest {
	
	private static Pageable pageable;

	private Veiculo veiculo;
	private VeiculoRepository veiculoRepository;
	private VeiculoService veiculoService;
	
	VeiculoServiceTest() {
		veiculoRepository = Mockito.mock(VeiculoRepository.class);
		veiculoService = new VeiculoServiceImpl(veiculoRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		Cor cor = new Cor("CorTest");
		Marca marca = new Marca("MarcaTest");
		Modelo modelo = new Modelo("ModeloTest", marca);
		veiculo = new Veiculo("PlacaTest", cor, modelo, 2020, 15000);
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(veiculoRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Veiculo> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(veiculo);
		when(veiculoRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Veiculo> objetoRecebido = veiculoService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (VeiculoException e) {
			fail("Não deve ocorrer VeiculoException.");
		}
	}

	@Test
	void testSuccessFindAllPageable() {
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(veiculo);
		Page<Veiculo> objetoParaRetornar = new PageImpl<Veiculo>(veiculos, pageable, veiculos.size());
		when(veiculoRepository.findAll(pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Veiculo> objetoRecebido = veiculoService.findAll(pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (VeiculoException e) {
			fail("Não deve ocorrer VeiculoException.");
		}
	}

	// Teste falhando com retorno Nulo do método do Repository por algum motivo. Talvez por ser uma custom query.
//	@Test
//	void testSuccessFindAllByData() {
//		List<Veiculo> movimentacoes = new ArrayList<>();
//		movimentacoes.add(veiculo);
//		Page<Veiculo> objetoParaRetornar = new PageImpl<Veiculo>(movimentacoes, pageable, movimentacoes.size());
//		LocalDateTime dataTimeInicio = veiculo.getHorario();
//		LocalDateTime dataTimeFim = veiculo.getHorario().plusDays(1);
//		when(veiculoRepository.findAll(pageable)).thenReturn(objetoParaRetornar);
//		when(veiculoRepository.findAllByDataBetween(dataTimeInicio, dataTimeFim, pageable)).thenReturn(objetoParaRetornar);
//		try {
//			Page<Veiculo> objetoRecebido = veiculoService.findAllByData(dataTimeInicio.toLocalDate(),
//					dataTimeFim.toLocalDate(), pageable);
//			assertTrue(objetoRecebido.get().findAny().isPresent());
//		} catch (VeiculoException e) {
//			fail("Não deve ocorrer VeiculoException.");
//		}
//	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Veiculo> objetoParaRetornar = Optional.ofNullable(new Veiculo());
		when(veiculoRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Veiculo> objetoRecebido = veiculoService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (VeiculoException e) {
			fail("Não deve ocorrer VeiculoException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(veiculoRepository.save(veiculo)).thenReturn(veiculo);
		try {
			Veiculo objetoRecebido = veiculoService.save(veiculo);
			assertTrue(veiculo.getPlaca().equals(objetoRecebido.getPlaca()));
		} catch (VeiculoException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		String placaNova = "PlacaTestUpdated";
		veiculo.setId(idBuscado);
		Veiculo veiculoUpdated = new Veiculo(idBuscado, placaNova, veiculo.getCor(), veiculo.getModelo(),
				veiculo.getAno(), veiculo.getKmTotal());
		Optional<Veiculo> objetoParaRetornar = Optional.ofNullable(veiculoUpdated);
		when(veiculoRepository.findById(veiculo.getId())).thenReturn(objetoParaRetornar);
		try {
			Veiculo objetoRecebido = veiculoService.update(idBuscado, veiculoUpdated);
			assertTrue(placaNova.equals(objetoRecebido.getPlaca()));
		} catch (VeiculoException e) {
			fail("Não deve ocorrer VeiculoException.");
		}
	}

	@Test
	void testSuccessUpdateKmTotal() {
		Long idBuscado = 1L;
		Integer kmTotalUpdated = 20000;
		veiculo.setId(idBuscado);
		Veiculo veiculoUpdated = new Veiculo(idBuscado, veiculo.getPlaca(), veiculo.getCor(), veiculo.getModelo(),
				veiculo.getAno(), kmTotalUpdated);
		Optional<Veiculo> objetoParaRetornar = Optional.ofNullable(veiculoUpdated);
		when(veiculoRepository.findById(veiculo.getId())).thenReturn(objetoParaRetornar);
		try {
			Veiculo objetoRecebido = veiculoService.updateKmTotal(idBuscado, kmTotalUpdated);
			assertTrue(kmTotalUpdated.equals(objetoRecebido.getKmTotal()));
		} catch (VeiculoException e) {
			fail("Não deve ocorrer VeiculoException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			veiculoService.delete(idBuscado);
		} catch (VeiculoException e) {
			fail("Não deve ocorrer VeiculoException.");
		}
	}

}
