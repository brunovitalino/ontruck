package com.tacografoonline.ontruck.service;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.repository.CidadeRepository;
import com.tacografoonline.ontruck.service.exception.CidadeException;
import com.tacografoonline.ontruck.service.impl.CidadeServiceImpl;

@SpringBootTest
class CidadeServiceTest {
	
	private static Pageable pageable;

	private Cidade cidade;
	private CidadeRepository cidadeRepository;
	private CidadeService cidadeService;
	
	CidadeServiceTest() {
		cidadeRepository = Mockito.mock(CidadeRepository.class);
		cidadeService = new CidadeServiceImpl(cidadeRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		Estado estado = new Estado("EstadoTeste");
		cidade = new Cidade("CidadeTeste", estado);
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(cidadeRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Cidade> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(cidade);
		when(cidadeRepository.findAll()).thenReturn(objetoParaRetornar);
		List<Cidade> objetoRecebido = null;
		try {
			objetoRecebido = cidadeService.findAll();
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.size() > 0);
		} catch (CidadeException e) {
			fail("Não deve ocorrer CidadeException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Cidade> cidades = new ArrayList<>();
		cidades.add(cidade);
		Page<Cidade> objetoParaRetornar = new PageImpl<Cidade>(cidades, pageable, cidades.size());
		when(cidadeRepository.findByNome("CidadeTeste", pageable)).thenReturn(objetoParaRetornar);
		Page<Cidade> objetoRecebido = null;
		try {
			objetoRecebido = cidadeService.findAllByNome(Optional.ofNullable("CidadeTeste"), pageable);
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (CidadeException e) {
			fail("Não deve ocorrer CidadeException.");
		}
	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Cidade> objetoParaRetornar = Optional.ofNullable(new Cidade());
		when(cidadeRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Cidade> objetoRecebido = cidadeService.findOne(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (CidadeException e) {
			fail("Não deve ocorrer CidadeException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(cidadeRepository.save(cidade)).thenReturn(cidade);
		try {
			Cidade objetoRecebido = cidadeService.saveOne(cidade);
			assertTrue("CidadeTeste".equals(objetoRecebido.getNome()));
		} catch (CidadeException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		cidade.setId(idBuscado);
		Cidade cidadeUpdated = new Cidade(idBuscado, "CidadeTestUpdated", new Estado("EstadoTeste"));
		Optional<Cidade> objetoParaRetornar = Optional.ofNullable(cidadeUpdated);
		when(cidadeRepository.findById(cidade.getId())).thenReturn(objetoParaRetornar);
		try {
			Cidade objetoRecebido = cidadeService.update(idBuscado, cidadeUpdated);
			assertTrue("CidadeTestUpdated".equals(objetoRecebido.getNome()));
		} catch (CidadeException e) {
			fail("Não deve ocorrer CidadeException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			cidadeService.delete(idBuscado);
		} catch (CidadeException e) {
			fail("Não deve ocorrer CidadeException.");
		}
	}

}
