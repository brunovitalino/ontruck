package com.tacografoonline.ontruck.service;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.repository.MarcaRepository;
import com.tacografoonline.ontruck.service.exception.MarcaException;
import com.tacografoonline.ontruck.service.impl.MarcaServiceImpl;

@SpringBootTest
class MarcaServiceTest {
	
	private static Pageable pageable;

	private Marca marca;
	private MarcaRepository marcaRepository;
	private MarcaService marcaService;
	
	MarcaServiceTest() {
		marcaRepository = Mockito.mock(MarcaRepository.class);
		marcaService = new MarcaServiceImpl(marcaRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		marca = new Marca("MarcaTest");
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(marcaRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Marca> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(marca);
		when(marcaRepository.findAll()).thenReturn(objetoParaRetornar);
		List<Marca> objetoRecebido = null;
		try {
			objetoRecebido = marcaService.findAll();
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.size() > 0);
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Marca> marcas = new ArrayList<>();
		marcas.add(marca);
		Page<Marca> objetoParaRetornar = new PageImpl<Marca>(marcas, pageable, marcas.size());
		when(marcaRepository.findByNome("MarcaTest", pageable)).thenReturn(objetoParaRetornar);
		Page<Marca> objetoRecebido = null;
		try {
			objetoRecebido = marcaService.findAllByNome("MarcaTest", pageable);
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessFindOneById() {
		Long idBuscado = 1L;
		Optional<Marca> objetoParaRetornar = Optional.ofNullable(new Marca());
		when(marcaRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		Optional<Marca> objetoRecebido = null;
		try {
			objetoRecebido = marcaService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessFindOneByNome() {
		String nomeBuscado = "MarcaTest";
		Optional<Marca> objetoParaRetornar = Optional.ofNullable(new Marca());
		when(marcaRepository.findByNome(nomeBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Marca> objetoRecebido = marcaService.findOneByNome(nomeBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(marcaRepository.save(marca)).thenReturn(marca);
		try {
			Marca objetoRecebido = marcaService.save(marca);
			assertTrue("MarcaTest".equals(objetoRecebido.getNome()));
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		marca.setId(idBuscado);
		Marca marcaUpdated = new Marca(idBuscado, "MarcaTestUpdated");
		Optional<Marca> objetoParaRetornar = Optional.ofNullable(marcaUpdated);
		when(marcaRepository.findById(marca.getId())).thenReturn(objetoParaRetornar);
		try {
			Marca objetoRecebido = marcaService.update(idBuscado, marcaUpdated);
			assertTrue("MarcaTestUpdated".equals(objetoRecebido.getNome()));
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			marcaService.delete(idBuscado);
		} catch (MarcaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

}
