package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Usuario;
import com.tacografoonline.ontruck.repository.UsuarioRepository;
import com.tacografoonline.ontruck.service.exception.UsuarioException;
import com.tacografoonline.ontruck.service.impl.UsuarioServiceImpl;

@SpringBootTest
class UsuarioServiceTest {
	
	private static Pageable pageable;

	private Usuario usuario;
	private UsuarioRepository usuarioRepository;
	private UsuarioService usuarioService;
	
	UsuarioServiceTest() {
		usuarioRepository = Mockito.mock(UsuarioRepository.class);
		usuarioService = new UsuarioServiceImpl(usuarioRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		usuario = new Usuario("NomeTest", "EmailTest", "SenhaTest");
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(usuarioRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Usuario> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(usuario);
		when(usuarioRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Usuario> objetoRecebido = usuarioService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessFindAllPageable() {
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(usuario);
		Page<Usuario> objetoParaRetornar = new PageImpl<Usuario>(usuarios, pageable, usuarios.size());
		when(usuarioRepository.findAll(pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Usuario> objetoRecebido = usuarioService.findAll(pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(usuario);
		Page<Usuario> objetoParaRetornar = new PageImpl<Usuario>(usuarios, pageable, usuarios.size());
		when(usuarioRepository.findByNome(usuario.getNome(), pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Usuario> objetoRecebido = usuarioService.findAllByNome(usuario.getNome(), pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Usuario> objetoParaRetornar = Optional.ofNullable(new Usuario());
		when(usuarioRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Usuario> objetoRecebido = usuarioService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(usuarioRepository.save(usuario)).thenReturn(usuario);
		try {
			Usuario objetoRecebido = usuarioService.save(usuario);
			assertTrue(Optional.ofNullable(objetoRecebido).isPresent());
		} catch (UsuarioException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		String nomeUpdated = "NomeTestUpdated";
		usuario.setId(idBuscado);
		Usuario usuarioUpdated = new Usuario(idBuscado, nomeUpdated, usuario.getEmail(), usuario.getSenha());
		Optional<Usuario> objetoParaRetornar = Optional.ofNullable(usuarioUpdated);
		when(usuarioRepository.findById(usuario.getId())).thenReturn(objetoParaRetornar);
		try {
			Usuario objetoRecebido = usuarioService.update(idBuscado, usuarioUpdated);
			assertTrue(nomeUpdated.equals(objetoRecebido.getNome()));
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessUpdateEmail() {
		Long idBuscado = 1L;
		String emailUpdated = "EmailTestUpdated";
		usuario.setId(idBuscado);
		Usuario usuarioUpdated = new Usuario(idBuscado, usuario.getNome(), emailUpdated, usuario.getSenha());
		Optional<Usuario> objetoParaRetornar = Optional.ofNullable(usuarioUpdated);
		when(usuarioRepository.findById(usuario.getId())).thenReturn(objetoParaRetornar);
		try {
			Usuario objetoRecebido = usuarioService.updateEmail(idBuscado, emailUpdated);
			assertTrue(emailUpdated.equals(objetoRecebido.getEmail()));
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessUpdateSenha() {
		Long idBuscado = 1L;
		String senhaUpdated = "jacktequila@tequila.com";
		usuario.setId(idBuscado);
		Usuario usuarioUpdated = new Usuario(idBuscado, usuario.getNome(), usuario.getEmail(), senhaUpdated);
		Optional<Usuario> objetoParaRetornar = Optional.ofNullable(usuarioUpdated);
		when(usuarioRepository.findById(usuario.getId())).thenReturn(objetoParaRetornar);
		try {
			Usuario objetoRecebido = usuarioService.updateEmail(idBuscado, senhaUpdated);
			assertTrue(senhaUpdated.equals(objetoRecebido.getEmail()));
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			usuarioService.delete(idBuscado);
		} catch (UsuarioException e) {
			fail("Não deve ocorrer UsuarioException.");
		}
	}

}
