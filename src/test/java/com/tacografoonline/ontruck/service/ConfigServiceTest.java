package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.tacografoonline.ontruck.model.Config;
import com.tacografoonline.ontruck.repository.ConfigRepository;
import com.tacografoonline.ontruck.service.exception.ConfigException;
import com.tacografoonline.ontruck.service.impl.ConfigServiceImpl;

@SpringBootTest
class ConfigServiceTest {

	private Config config;
	private ConfigRepository configRepository;
	private ConfigService configService;
	
	ConfigServiceTest() {
		configRepository = Mockito.mock(ConfigRepository.class);
		configService = new ConfigServiceImpl(configRepository);
	}

	@BeforeEach
	void beforeEach() {
		config = new Config(false);
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(configRepository);
	}

	@Test
	void testSuccessFillDB() {
		try {
			configService.fillDB();
		} catch (ConfigException e) {
			fail("Não deve ocorrer ConfigException.");
		}
	}

}
