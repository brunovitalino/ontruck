package com.tacografoonline.ontruck.service;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.repository.EstadoRepository;
import com.tacografoonline.ontruck.service.exception.EstadoException;
import com.tacografoonline.ontruck.service.impl.EstadoServiceImpl;

@SpringBootTest
class EstadoServiceTest {
	
	private static Pageable pageable;

	private Estado estado;
	private EstadoRepository estadoRepository;
	private EstadoService estadoService;
	
	EstadoServiceTest() {
		estadoRepository = Mockito.mock(EstadoRepository.class);
		estadoService = new EstadoServiceImpl(estadoRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		estado = new Estado("EstadoTest");
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(estadoRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Estado> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(estado);
		when(estadoRepository.findAll()).thenReturn(objetoParaRetornar);
		List<Estado> objetoRecebido = null;
		try {
			objetoRecebido = estadoService.findAll();
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.size() > 0);
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Estado> estados = new ArrayList<>();
		estados.add(estado);
		Page<Estado> objetoParaRetornar = new PageImpl<Estado>(estados, pageable, estados.size());
		when(estadoRepository.findByNome("EstadoTest", pageable)).thenReturn(objetoParaRetornar);
		Page<Estado> objetoRecebido = null;
		try {
			objetoRecebido = estadoService.findAllByNome("EstadoTest", pageable);
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessFindOneById() {
		Long idBuscado = 1L;
		Optional<Estado> objetoParaRetornar = Optional.ofNullable(new Estado());
		when(estadoRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		Optional<Estado> objetoRecebido = null;
		try {
			objetoRecebido = estadoService.findOne(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessFindOneByNome() {
		String nomeBuscado = "EstadoTest";
		Optional<Estado> objetoParaRetornar = Optional.ofNullable(new Estado());
		when(estadoRepository.findByNome(nomeBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Estado> objetoRecebido = estadoService.findOneByNome(nomeBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(estadoRepository.save(estado)).thenReturn(estado);
		try {
			Estado objetoRecebido = estadoService.save(estado);
			assertTrue("EstadoTest".equals(objetoRecebido.getNome()));
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		estado.setId(idBuscado);
		Estado estadoUpdated = new Estado(idBuscado, "EstadoTestUpdated");
		Optional<Estado> objetoParaRetornar = Optional.ofNullable(estadoUpdated);
		when(estadoRepository.findById(estado.getId())).thenReturn(objetoParaRetornar);
		try {
			Estado objetoRecebido = estadoService.update(idBuscado, estadoUpdated);
			assertTrue("EstadoTestUpdated".equals(objetoRecebido.getNome()));
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			estadoService.delete(idBuscado);
		} catch (EstadoException e) {
			fail("Não deve ocorrer EstadoException.");
		}
	}

}
