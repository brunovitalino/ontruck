package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.tacografoonline.ontruck.model.Role;
import com.tacografoonline.ontruck.repository.RoleRepository;
import com.tacografoonline.ontruck.service.exception.RoleException;
import com.tacografoonline.ontruck.service.impl.RoleServiceImpl;

@SpringBootTest
class RoleServiceTest {
	
	private Role role;
	private RoleRepository roleRepository;
	private RoleService roleService;
	
	RoleServiceTest() {
		roleRepository = Mockito.mock(RoleRepository.class);
		roleService = new RoleServiceImpl(roleRepository);
	}

	@BeforeEach
	void beforeEach() {
		role = new Role("ROLE_Test");
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(roleRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Role> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(role);
		when(roleRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Role> objetoRecebido = roleService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (RoleException e) {
			fail("Não deve ocorrer RoleException.");
		}
	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Role> objetoParaRetornar = Optional.ofNullable(new Role());
		when(roleRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Role> objetoRecebido = roleService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (RoleException e) {
			fail("Não deve ocorrer RoleException.");
		}
	}

	@Test
	void testSuccessFindOneByNome() {
		String nomeBuscado = "ROLE_Test";
		Optional<Role> objetoParaRetornar = Optional.ofNullable(new Role());
		when(roleRepository.findByNome(nomeBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Role> objetoRecebido = roleService.findByNome(nomeBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (RoleException e) {
			fail("Não deve ocorrer RoleException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(roleRepository.save(role)).thenReturn(role);
		try {
			Role objetoRecebido = roleService.save(role);
			assertTrue(Optional.ofNullable(objetoRecebido).isPresent());
		} catch (RoleException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		role.setId(idBuscado);
		String nomeUpdated = role.getNome() + "Updated";
		Role roleUpdated = new Role(nomeUpdated);
		Optional<Role> objetoParaRetornar = Optional.ofNullable(roleUpdated);
		when(roleRepository.findById(role.getId())).thenReturn(objetoParaRetornar);
		try {
			Role objetoRecebido = roleService.update(idBuscado, roleUpdated);
			assertTrue(nomeUpdated.equals(objetoRecebido.getNome()));
		} catch (RoleException e) {
			fail("Não deve ocorrer RoleException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			roleService.delete(idBuscado);
		} catch (RoleException e) {
			fail("Não deve ocorrer RoleException.");
		}
	}

}
