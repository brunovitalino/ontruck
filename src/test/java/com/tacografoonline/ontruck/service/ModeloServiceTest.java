package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.repository.ModeloRepository;
import com.tacografoonline.ontruck.service.exception.ModeloException;
import com.tacografoonline.ontruck.service.impl.ModeloServiceImpl;

@SpringBootTest
class ModeloServiceTest {
	
	private static Pageable pageable;

	private Modelo modelo;
	private ModeloRepository modeloRepository;
	private ModeloService modeloService;
	
	ModeloServiceTest() {
		modeloRepository = Mockito.mock(ModeloRepository.class);
		modeloService = new ModeloServiceImpl(modeloRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		Marca marca = new Marca("MarcaTeste");
		modelo = new Modelo("ModeloTeste", marca);
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(modeloRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Modelo> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(modelo);
		when(modeloRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Modelo> objetoRecebido = modeloService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (ModeloException e) {
			fail("Não deve ocorrer ModeloException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Modelo> modelos = new ArrayList<>();
		modelos.add(modelo);
		Page<Modelo> objetoParaRetornar = new PageImpl<Modelo>(modelos, pageable, modelos.size());
		when(modeloRepository.findByNome("ModeloTeste", pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Modelo> objetoRecebido = modeloService.findAllByNome("ModeloTeste", pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (ModeloException e) {
			fail("Não deve ocorrer ModeloException.");
		}
	}

	@Test
	void testSuccessFindAllByNomeAndMarcaNome() {
		List<Modelo> modelos = new ArrayList<>();
		modelos.add(modelo);
		Page<Modelo> objetoParaRetornar = new PageImpl<Modelo>(modelos, pageable, modelos.size());
		when(modeloRepository.findAllByNomeAndMarcaNome(modelo.getNome(), modelo.getMarca().getNome(), pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Modelo> objetoRecebido = modeloService.findAllByNomeAndMarcaNome(modelo.getNome(), modelo.getMarca().getNome(), pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (ModeloException e) {
			fail("Não deve ocorrer ModeloException.");
		}
	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Modelo> objetoParaRetornar = Optional.ofNullable(new Modelo());
		when(modeloRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Modelo> objetoRecebido = modeloService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (ModeloException e) {
			fail("Não deve ocorrer ModeloException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(modeloRepository.save(modelo)).thenReturn(modelo);
		try {
			Modelo objetoRecebido = modeloService.save(modelo);
			assertTrue("ModeloTeste".equals(objetoRecebido.getNome()));
		} catch (ModeloException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		modelo.setId(idBuscado);
		Modelo modeloUpdated = new Modelo(idBuscado, "ModeloTestUpdated", new Marca("MarcaTeste"));
		Optional<Modelo> objetoParaRetornar = Optional.ofNullable(modeloUpdated);
		when(modeloRepository.findById(modelo.getId())).thenReturn(objetoParaRetornar);
		try {
			Modelo objetoRecebido = modeloService.update(idBuscado, modeloUpdated);
			assertTrue("ModeloTestUpdated".equals(objetoRecebido.getNome()));
		} catch (ModeloException e) {
			fail("Não deve ocorrer ModeloException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			modeloService.delete(idBuscado);
		} catch (ModeloException e) {
			fail("Não deve ocorrer ModeloException.");
		}
	}

}
