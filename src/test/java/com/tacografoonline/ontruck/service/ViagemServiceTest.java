package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.repository.ViagemRepository;
import com.tacografoonline.ontruck.service.exception.ViagemException;
import com.tacografoonline.ontruck.service.impl.ViagemServiceImpl;

@SpringBootTest
class ViagemServiceTest {
	
	private static Pageable pageable;

	private Viagem viagem;
	private ViagemRepository viagemRepository;
	private ViagemService viagemService;
	
	ViagemServiceTest() {
		viagemRepository = Mockito.mock(ViagemRepository.class);
		viagemService = new ViagemServiceImpl(viagemRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		LocalDate dataViagem = LocalDate.now();
		Motorista motorista1 = new Motorista("99988877701", "MotoristaTeste");
		Motorista motorista2 = new Motorista("99988877702", "MotoristaTeste2");
		Cor cor = new Cor("CorTest");
		Marca marca = new Marca("MarcaTest");
		Modelo modelo = new Modelo("ModeloTest", marca);
		Veiculo veiculo = new Veiculo("PlacaTest", cor, modelo, 2020, 15000);
		Estado estado = new Estado("EstadoTeste");
		Cidade origem = new Cidade("OrigemTest", estado);
		Cidade destino = new Cidade("DestinoTest", estado);
		LocalDateTime horarioPartida = LocalDateTime.now().minusDays(1);
		LocalDateTime horarioChegada = LocalDateTime.now().plusDays(1);
		Integer kmTotalPartida = 0;
		Integer kmTotalChegada = 45;
		viagem = new Viagem(dataViagem, motorista1, motorista2, veiculo, origem, destino, horarioPartida,
				horarioChegada, kmTotalPartida, kmTotalChegada);
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(viagemRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Viagem> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(viagem);
		when(viagemRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Viagem> objetoRecebido = viagemService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessFindAllPageable() {
		List<Viagem> viagens = new ArrayList<>();
		viagens.add(viagem);
		Page<Viagem> objetoParaRetornar = new PageImpl<Viagem>(viagens, pageable, viagens.size());
		when(viagemRepository.findAll(pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Viagem> objetoRecebido = viagemService.findAll(pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessFindAllByData() {
		List<Viagem> viagens = new ArrayList<>();
		viagens.add(viagem);
		Page<Viagem> objetoParaRetornar = new PageImpl<Viagem>(viagens, pageable, viagens.size());
		when(viagemRepository.findByData(viagem.getData(), pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Viagem> objetoRecebido = viagemService.findAllByData(viagem.getData(), pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessFindAllByVeiculoPlaca() {
		List<Viagem> viagens = new ArrayList<>();
		viagens.add(viagem);
		Page<Viagem> objetoParaRetornar = new PageImpl<Viagem>(viagens, pageable, viagens.size());
		when(viagemRepository.findByVeiculoPlaca(viagem.getVeiculo().getPlaca(), pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Viagem> objetoRecebido = viagemService.findAllByVeiculoPlaca(viagem.getVeiculo().getPlaca(), pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Viagem> objetoParaRetornar = Optional.ofNullable(new Viagem());
		when(viagemRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Viagem> objetoRecebido = viagemService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(viagemRepository.save(viagem)).thenReturn(viagem);
		try {
			Viagem objetoRecebido = viagemService.save(viagem);
			assertTrue(Optional.ofNullable(objetoRecebido).isPresent());
		} catch (ViagemException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		Integer kmTotalChegada = 60;
		viagem.setId(idBuscado);
		Viagem viagemUpdated = new Viagem(idBuscado, viagem.getData(), viagem.getMotorista1(), viagem.getMotorista2(),
				viagem.getVeiculo(), viagem.getOrigem(), viagem.getDestino(), viagem.getHorarioPartida(),
				viagem.getHorarioChegada(), viagem.getKmTotalPartida(), kmTotalChegada);
		Optional<Viagem> objetoParaRetornar = Optional.ofNullable(viagemUpdated);
		when(viagemRepository.findById(viagem.getId())).thenReturn(objetoParaRetornar);
		try {
			Viagem objetoRecebido = viagemService.update(idBuscado, viagemUpdated);
			assertTrue(kmTotalChegada.equals(objetoRecebido.getKmTotalChegada()));
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessUpdateHorarioPartida() {
		Long idBuscado = 1L;
		LocalDateTime horarioPartida = LocalDateTime.now().minusDays(5).plusHours(3);
		viagem.setId(idBuscado);
		Viagem viagemUpdated = new Viagem(idBuscado, viagem.getData(), viagem.getMotorista1(), viagem.getMotorista2(),
				viagem.getVeiculo(), viagem.getOrigem(), viagem.getDestino(), horarioPartida,
				viagem.getHorarioChegada(), viagem.getKmTotalPartida(), viagem.getKmTotalChegada());
		Optional<Viagem> objetoParaRetornar = Optional.ofNullable(viagemUpdated);
		when(viagemRepository.findById(viagem.getId())).thenReturn(objetoParaRetornar);
		try {
			Viagem objetoRecebido = viagemService.updateHorarioPartida(idBuscado, horarioPartida);
			assertTrue(horarioPartida.equals(objetoRecebido.getHorarioPartida()));
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessUpdateHorarioChegada() {
		Long idBuscado = 1L;
		LocalDateTime horarioChegada = LocalDateTime.now().plusDays(3).plusHours(3);
		viagem.setId(idBuscado);
		Viagem viagemUpdated = new Viagem(idBuscado, viagem.getData(), viagem.getMotorista1(), viagem.getMotorista2(),
				viagem.getVeiculo(), viagem.getOrigem(), viagem.getDestino(), viagem.getHorarioPartida(),
				horarioChegada, viagem.getKmTotalPartida(), viagem.getKmTotalChegada());
		Optional<Viagem> objetoParaRetornar = Optional.ofNullable(viagemUpdated);
		when(viagemRepository.findById(viagem.getId())).thenReturn(objetoParaRetornar);
		try {
			Viagem objetoRecebido = viagemService.updateHorarioChegada(idBuscado, horarioChegada);
			assertTrue(horarioChegada.equals(objetoRecebido.getHorarioChegada()));
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessUpdateKmTotalPartida() {
		Long idBuscado = 1L;
		Integer kmTotalPartida = 60;
		viagem.setId(idBuscado);
		Viagem viagemUpdated = new Viagem(idBuscado, viagem.getData(), viagem.getMotorista1(), viagem.getMotorista2(),
				viagem.getVeiculo(), viagem.getOrigem(), viagem.getDestino(), viagem.getHorarioPartida(),
				viagem.getHorarioChegada(), kmTotalPartida, viagem.getKmTotalChegada());
		Optional<Viagem> objetoParaRetornar = Optional.ofNullable(viagemUpdated);
		when(viagemRepository.findById(viagem.getId())).thenReturn(objetoParaRetornar);
		try {
			Viagem objetoRecebido = viagemService.updateKmTotalPartida(idBuscado, kmTotalPartida);
			assertTrue(kmTotalPartida.equals(objetoRecebido.getKmTotalPartida()));
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessUpdateKmTotalChegada() {
		Long idBuscado = 1L;
		Integer kmTotalChegada = 60;
		viagem.setId(idBuscado);
		Viagem viagemUpdated = new Viagem(idBuscado, viagem.getData(), viagem.getMotorista1(), viagem.getMotorista2(),
				viagem.getVeiculo(), viagem.getOrigem(), viagem.getDestino(), viagem.getHorarioPartida(),
				viagem.getHorarioChegada(), viagem.getKmTotalPartida(), kmTotalChegada);
		Optional<Viagem> objetoParaRetornar = Optional.ofNullable(viagemUpdated);
		when(viagemRepository.findById(viagem.getId())).thenReturn(objetoParaRetornar);
		try {
			Viagem objetoRecebido = viagemService.updateKmTotalChegada(idBuscado, kmTotalChegada);
			assertTrue(kmTotalChegada.equals(objetoRecebido.getKmTotalChegada()));
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			viagemService.delete(idBuscado);
		} catch (ViagemException e) {
			fail("Não deve ocorrer ViagemException.");
		}
	}

}
