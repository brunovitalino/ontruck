package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.model.enums.MovendoParadoEnum;
import com.tacografoonline.ontruck.repository.MovimentacaoRepository;
import com.tacografoonline.ontruck.service.exception.MovimentacaoException;
import com.tacografoonline.ontruck.service.impl.MovimentacaoServiceImplOLD;

@SpringBootTest
class MovimentacaoServiceTest {
	
	private static Pageable pageable;

	private Movimentacao movimentacao;
	private MovimentacaoRepository movimentacaoRepository;
	private MovimentacaoServiceOLD movimentacaoService;
	
	MovimentacaoServiceTest() {
		movimentacaoRepository = Mockito.mock(MovimentacaoRepository.class);
		movimentacaoService = new MovimentacaoServiceImplOLD(movimentacaoRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		LocalDate dataViagem = LocalDate.now();
		Motorista motorista1 = new Motorista("99988877701", "MotoristaTeste");
		Motorista motorista2 = new Motorista("99988877702", "MotoristaTeste2");
		Cor cor = new Cor("CorTest");
		Marca marca = new Marca("MarcaTest");
		Modelo modelo = new Modelo("ModeloTest", marca);
		Veiculo veiculo = new Veiculo("PlacaTest", cor, modelo, 2020, 15000);
		Estado estado = new Estado("EstadoTeste");
		Cidade origem = new Cidade("OrigemTest", estado);
		Cidade destino = new Cidade("DestinoTest", estado);
		LocalDateTime horarioPartida = LocalDateTime.now().minusDays(1);
		LocalDateTime horarioChegada = LocalDateTime.now().plusDays(1);
		Integer kmTotalPartida = 0;
		Integer kmTotalChegada = 45;
		Viagem viagem = new Viagem(dataViagem, motorista1, motorista2, veiculo, origem, destino, horarioPartida,
				horarioChegada, kmTotalPartida, kmTotalChegada);
		MovendoParadoEnum status = MovendoParadoEnum.PARADO;
		LocalDateTime horario = LocalDateTime.now();
		Integer velocidade = 60;
		movimentacao = new Movimentacao(viagem, status, horario, velocidade);
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(movimentacaoRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Movimentacao> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(movimentacao);
		when(movimentacaoRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Movimentacao> objetoRecebido = movimentacaoService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (MovimentacaoException e) {
			fail("Não deve ocorrer MovimentacaoException.");
		}
	}

	// Teste falhando com retorno Nulo do método do Repository por algum motivo. Talvez por ser uma custom query.
//	@Test
//	void testSuccessFindAllByData() {
//		List<Movimentacao> movimentacoes = new ArrayList<>();
//		movimentacoes.add(movimentacao);
//		Page<Movimentacao> objetoParaRetornar = new PageImpl<Movimentacao>(movimentacoes, pageable, movimentacoes.size());
//		LocalDateTime dataTimeInicio = movimentacao.getHorario();
//		LocalDateTime dataTimeFim = movimentacao.getHorario().plusDays(1);
//		when(movimentacaoRepository.findAll(pageable)).thenReturn(objetoParaRetornar);
//		when(movimentacaoRepository.findAllByDataBetween(dataTimeInicio, dataTimeFim, pageable)).thenReturn(objetoParaRetornar);
//		try {
//			Page<Movimentacao> objetoRecebido = movimentacaoService.findAllByData(dataTimeInicio.toLocalDate(),
//					dataTimeFim.toLocalDate(), pageable);
//			assertTrue(objetoRecebido.get().findAny().isPresent());
//		} catch (MovimentacaoException e) {
//			fail("Não deve ocorrer MovimentacaoException.");
//		}
//	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Movimentacao> objetoParaRetornar = Optional.ofNullable(new Movimentacao());
		when(movimentacaoRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Movimentacao> objetoRecebido = movimentacaoService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (MovimentacaoException e) {
			fail("Não deve ocorrer MovimentacaoException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(movimentacaoRepository.save(movimentacao)).thenReturn(movimentacao);
		try {
			Movimentacao objetoRecebido = movimentacaoService.save(movimentacao);
			assertTrue(MovendoParadoEnum.PARADO.equals(objetoRecebido.getStatus()));
		} catch (MovimentacaoException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		MovendoParadoEnum statusNovo = MovendoParadoEnum.PARADO;
		movimentacao.setId(idBuscado);
		Movimentacao movimentacaoUpdated = new Movimentacao(idBuscado, movimentacao.getViagem(), statusNovo,
				movimentacao.getHorario(), movimentacao.getVelocidade());
		Optional<Movimentacao> objetoParaRetornar = Optional.ofNullable(movimentacaoUpdated);
		when(movimentacaoRepository.findById(movimentacao.getId())).thenReturn(objetoParaRetornar);
		try {
			Movimentacao objetoRecebido = movimentacaoService.update(idBuscado, movimentacaoUpdated);
			assertTrue(statusNovo.equals(objetoRecebido.getStatus()));
		} catch (MovimentacaoException e) {
			fail("Não deve ocorrer MovimentacaoException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			movimentacaoService.delete(idBuscado);
		} catch (MovimentacaoException e) {
			fail("Não deve ocorrer MovimentacaoException.");
		}
	}

}
