package com.tacografoonline.ontruck.service;

import static com.tacografoonline.ontruck.util.OntruckUtil.isNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.repository.CorRepository;
import com.tacografoonline.ontruck.service.exception.CorException;
import com.tacografoonline.ontruck.service.impl.CorServiceImpl;

@SpringBootTest
class CorServiceTest {
	
	private static Pageable pageable;

	private Cor cor;
	private CorRepository corRepository;
	private CorService corService;
	
	CorServiceTest() {
		corRepository = Mockito.mock(CorRepository.class);
		corService = new CorServiceImpl(corRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		cor = new Cor("CorTest");
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(corRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Cor> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(cor);
		when(corRepository.findAll()).thenReturn(objetoParaRetornar);
		List<Cor> objetoRecebido = null;
		try {
			objetoRecebido = corService.findAll();
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.size() > 0);
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Cor> cors = new ArrayList<>();
		cors.add(cor);
		Page<Cor> objetoParaRetornar = new PageImpl<Cor>(cors, pageable, cors.size());
		when(corRepository.findByNome("CorTest", pageable)).thenReturn(objetoParaRetornar);
		Page<Cor> objetoRecebido = null;
		try {
			objetoRecebido = corService.findAllByNome("CorTest", pageable);
			assertFalse(isNull(objetoRecebido));
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

	@Test
	void testSuccessFindOneById() {
		Long idBuscado = 1L;
		Optional<Cor> objetoParaRetornar = Optional.ofNullable(new Cor());
		when(corRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		Optional<Cor> objetoRecebido = null;
		try {
			objetoRecebido = corService.findOneById(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

	@Test
	void testSuccessFindOneByNome() {
		String nomeBuscado = "CorTest";
		Optional<Cor> objetoParaRetornar = Optional.ofNullable(new Cor());
		when(corRepository.findByNome(nomeBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Cor> objetoRecebido = corService.findOneByNome(nomeBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(corRepository.save(cor)).thenReturn(cor);
		try {
			Cor objetoRecebido = corService.save(cor);
			assertTrue("CorTest".equals(objetoRecebido.getNome()));
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		cor.setId(idBuscado);
		Cor corUpdated = new Cor(idBuscado, "CorTestUpdated");
		Optional<Cor> objetoParaRetornar = Optional.ofNullable(corUpdated);
		when(corRepository.findById(cor.getId())).thenReturn(objetoParaRetornar);
		try {
			Cor objetoRecebido = corService.update(idBuscado, corUpdated);
			assertTrue("CorTestUpdated".equals(objetoRecebido.getNome()));
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			corService.delete(idBuscado);
		} catch (CorException e) {
			fail("Não deve ocorrer CorException.");
		}
	}

}
