package com.tacografoonline.ontruck.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.repository.MotoristaRepository;
import com.tacografoonline.ontruck.service.exception.MotoristaException;
import com.tacografoonline.ontruck.service.impl.MotoristaServiceImpl;

@SpringBootTest
class MotoristaServiceTest {
	
	private static Pageable pageable;

	private Motorista motorista;
	private MotoristaRepository motoristaRepository;
	private MotoristaService motoristaService;
	
	MotoristaServiceTest() {
		motoristaRepository = Mockito.mock(MotoristaRepository.class);
		motoristaService = new MotoristaServiceImpl(motoristaRepository);
	}
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int paginaAtual=0, qtdDeItensPorPagina=15;
		pageable = PageRequest.of(paginaAtual, qtdDeItensPorPagina, sort);
	}

	@BeforeEach
	void beforeEach() {
		motorista = new Motorista("99988877701", "MotoristaTeste");
	}

	@AfterEach
	void resetMokito() {
		Mockito.reset(motoristaRepository);
	}

	@Test
	void testSuccessFindAll() {
		List<Motorista> objetoParaRetornar = new ArrayList<>();
		objetoParaRetornar.add(motorista);
		when(motoristaRepository.findAll()).thenReturn(objetoParaRetornar);
		try {
			List<Motorista> objetoRecebido = motoristaService.findAll();
			assertTrue(objetoRecebido.size() > 0);
		} catch (MotoristaException e) {
			fail("Não deve ocorrer MotoristaException.");
		}
	}

	@Test
	void testSuccessFindAllByNome() {
		List<Motorista> motoristas = new ArrayList<>();
		motoristas.add(motorista);
		Page<Motorista> objetoParaRetornar = new PageImpl<Motorista>(motoristas, pageable, motoristas.size());
		when(motoristaRepository.findByNome("MotoristaTeste", pageable)).thenReturn(objetoParaRetornar);
		try {
			Page<Motorista> objetoRecebido = motoristaService.findAllByNome("MotoristaTeste", pageable);
			assertTrue(objetoRecebido.get().findAny().isPresent());
		} catch (MotoristaException e) {
			fail("Não deve ocorrer MotoristaException.");
		}
	}

	@Test
	void testSuccessFindOne() {
		Long idBuscado = 1L;
		Optional<Motorista> objetoParaRetornar = Optional.ofNullable(new Motorista());
		when(motoristaRepository.findById(idBuscado)).thenReturn(objetoParaRetornar);
		try {
			Optional<Motorista> objetoRecebido = motoristaService.findOne(idBuscado);
			assertTrue(objetoRecebido.isPresent());
		} catch (MotoristaException e) {
			fail("Não deve ocorrer MotoristaException.");
		}
	}

	@Test
	void testSuccessSave() {
		when(motoristaRepository.save(motorista)).thenReturn(motorista);
		try {
			Motorista objetoRecebido = motoristaService.save(motorista);
			assertTrue("MotoristaTeste".equals(objetoRecebido.getNome()));
		} catch (MotoristaException e) {
			fail("Não deve ocorrer MarcaException.");
		}
	}

	@Test
	void testSuccessUpdate() {
		Long idBuscado = 1L;
		motorista.setId(idBuscado);
		Motorista motoristaUpdated = new Motorista(idBuscado, motorista.getCpf(), "MotoristaTestUpdated");
		Optional<Motorista> objetoParaRetornar = Optional.ofNullable(motoristaUpdated);
		when(motoristaRepository.findById(motorista.getId())).thenReturn(objetoParaRetornar);
		try {
			Optional<Motorista> objetoRecebido = motoristaService.update(idBuscado, motoristaUpdated);
			assertTrue("MotoristaTestUpdated".equals(objetoRecebido.get().getNome()));
		} catch (MotoristaException e) {
			fail("Não deve ocorrer MotoristaException.");
		}
	}

	@Test
	void testSuccessDelete() {
		Long idBuscado = 1L;
		try {
			motoristaService.delete(idBuscado);
		} catch (MotoristaException e) {
			fail("Não deve ocorrer MotoristaException.");
		}
	}

}
