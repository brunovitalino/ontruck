package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class ModeloRepositoryTest {
	
	private static Pageable pageable;

	private Modelo modelo;
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		Marca marca = em.persist(new Marca("MarcaTest"));
		modelo = new Modelo("ModeloTest", marca);
	}

	@Test
	void testSuccessSaveModelo() {
		try {
			Modelo objetoRecebido = modeloRepository.save(modelo);
			assertEquals(modelo, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveModeloNulo() {
		try {
			modeloRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllModelo() {
		em.persist(modelo);
		Page<Modelo> modelos = modeloRepository.findAll(pageable);
		assertTrue(modelos.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllModeloByNome() {
		em.persist(modelo);
		Page<Modelo> modelos = modeloRepository.findByNome(modelo.getNome(), pageable);
		assertTrue(modelos.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindOneModeloByNome() {
		em.persist(modelo);
		Optional<Modelo> modelo = modeloRepository.findByNome(this.modelo.getNome());
		assertTrue(modelo.isPresent());
	}

	@Test
	void testSuccessFindAllByNomeAndMarcaNome() {
		em.persist(modelo);
		Page<Modelo> modelos = modeloRepository.findAllByNomeAndMarcaNome(modelo.getNome(), modelo.getMarca().getNome(),
				pageable);
		assertTrue(modelos.get().findAny().isPresent());
	}

	@Test
	void testSuccessUpdateModelo() {
		em.persist(modelo);
		Optional<Modelo> objetoRecebido = modeloRepository.findById(modelo.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(modelo.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome("ModeloUpdated");
		Optional<Modelo> optionalUpdated = modeloRepository.findById(modelo.getId());
		assertEquals("ModeloUpdated", optionalUpdated.get().getNome());
	}
	
	@Test
	void testSuccessDeleteModeloById() {
		Modelo objetoRecebido = em.persist(modelo);
		Optional<Modelo> optional = modeloRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		modeloRepository.delete(objetoRecebido);
		optional = modeloRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}