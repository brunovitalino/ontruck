package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Cor;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class CorRepositoryTest {
	
	private static Pageable pageable;

	private Cor cor;
	
	@Autowired
	CorRepository corRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		cor = new Cor("CorTest");
	}

	@Test
	void testSuccessSaveCor() {
		try {
			Cor objetoRecebido = corRepository.save(cor);
			assertEquals(cor, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveCorNulo() {
		try {
			corRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllCor() {
		em.persist(cor);
		Page<Cor> estados = corRepository.findAll(pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllCorByNome() {
		em.persist(cor);
		Page<Cor> estados = corRepository.findByNome(cor.getNome(), pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindOneCorByNome() {
		em.persist(cor);
		Optional<Cor> estados = corRepository.findByNome(cor.getNome());
		assertTrue(estados.isPresent());
	}

	@Test
	void testSuccessUpdateCor() {
		em.persist(cor);
		Optional<Cor> objetoRecebido = corRepository.findById(cor.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(cor.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome(cor.getNome() + "Updated");
		Optional<Cor> objetoRecebidoUpdated = corRepository.findById(cor.getId());
		assertEquals("CorTestUpdated", objetoRecebidoUpdated.get().getNome());
	}

	@Test
	void testFailUpdateCor() {
		Optional<Cor> objetoRecebido = corRepository.findByNome(cor.getNome());
		if (objetoRecebido.isPresent()) {
			fail("O item não deveria existir.");
		}
	}
	
	@Test
	void testSuccessDeleteCorById() {
		Cor objetoRecebido = em.persist(cor);
		Optional<Cor> optional = corRepository.findById(cor.getId());
		assertTrue(optional.isPresent());
		corRepository.delete(objetoRecebido);
		optional = corRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}