package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Estado;

@ExtendWith(SpringExtension.class)
@DataJpaTest
// Permite o uso de bancos reais de persistência p/ realização de testes
// e não somente os bancos em memória como H2.
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class EstadoRepositoryTest {
	
	private static Pageable pageable;

	private Estado estado;
	
	@Autowired
	EstadoRepository estadoRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		estado = new Estado("EstadoTest");
	}

	@Test
	void testSuccessSaveEstado() {
		try {
			Estado objetoRecebido = estadoRepository.save(estado);
			assertEquals(estado, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveEstadoNulo() {
		try {
			estadoRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllEstado() {
		em.persist(estado);
		Page<Estado> estados = estadoRepository.findAll(pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllEstadoByNome() {
		em.persist(estado);
		Page<Estado> estados = estadoRepository.findByNome(estado.getNome(), pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindOneEstadoByNome() {
		em.persist(estado);
		Optional<Estado> objetoRecebido = estadoRepository.findByNome(estado.getNome());
		assertTrue(objetoRecebido.isPresent());
	}

	@Test
	void testSuccessUpdateEstado() {
		em.persist(estado);
		Optional<Estado> objetoRecebido = estadoRepository.findById(estado.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(estado.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome("EstadoTestUpdated");
		Optional<Estado> optionalUpdated = estadoRepository.findById(estado.getId());
		assertEquals("EstadoTestUpdated", optionalUpdated.get().getNome());
	}
	
	@Test
	void testSuccessDeleteEstadoById() {
		Estado objetoRecebido = em.persist(estado);
		Optional<Estado> optional = estadoRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		estadoRepository.delete(objetoRecebido);
		optional = estadoRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}