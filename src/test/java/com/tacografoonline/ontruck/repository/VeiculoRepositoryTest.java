package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Veiculo;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class VeiculoRepositoryTest {
	
	private static Pageable pageable;

	private Veiculo veiculo;
	
	@Autowired
	VeiculoRepository veiculoRepository;
	
	@Autowired
	CorRepository corRepository;
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "created");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		Cor cor = em.persist(new Cor("CorTest"));
		Marca marca = em.persist(new Marca("MarcaTest"));
		Modelo modelo = em.persist(new Modelo("ModeloTest", marca));
		veiculo = new Veiculo("PlacaTest", cor, modelo, 2020, 15000);
	}

	@Test
	void testSuccessSaveVeiculo() {
		try {
			Veiculo objetoRecebido = veiculoRepository.save(veiculo);
			assertEquals(veiculo, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveVeiculoNulo() {
		try {
			veiculoRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllVeiculo() {
		em.persist(veiculo);
		Page<Veiculo> veiculos = veiculoRepository.findAll(pageable);
		assertTrue(veiculos.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllVeiculoByCorNome() {
		em.persist(veiculo);
		Page<Veiculo> veiculos = veiculoRepository.findByCorNome(veiculo.getCor().getNome(), pageable);
		assertTrue(veiculos.hasContent());
	}

	@Test
	void testSuccessFindAllVeiculoByModeloNome() {
		em.persist(veiculo);
		Page<Veiculo> veiculos = veiculoRepository.findByModeloNome(veiculo.getModelo().getNome(), pageable);
		assertTrue(veiculos.hasContent());
	}

	@Test
	void testSuccessFindAllVeiculoByAno() {
		em.persist(veiculo);
		Page<Veiculo> veiculos = veiculoRepository.findByAno(veiculo.getAno(), pageable);
		assertTrue(veiculos.hasContent());
	}

	@Test
	void testSuccessFindAllVeiculoWhereKmTotalEqualsOrGreaterThanX() {
		em.persist(veiculo);
		Page<Veiculo> veiculos = veiculoRepository.findWhereKmTotalEqualsOrGreaterThanX(15000, pageable);
		assertTrue(veiculos.hasContent());
	}

	@Test
	void testSuccessFindOneVeiculoByPlaca() {
		em.persist(veiculo);
		Page<Veiculo> veiculo = veiculoRepository.findByPlaca(this.veiculo.getPlaca(), pageable);
		assertTrue(veiculo.hasContent());
	}

	@Test
	void testSuccessUpdateVeiculo() {
		em.persist(veiculo);
		Optional<Veiculo> objetoRecebido = veiculoRepository.findById(veiculo.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(veiculo.getPlaca(), objetoRecebido.get().getPlaca());
		objetoRecebido.get().setPlaca("PlacaTestUpdated");
		Optional<Veiculo> optionalUpdated = veiculoRepository.findById(veiculo.getId());
		assertEquals("PlacaTestUpdated", optionalUpdated.get().getPlaca());
	}
	
	@Test
	void testSuccessDeleteVeiculoById() {
		Veiculo objetoRecebido = em.persist(veiculo);
		Optional<Veiculo> optional = veiculoRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		veiculoRepository.delete(objetoRecebido);
		optional = veiculoRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}