package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Motorista;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class MotoristaRepositoryTest {
	
	private static Pageable pageable;

	private Motorista motorista;
	
	@Autowired
	MotoristaRepository motoristaRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		motorista = new Motorista("99988877701", "MotoristaTest");
	}

	@Test
	void testSuccessSaveMotorista() {
		try {
			Motorista objetoRecebido = motoristaRepository.save(motorista);
			assertEquals(motorista, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveMotoristaNulo() {
		try {
			motoristaRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllMotorista() {
		em.persist(motorista);
		Page<Motorista> motoristas = motoristaRepository.findAll(pageable);
		assertTrue(motoristas.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindMotoristaByNome() {
		em.persist(motorista);
		Page<Motorista> motoristas = motoristaRepository.findByNome("MotoristaTest", pageable);
		assertTrue(motoristas.get().findAny().isPresent());
	}

	@Test
	void testSuccessUpdateMotorista() {
		em.persist(motorista);
		Optional<Motorista> objetoRecebido = motoristaRepository.findById(motorista.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(motorista.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome("MotoristaUpdated");
		Optional<Motorista> optionalUpdated = motoristaRepository.findById(motorista.getId());
		assertEquals("MotoristaUpdated", optionalUpdated.get().getNome());
	}
	
	@Test
	void testSuccessDeleteMotoristaById() {
		Motorista objetoRecebido = em.persist(motorista);
		Optional<Motorista> optional = motoristaRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		motoristaRepository.delete(objetoRecebido);
		optional = motoristaRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}