package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Role;
import com.tacografoonline.ontruck.model.Usuario;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class UsuarioRepositoryTest {
	
	private static Pageable pageable;

	private Usuario usuario;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		usuario = new Usuario("UsuarioTest", "usuariotest", "123");
	}

	@Test
	void testSuccessSaveUsuario() {
		try {
			Role role = new Role("ROLE_TEST");
			em.persist(role);
			List<Role> roles = new ArrayList<>();
			roles.add(role);
			usuario.setRoles(roles);
			Usuario objetoRecebido = usuarioRepository.save(usuario);
			assertEquals(usuario, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveUsuarioNulo() {
		try {
			usuarioRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllUsuario() {
		em.persist(usuario);
		Page<Usuario> estados = usuarioRepository.findAll(pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllUsuarioByNome() {
		em.persist(usuario);
		Page<Usuario> estados = usuarioRepository.findByNome(usuario.getNome(), pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindOneUsuarioByNome() {
		em.persist(usuario);
		Optional<Usuario> estados = usuarioRepository.findByNome(usuario.getNome());
		assertTrue(estados.isPresent());
	}

	@Test
	void testSuccessUpdateUsuario() {
		em.persist(usuario);
		Optional<Usuario> objetoRecebido = usuarioRepository.findById(usuario.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(usuario.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome(usuario.getNome() + "Updated");
		Optional<Usuario> objetoRecebidoUpdated = usuarioRepository.findById(usuario.getId());
		assertEquals("UsuarioTestUpdated", objetoRecebidoUpdated.get().getNome());
	}

	@Test
	void testFailUpdateUsuario() {
		Optional<Usuario> objetoRecebido = usuarioRepository.findByNome(usuario.getNome());
		if (objetoRecebido.isPresent()) {
			fail("O item não deveria existir.");
		}
	}
	
	@Test
	void testSuccessDeleteUsuarioById() {
		Usuario objetoRecebido = em.persist(usuario);
		Optional<Usuario> optional = usuarioRepository.findById(usuario.getId());
		assertTrue(optional.isPresent());
		usuarioRepository.delete(objetoRecebido);
		optional = usuarioRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}