package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Role;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class RoleRepositoryTest {
	
	private static Pageable pageable;

	private Role role;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		role = new Role("ROLE_TEST");
	}

	@Test
	void testSuccessSaveRole() {
		try {
			Role objetoRecebido = roleRepository.save(role);
			assertEquals(role, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveRoleNulo() {
		try {
			roleRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllRole() {
		em.persist(role);
		Page<Role> roles = roleRepository.findAll(pageable);
		assertTrue(roles.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllRoleByNome() {
		em.persist(role);
		List<Role> roles = roleRepository.findAll();
		assertTrue(roles.size() > 0);
	}

	@Test
	void testSuccessFindOneRoleByNome() {
		em.persist(role);
		Optional<Role> roles = roleRepository.findByNome(role.getNome());
		assertTrue(roles.isPresent());
	}

	@Test
	void testSuccessUpdateRole() {
		em.persist(role);
		Optional<Role> objetoRecebido = roleRepository.findById(role.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(role.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome(role.getNome() + "Updated");
		Optional<Role> objetoRecebidoUpdated = roleRepository.findById(role.getId());
		assertEquals("ROLE_TESTUpdated", objetoRecebidoUpdated.get().getNome());
	}

	@Test
	void testFailUpdateRole() {
		Optional<Role> objetoRecebido = roleRepository.findByNome(role.getNome());
		if (objetoRecebido.isPresent()) {
			fail("O item não deveria existir.");
		}
	}
	
	@Test
	void testSuccessDeleteRoleById() {
		Role objetoRecebido = em.persist(role);
		Optional<Role> optional = roleRepository.findById(role.getId());
		assertTrue(optional.isPresent());
		roleRepository.delete(objetoRecebido);
		optional = roleRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}