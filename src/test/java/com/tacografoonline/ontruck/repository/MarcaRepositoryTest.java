package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Marca;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class MarcaRepositoryTest {
	
	private static Pageable pageable;

	private Marca marca;
	
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		marca = new Marca("MarcaTest");
	}

	@Test
	void testSuccessSaveMarca() {
		try {
			Marca objetoRecebido = marcaRepository.save(marca);
			assertEquals(marca, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveMarcaNulo() {
		try {
			marcaRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllMarca() {
		em.persist(marca);
		Page<Marca> estados = marcaRepository.findAll(pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllMarcaByNome() {
		em.persist(marca);
		Page<Marca> estados = marcaRepository.findByNome(marca.getNome(), pageable);
		assertTrue(estados.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindOneMarcaByNome() {
		em.persist(marca);
		Optional<Marca> marca = marcaRepository.findByNome(this.marca.getNome());
		assertTrue(marca.isPresent());
	}

	@Test
	void testSuccessUpdateMarca() {
		em.persist(marca);
		Optional<Marca> objetoRecebido = marcaRepository.findById(marca.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(marca.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome("MarcaTestUpdated");
		Optional<Marca> optionalUpdated = marcaRepository.findById(marca.getId());
		assertEquals("MarcaTestUpdated", optionalUpdated.get().getNome());
	}
	
	@Test
	void testSuccessDeleteMarcaById() {
		Marca objetoRecebido = em.persist(marca);
		Optional<Marca> optional = marcaRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		marcaRepository.delete(objetoRecebido);
		optional = marcaRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}