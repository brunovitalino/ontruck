package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Estado;

@ExtendWith(SpringExtension.class)
@DataJpaTest
// Permite o uso de bancos reais de persistência p/ realização de testes
// e não somente os bancos em memória como H2.
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class CidadeRepositoryTest {
	
	private static Pageable pageable;

	private Cidade cidade;
	
	@Autowired
	CidadeRepository cidadeRepository;
	
	@Autowired
	EstadoRepository estadoRepository;
	
	@Autowired
	TestEntityManager em;
	
	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "nome");
		int pageNumber=0, pageItensQty=15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}
	
	@BeforeEach
	void beforeEach() {
		Estado estado = em.persist(new Estado("EstadoTest"));
		cidade = new Cidade("CidadeTest", estado);
	}

	@Test
	void testSuccessSaveCidade() {
		try {
			Cidade objetoRecebido = cidadeRepository.save(cidade);
			assertEquals(cidade, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveCidadeNulo() {
		try {
			cidadeRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAll() {
		em.persist(cidade);
		Page<Cidade> cidades = cidadeRepository.findAll(pageable);
		assertTrue(cidades.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllByNome() {
		em.persist(cidade);
		Page<Cidade> cidades = cidadeRepository.findByNome(cidade.getNome(), pageable);
		assertTrue(cidades.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllByEstadoNome() {
		em.persist(cidade);
		Page<Cidade> cidades = cidadeRepository.findByEstadoNome(cidade.getEstado().getNome(), pageable);
		assertTrue(cidades.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllByNomeAndEstadoNome() {
		em.persist(cidade);
		Page<Cidade> cidades = cidadeRepository.findAllByNomeAndEstadoNome(cidade.getNome(),
				cidade.getEstado().getNome(), pageable);
		assertTrue(cidades.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindOneCidadeByNome() {
		em.persist(cidade);
		Optional<Cidade> cidade = cidadeRepository.findByNome(this.cidade.getNome());
		assertTrue(cidade.isPresent());
	}

	@Test
	void testSuccessUpdateCidade() {
		em.persist(cidade);
		Optional<Cidade> objetoRecebido = cidadeRepository.findById(cidade.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(cidade.getNome(), objetoRecebido.get().getNome());
		objetoRecebido.get().setNome("CidadeUpdated");
		Optional<Cidade> optionalUpdated = cidadeRepository.findById(cidade.getId());
		assertEquals("CidadeUpdated", optionalUpdated.get().getNome());
	}
	
	@Test
	void testSuccessDeleteCidadeById() {
		Cidade objetoRecebido = em.persist(cidade);
		Optional<Cidade> optional = cidadeRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		cidadeRepository.delete(objetoRecebido);
		optional = cidadeRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}