package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Movimentacao;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.model.Viagem;
import com.tacografoonline.ontruck.model.enums.MovendoParadoEnum;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class MovimentacaoRepositoryTest {

	private static Pageable pageable;

	private Movimentacao movimentacao;

	@Autowired
	MovimentacaoRepository movimentacaoRepository;

	@Autowired
	MotoristaRepository motoristaRepository;
	
	@Autowired
	CorRepository corRepository;
	
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	VeiculoRepository veiculoRepository;

	@Autowired
	EstadoRepository estadoRepository;

	@Autowired
	CidadeRepository cidadeRepository;

	@Autowired
	ViagemRepository viagemRepository;
	
	@Autowired
	TestEntityManager em;

	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "created");
		int pageNumber = 0, pageItensQty = 15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}

	@BeforeEach
	void beforeEach() {
		LocalDate data = LocalDate.now();
		Motorista motorista = em.persist(new Motorista("99988877701", "Motorista1Test"));
		Cor cor = em.persist(new Cor("CorTest"));
		Marca marca = em.persist(new Marca("MarcaTest"));
		Modelo modelo = em.persist(new Modelo("ModeloTest", marca));
		Veiculo veiculo = em.persist(new Veiculo("PlacaTest", cor, modelo, 2020, 15000));
		Estado estado = em.persist(new Estado("EstadoTest"));
		Cidade origem = em.persist(new Cidade("OrigemTest", estado));
		Cidade destino = em.persist(new Cidade("DestinoTest", estado));
		LocalDateTime horarioPartida = LocalDateTime.now();
		LocalDateTime horarioChegada = LocalDateTime.now();
		Integer kmTotalPartida = 0;
		Integer kmTotalChegada = 45;
		Viagem viagem = viagemRepository.save(new Viagem(data, motorista, motorista, veiculo, origem, destino,
				horarioPartida, horarioChegada, kmTotalPartida, kmTotalChegada));
		LocalDateTime horario = LocalDateTime.now();
		Integer velocidade = 60;
		movimentacao = new Movimentacao(viagem, MovendoParadoEnum.PARADO, horario, velocidade);
	}

	@Test
	void testSuccessSaveMovimentacao() {
		try {
			Movimentacao objetoRecebido = movimentacaoRepository.save(movimentacao);
			assertEquals(movimentacao, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveMovimentacaoNulo() {
		try {
			movimentacaoRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAll() {
		em.persist(movimentacao);
		Page<Movimentacao> movimentacaos = movimentacaoRepository.findAll(pageable);
		assertTrue(movimentacaos.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllByData() {
		em.persist(movimentacao);
		LocalDate data = movimentacao.getHorario().toLocalDate();
		Page<Movimentacao> movimentacaos = movimentacaoRepository.findAllByDataBetween(data.atStartOfDay(),
				data.plusDays(1).atStartOfDay(), pageable);
		assertTrue(movimentacaos.hasContent());
	}

	@Test
	void testSuccessFindAllByViagemId() {
		em.persist(movimentacao);
		Page<Movimentacao> movimentacaos = movimentacaoRepository.findByViagemId(movimentacao.getViagem().getId(), pageable);
		assertTrue(movimentacaos.hasContent());
	}

	@Test
	void testSuccessUpdateVelocidade() {
		em.persist(movimentacao);
		Optional<Movimentacao> objetoRecebido = movimentacaoRepository.findById(movimentacao.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(movimentacao.getVelocidade(), objetoRecebido.get().getVelocidade());
		objetoRecebido.get().setVelocidade(80);
		Optional<Movimentacao> optionalUpdated = movimentacaoRepository.findById(movimentacao.getId());
		assertEquals(80, optionalUpdated.get().getVelocidade());
	}

	@Test
	void testSuccessDeleteById() {
		Movimentacao objetoRecebido = em.persist(movimentacao);
		Optional<Movimentacao> optional = movimentacaoRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		movimentacaoRepository.delete(objetoRecebido);
		optional = movimentacaoRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}