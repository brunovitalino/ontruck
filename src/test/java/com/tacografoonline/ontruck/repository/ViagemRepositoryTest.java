package com.tacografoonline.ontruck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tacografoonline.ontruck.model.Cidade;
import com.tacografoonline.ontruck.model.Cor;
import com.tacografoonline.ontruck.model.Estado;
import com.tacografoonline.ontruck.model.Marca;
import com.tacografoonline.ontruck.model.Modelo;
import com.tacografoonline.ontruck.model.Motorista;
import com.tacografoonline.ontruck.model.Veiculo;
import com.tacografoonline.ontruck.model.Viagem;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class ViagemRepositoryTest {

	private static Pageable pageable;

	private Viagem viagem;
	
	@Autowired
	TestEntityManager em;

	@Autowired
	ViagemRepository viagemRepository;

	@Autowired
	MotoristaRepository motoristaRepository;
	
	@Autowired
	CorRepository corRepository;
	
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	VeiculoRepository veiculoRepository;

	@Autowired
	EstadoRepository estadoRepository;

	@Autowired
	CidadeRepository cidadeRepository;

	@BeforeAll
	static void beforeAll() {
		Sort sort = Sort.by(Direction.ASC, "created");
		int pageNumber = 0, pageItensQty = 15;
		pageable = PageRequest.of(pageNumber, pageItensQty, sort);
	}

	@BeforeEach
	void beforeEach() {
		LocalDate data = LocalDate.now();
		Motorista motorista = em.persist(new Motorista("99988877701", "Motorista1Test"));
		Cor cor = em.persist(new Cor("CorTest"));
		Marca marca = em.persist(new Marca("MarcaTest"));
		Modelo modelo = em.persist(new Modelo("ModeloTest", marca));
		Veiculo veiculo = em.persist(new Veiculo("PlacaTest", cor, modelo, 2020, 15000));
		Estado estado = em.persist(new Estado("EstadoTest"));
		Cidade origem = em.persist(new Cidade("OrigemTest", estado));
		Cidade destino = em.persist(new Cidade("DestinoTest", estado));
		LocalDateTime horarioPartida = LocalDateTime.now();
		LocalDateTime horarioChegada = LocalDateTime.now();
		Integer kmTotalPartida = 0;
		Integer kmTotalChegada = 45;
		viagem = new Viagem(data, motorista, motorista, veiculo, origem, destino, horarioPartida, horarioChegada,
				kmTotalPartida, kmTotalChegada);
	}

	@Test
	void testSuccessSaveViagem() {
		try {
			Viagem objetoRecebido = viagemRepository.save(viagem);
			assertEquals(viagem, objetoRecebido);
		} catch (Exception e) {
			fail("Não foi possível salvar. Entidade passada como parâmetro está nula.");
		}
	}

	@Test
	void testFailSaveViagemNulo() {
		try {
			viagemRepository.save(null);
			fail("Não deveria ser possível salvar uma Entidade nula passada como parâmetro.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testSuccessFindAllViagem() {
		em.persist(viagem);
		Page<Viagem> viagems = viagemRepository.findAll(pageable);
		assertTrue(viagems.get().findAny().isPresent());
	}

	@Test
	void testSuccessFindAllViagemByData() {
		em.persist(viagem);
		Page<Viagem> viagems = viagemRepository.findByData(viagem.getData(), pageable);
		assertTrue(viagems.hasContent());
	}

	@Test
	void testSuccessFindAllViagemByVeiculoPlaca() {
		em.persist(viagem);
		Page<Viagem> viagems = viagemRepository.findByVeiculoPlaca(viagem.getVeiculo().getPlaca(), pageable);
		assertTrue(viagems.hasContent());
	}

	@Test
	void testSuccessUpdateViagem() {
		em.persist(viagem);
		Optional<Viagem> objetoRecebido = viagemRepository.findById(viagem.getId());
		if (objetoRecebido.isEmpty()) {
			fail("O item que deveria existir, não pôde ser encontrado.");
		}
		assertEquals(viagem.getKmTotalChegada(), objetoRecebido.get().getKmTotalChegada());
		objetoRecebido.get().setKmTotalChegada(50);
		Optional<Viagem> optionalUpdated = viagemRepository.findById(viagem.getId());
		assertEquals(50, optionalUpdated.get().getKmTotalChegada());
	}

	@Test
	void testSuccessDeleteViagemById() {
		Viagem objetoRecebido = em.persist(viagem);
		Optional<Viagem> optional = viagemRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isPresent());
		viagemRepository.delete(objetoRecebido);
		optional = viagemRepository.findById(objetoRecebido.getId());
		assertTrue(optional.isEmpty());
	}

}