package com.tacografoonline.ontruck.controller;

import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc // Faz-se necessaria pois @WebMvcTest nao esta sendo usada
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class AuthenticationControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testSuccessReturn200WhenAuthenticationIsValid() {
		try {
			URI uri = new URI("/auth");
			String body = "{\"email\":\"jd@ontruck.com\",\"senha\":\"123\"}";
			
			mockMvc.perform(
				MockMvcRequestBuilders
				.post(uri)
				.content(body)
				.contentType(MediaType.APPLICATION_JSON)
			).andExpect(
				MockMvcResultMatchers
				.status()
				.isOk()
			).andReturn();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Código retornado não foi 200.");
		}
	}
	
	@Test
	void testSuccessReturn400WhenAuthenticationIsInvalid() {
		try {
			URI uri = new URI("/auth");
			String body = "{\"email\":\"invalido@ontruck.com\",\"senha\":\"123\"}";
			
			mockMvc.perform(
				MockMvcRequestBuilders
				.post(uri)
				.content(body)
				.contentType(MediaType.APPLICATION_JSON)
			).andExpect(
				MockMvcResultMatchers
				.status()
				.is4xxClientError()
			);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Código retornado não foi 400.");
		}
	}

}