package com.tacografoonline.ontruck.controller;

import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
//@WebMvcTest
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class EstadoControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testSuccessReturnSomeEstado() {
		try {
			URI uri = new URI("/estados");
			
			MvcResult result = mockMvc.perform(
						MockMvcRequestBuilders
						.get(uri)
					).andExpect(
						MockMvcResultMatchers
						.status()
						.isOk()
					).andReturn();
			
			result.getResponse().getContentAsString();

		} catch (Exception e) {
			e.printStackTrace();
			fail("Código retornado não foi 200.");
		}
	}
}